/**
* <h1>ExtractNeo4jScript!</h1>
* Extract Neo4j script is used to extract Neo4j script as follows:
* <ol>
* <li>Start with an entity and create a node that its' label is the entity name and the atttributes are its properties. </li>
* <li>All the relations are having the same </li>
*
* @author  Omnia Kahla
* @version 1.0
* @since   2018-10-05
*
*/
/**
 * [ExtractNeo4jSchema A function used to return the script for the Neo4j. It searches for the entities to create nodes and for relationships to create the relationship between nodes.]
 * @param       {Array} shapes [The shapes array]
 * @param       {Array} intialeValues [an array that contains the intiale values of the properties.]
 * @param   {Array } directions [an array that contains the relationships directions]
 */
function ExtractNeo4jSchema(shapes,intialeValues,directions){
  /* Initial values is an array of attribute type
  attribute_type = {
   attributeOFEntityOrRelation:EntityOrRelation,
    entity_name:Entity_name,//used in the UI
    attribute_name: connectedAttributes[i].shape_name,//used in the UI
    attribute_Initialvalue:"",//returned from UI
    entityName_attributeName:Entity_name+"_"+connectedAttributes[i].shape_name//used for searching
  };
  direction is an array of relation details
  var relationdetails={
    relation:shapes[i],
    connectedEntitiesToParentEntity:[],
    connectedEntitiesToRelation:connectedEntities,
    mainRole:"",
    directionFrom:""
  };
   */
  var schema="", createNodesScript="", createRelationshipScript="";
  for (var i = 0; i < shapes.length; i++) {
    if(shapes[i].shape_type==ENTITY_SHAPE && !shapes[i].shape_name.toLowerCase().endsWith("_p")){
      checkedShapes.push(shapes[i].id);
      var connectedAttributes=GetConnectedObjects(shapes,shapes[i].id,ATTRIBUTE_SHAPE);
      /*var connectedEntities=GetConnectedObjects(shapes,shapes[i].id,ENTITY_SHAPE);
      for(var j=0;j<connectedEntities.length;j++){
        var connectedAttributesToEntity=GetConnectedObjects(shapes,connectedEntities[j].id,ATTRIBUTE_SHAPE);
        for(var k=0;k<connectedAttributesToEntity.length;k++){
          temp=connectedEntities[j].shape_name+"_"+connectedAttributesToEntity[k].shape_name;
          connectedAttributesToEntity[k].shape_name=temp;
          connectedAttributes.push( Object.assign({}, connectedAttributesToEntity[k]));
          //Array.prototype.push.apply(connectedAttributes, Object.assign({}, connectedAttributesToEntity[k]));
        }
        //Array.prototype.push.apply(connectedAttributes, connectedAttributesToEntity);
      }*/
      createNodesScript+=CreateNode(shapes,shapes[i].shape_name,connectedAttributes,intialeValues);
      }
      else if(shapes[i].shape_type==ENTITY_SHAPE && shapes[i].shape_name.toLowerCase().endsWith("_p")){
        checkedShapes.push(shapes[i].id);
        var connectedAttributes=GetConnectedObjects(shapes,shapes[i].id,ATTRIBUTE_SHAPE);
        createNodesScript+=CreateNode(shapes,shapes[i].shape_name,connectedAttributes,intialeValues);
        var connectedEntities=GetConnectedObjects(shapes,shapes[i].id,ENTITY_SHAPE);
        createRelationshipScript+=CreateIsARelation(shapes[i],connectedEntities);
        createRelationshipScript+="\n";
      }
      else if(shapes[i].shape_type==RELATION_SHAPE){
        checkedShapes.push(shapes[i].id);
        var connectedAttributes=GetConnectedObjects(shapes,shapes[i].id,ATTRIBUTE_SHAPE);
        var connectedEntities=GetConnectedObjects(shapes,shapes[i].id,ENTITY_SHAPE);
        createRelationshipScript+=CreateRelationship(shapes,shapes[i],connectedAttributes,connectedEntities,intialeValues,directions);
        createRelationshipScript+="\n";
        }
  }
  schema=createNodesScript+"\n"+createRelationshipScript;
  return schema;
}
/**
 * [CreateNode returns the stetement for the node creation]
 * @param {Array}shapes [ The shapes array]
 * @param       {String} Entity_name         [The entity name]
 * @param       {Array} connectedAttributes [All the connected attributes to the entity, it includes also all connecected attributes to parent entity that is connected to the main entity if exists.]
 * @param       {Array} intialValues       [An array that contains the initial values for the attributes]
 */
function CreateNode(shapes,Entity_name,connectedAttributes,initialValues){
  var initialvalue;
  var node="CREATE (n:"+Entity_name;//name: 'Andy', title: 'Developer' })";
  if(connectedAttributes.length>0){
    node+="{";
  }
  for(var i=0;i<connectedAttributes.length;i++){
    var structuredAttribute=GetConnectedObjects(shapes,connectedAttributes[i].id,ATTRIBUTE_SHAPE);
    if (structuredAttribute.length>0) {
      for(var j=0;j<structuredAttribute.length;j++){
        var attribute=connectedAttributes[i].shape_name+"_"+structuredAttribute[j].shape_name;
        node+=attribute+":";
        initialvalue=searchInInitialValues(Entity_name+"_"+attribute,initialValues);
        node+=initialvalue.attribute_Initialvalue;
        if(j<structuredAttribute.length-1){
          node+=",";
        }
      }
    }
    else{
      node+=connectedAttributes[i].shape_name+":";
      initialvalue=searchInInitialValues(Entity_name+"_"+connectedAttributes[i].shape_name,initialValues);
      node+=initialvalue.attribute_Initialvalue;
      //node+=searchInInitialValues(Entity_name+"_"+connectedAttributes[i].shape_name,initialValues);
    }
    if(i<connectedAttributes.length-1){
      node+=",";
    }
  }
  if(connectedAttributes.length>0){
    node+="}";
  }
   node+=")\n";
  return node;
}
/**
 * [CreateRelationship returns the stetement for the relationship creation]
 * @param {Array} shapes [The shapes array]
 * @param       {Object} Relation       [The relationship object]
 * @param       {Array} connectedAttributes [All the connected attributes to the relationship.]
 * @param       {Array} connectedEntities   [An array of the connected entities]
 * @param       {Array} intialeValues       [An array that contains the initial values for the attributes]
 * @param {Array} directions  [An array of the directions between 2 nodes]
 */
function CreateRelationship(shapes,Relation,connectedAttributes,connectedEntities,initialValues,directions){
  var arrayOfCharacters=  [...Array(26)].reduce(a=>a+String.fromCharCode(i++),'',i=97);
  var relationship="";
  var initialvalue="";
  var matchQuery = "MATCH ";
  var createQuery="CREATE ";
  var relation_properties="";
  var connectedchilds="";//new Array();
  var recursive=false;
  var Relation_name=Relation.shape_name;
  var indexOfRelationDetails=GetRelationDetails(Relation/*,connectedEntities*/,directions);
  if(connectedEntities.length==1){//a recursive Relation
    recursive=true;
    //CreateRecursiveRelationship(shapes,Relation,connectedAttributes,connectedEntities[0],initialValues,directions);
  }
  for(var i=0;i<connectedAttributes.length;i++){
    var structuredAttribute=GetConnectedObjects(shapes,connectedAttributes[i].id,ATTRIBUTE_SHAPE);
    if (structuredAttribute.length>0) {
      for(var j=0;j<structuredAttribute.length;j++){
        var attribute=connectedAttributes[i].shape_name+"_"+structuredAttribute[j].shape_name;
        relation_properties+=attribute+":";
        initialvalue=searchInInitialValues(Relation_name+"_"+connectedAttributes[i].shape_name+"_"+structuredAttribute[j].shape_name,initialValues);
        relation_properties+=initialvalue.attribute_Initialvalue;

        //relation_properties+=searchInInitialValues(Relation_name+"_"+connectedAttributes[i].shape_name,initialValues);
        if(j<structuredAttribute.length-1){
          relation_properties+=",";
        }
      }
    }
    else{
      relation_properties+=connectedAttributes[i].shape_name+":";
      initialvalue=searchInInitialValues(Relation_name+"_"+connectedAttributes[i].shape_name,initialValues);
      relation_properties+=initialvalue.attribute_Initialvalue;

      //relation_properties+=searchInInitialValues(Relation_name+"_"+connectedAttributes[i].shape_name,initialValues);
    }
    if(i<connectedAttributes.length-1){
      relation_properties+=", ";
    }
  }
  //Suppose it is a binary relation, this solution will not work with the trenary relationship.
  //Also, I am not adding the recursive relation to the directions array.
  var reversedConnectedEntities=new Array();
  if(!recursive&& directions[indexOfRelationDetails].directionFrom==connectedEntities[1].shape_name ){
    connectedEntities=connectedEntities.reverse();
  }
  for(var i=0;i<connectedEntities.length;i++){
     //connectedchilds.splice(0);
     connectedchilds="";
    if(connectedEntities[i].shape_name.toLowerCase().endsWith("_p")){
      connectedchilds=((indexOfRelationDetails==-1)?connectedEntities[i].shape_name:directions[indexOfRelationDetails].mainRole)//GetConnectedObjects(shapes,connectedEntities[i].id,ENTITY_SHAPE);
      //if(connectedchilds.length!=0){
      if(connectedchilds!=""){
        matchQuery+="("+arrayOfCharacters[i]+":"+connectedchilds+")";//connectedchilds[0].shape_name+")";
        createQuery+="("+arrayOfCharacters[i]+")";
        if(i<connectedEntities.length-1){
          matchQuery+=",";
          createQuery+="-[:"+Relation_name+((relation_properties=="")?"":"{"+relation_properties+"}")+"]"+"->";
        }
        continue;
      }
    }
    matchQuery+="("+arrayOfCharacters[i]+":"+connectedEntities[i].shape_name+")";
    createQuery+="("+arrayOfCharacters[i]+")";
    if(recursive){
      matchQuery+=",("+arrayOfCharacters[i+1]+":"+connectedEntities[i].shape_name+")";
      createQuery+="-[:"+Relation_name+((relation_properties=="")?"":"{"+relation_properties+"}")+"]"+"->";
      createQuery+="("+arrayOfCharacters[i+1]+")";
      return matchQuery+"\n"+createQuery+"\n";
    }
    if(i<connectedEntities.length-1){
      matchQuery+=",";
      createQuery+="-[:"+Relation_name+((relation_properties=="")?"":"{"+relation_properties+"}")+"]"+"->";
    }
  }
  relationship=matchQuery+"\n" +createQuery+"\n";
  return relationship;
}
function CreateIsARelation(parentEntity,connectedchilds){
  var matchQuery = "MATCH ";
  var createQuery="CREATE ";
  for(var i=0;i<connectedchilds.length;i++){
        matchQuery+="(a:"+connectedchilds[i].shape_name+"),(b:"+parentEntity.shape_name+")";//connectedchilds[0].shape_name+")";
        createQuery+="(a)-[:isA]->(b)";
    }
      return matchQuery+"\n"+createQuery+"\n";
}
function GetRelationDetails(Relation/*,connectedEntities*/,directions){
  /*var relationdetails={
    relation:shapes[i],
    connectedEntitiesToParentEntity:[],
    connectedEntitiesToRelation:connectedEntities,
    mainRole:"",
    directionFrom:""
  };*/
  for (var i = 0; i < directions.length; i++) {
    if(directions[i].relation.id==Relation.id ){
      return i;
    }
  }
  return -1;
}
function CreateRecursiveRelationship(shapes,Relation,connectedAttributes,Entity,initialValues,directions)
{
  var relationship="";
  var matchQuery = "MATCH ";
  var createQuery="CREATE ";
  var relation_properties="";
  var roles=new Array();
  var Relation_name=Relation.shape_name;
  var lineBetweenEntityAndRecursiveRelation=getLinebetweenRelationAndEntity(shapes,Relation.id,Entity.id);
  for(var i=0;i<connectedAttributes.length;i++){
    var structuredAttribute=GetConnectedObjects(shapes,connectedAttributes[i].id,ATTRIBUTE_SHAPE);
    if (structuredAttribute.length>0) {
      for(var j=0;j<structuredAttribute.length;j++){
        relation_properties+=connectedAttributes[i].shape_name+"_"+structuredAttribute[j].shape_name;
        relation_properties+=attribute+":";
        relation_properties+=searchInInitialValues(Relation_name+"_"+connectedAttributes[i].shape_name,initialValues);
        if(j<structuredAttribute.length-1){
          relation_properties+=",";
        }
      }
    }
    else{
      relation_properties+=connectedAttributes[i].shape_name+":";
      relation_properties+=searchInInitialValues(Relation_name+"_"+connectedAttributes[i].shape_name,initialValues);
    }
    if(i<connectedAttributes.length-1){
      relation_properties+=", ";
    }
  }

      temp=lineBetweenEntityAndRecursiveRelation.shape_name.substr(9);//removes the keyword recursive
      temp=temp.trim();
      roles.push(temp.substr(0,temp.indexOf("(")));//added the first role
      temp2=temp.substr(temp.indexOf(")")+1);
      roles.push(temp2.substr(0,temp2.indexOf("(")));//added the second role

    matchQuery+="(a"+":"+Entity.shape_name+")";
    createQuery+="(a)";
      matchQuery+=",(b:"+Entity.shape_name+")";
      createQuery+="-[:"+Relation_name+((relation_properties=="")?"":"{"+relation_properties+"}")+"]"+"->";
      createQuery+="(b)";
      return matchQuery+"\n"+createQuery+"\n";

}
function searchInInitialValues(Entity_ConnectedAttribute,initialValues){
  if(initialValues.length==0){
    return "\"\"";
  }
  for (var i = 0; i < initialValues.length; i++) {
    if(initialValues[i].entityName_attributeName==Entity_ConnectedAttribute){
      return initialValues[i];
    }
  }
}
