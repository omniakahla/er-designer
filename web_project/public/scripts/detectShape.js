/**
*
* @author  Omnia Kahla
* @version 1.0
* @since   2018-10-05
*
*/

/**
 * [computeOutSideRectangle A function used to draw the smallest rectangle that the drawn shape fits in.]
 * @param  {Array} points [The array of the mouse stroks]
 * @return {Array}        [The 4 points of the rectangle.]
 */
function computeOutSideRectangle(points)
{
	var maxX=-99999;
	var maxY=-99999;
	var minX=99999;
	var minY=99999;
	var p1,p1_temp;
	var p2,p2_temp;
	var p3,p3_temp;
	var p4,p4_temp;
	for(var i=0;i<points.length;i++)
	{
		if(points[i].x>maxX)
		{
			maxX=points[i].x;
			p1_temp=points[i];
		}
		if(points[i].x<minX)
		{
			minX=points[i].x;
			p2_temp=points[i];
		}
		if(points[i].y>maxY)
		{
			maxY=points[i].y;
			p3_temp=points[i];
		}
		if(points[i].y<minY)
		{
			minY=points[i].y;
			p4_temp=points[i];
		}
	}
	p1 = {x:minX, y:minY};
	p2= {x:minX, y:maxY};
	p3= {x:maxX, y:maxY};
	p4= {x:maxX, y:minY};

	EnclosedRec_P= perimeterOfPolygon([p1,p2,p3,p4,p1]);
	EnclosedRec_A=areaOfPolygon([p1,p2,p3,p4,p1]);
	/////more variables used for drawing the fine shapes
	start_point=points[0];
	end_point=points[points.length-1];
	if((p1_temp==p2_temp)||(p1_temp==p3_temp)||(p1_temp==p4_temp))//so it is a line
	{
		EnclosedRec_Width = 0;
	}
	else {
		EnclosedRec_Width = (maxX-minX);
	}
	EnclosedRec_Height = (maxY-minY);
	centerX_circle = minX + EnclosedRec_Width/2;
	centerY_circle = minY + EnclosedRec_Height/2;
	if(EnclosedRec_Width<EnclosedRec_Height)
	{
			diameter_circle = EnclosedRec_Width/2;
	}
	else {
		diameter_circle= EnclosedRec_Height/2;
	}

	//return [p2,p3,p1,p4,p2];
	return [p1,p2,p3,p4,p1];

}
/**
 * [computeInSideRectangle A function used to draw the biggest rectangle that fits inside the drawn shape.]
 * @param  {Array} points [The array of the mouse stroks]
 * @return {Array}        [The 4 points of the rectangle.]
 */
function computeInSideRectangle(points)
{
	var minX=9999,minY=9999,maxX=-9999,maxY=-9999;
		var p1,p2,p3,p4;
		for(var i=0;i<points.length;i++)
			{
				if(points[i].x>maxX)
				{
					maxX=points[i].x;
					p3=points[i];
				}
				if(points[i].x<minX)
				{
					minX=points[i].x;
					p1=points[i];
				}
				if(points[i].y>maxY)
				{
					maxY=points[i].y;
					p2=points[i];
				}
				if(points[i].y<minY)
				{
					minY=points[i].y;
					p4=points[i];

			}
		}
		LargestQuad_A = areaOfPolygon([p1,p2,p3,p4,p1]);
			return [p1,p2,p3,p4,p1];
}
/**
 * [computeInSideTriangle  A function used to draw the biggest Triangle that fits inside the drawn shape.]
 * @param  {Array} points [The array of the mouse stroks]
 * @return {Array}        [The 3 points of the triangle.]
 */
function computeInSideTriangle(points)
{
	var maxArea= -1;
	var calculatedArea=-1;
	var p0 ,p1,p2;

	for (var a=0;a<points.length-2;a++)
	{
		for(var b=a+1;b<points.length-1;b++)
		{
			for (var c=b+1;c<points.length;c++)
			{
				calculatedArea=areaOfPolygon([points[a], points[b], points[c]]);
				if(calculatedArea>maxArea)
				{
					maxArea=calculatedArea;
					p0=points[a];
					p1=points[b];
					p2=points[c];
				}
			}
		}
	}
	LargestTrian_P=perimeterOfPolygon([p0,p1,p2,p0]);
	LargestTrian_A=areaOfPolygon([p0,p1,p2,p0]);
	return [p0,p1,p2,p0];
}
/**
 * [convexHull  A function used to draw the convex hull of the drawn shape.]
 * @param  {Array} points [The array of the mouse stroks]
 * @return {Array}        [Array of points of the convex hull]
 */
function convexHull(points)
    {
    		var result ;
        var convexHull_points= new Array();
        if (points.length < 3)
        {
					result=Array.from(points);
						return result;
        }
        var minPoint = -1, maxPoint = -1;
        var minX = Number.MAX_VALUE;
        var maxX = Number.MIN_VALUE;
        for (var i = 0; i < points.length; i++)
        {
            if (points[i].x < minX)
            {
                minX = points[i].x;
                minPoint = i;
            }
            if (points[i].x > maxX)
            {
                maxX = points[i].x;
                maxPoint = i;
            }
        }
        var A,B;
				A=points[minPoint];
				B=points[maxPoint];
        convexHull_points.push(A);
        convexHull_points.push(B);

				if(minPoint<maxPoint)
				{
					points.splice(maxPoint,1);
				  points.splice(minPoint,1);
			  }
			  else {
					points.splice(minPoint,1);
					points.splice(maxPoint,1);
				}

        var leftSet= new Array();
        var rightSet= new Array();
        for (var i = 0; i < points.length; i++)
        {
            var p = points[i];
						var location= pointLocation(A, B, p);
            if ( location == -1)
                leftSet.push(p);
            else if (location == 1)
                rightSet.push(p);
        }
        hullSet(A, B, rightSet, convexHull_points);
        hullSet(B, A, leftSet, convexHull_points);
				//Using the object set to keep unique values only, because the result contains a lot of duplicates.
			  result=Array.from(new Set(convexHull_points));
        return result;
    }

    function distance( A,  B,  C)
    {
        var ABx = B.x - A.x;
        var ABy = B.y - A.y;
        var num = ABx * (A.y - C.y) - ABy * (A.x - C.x);
				if(num<0)
					num=-num;
        //return Math.abs(num);
				return num;
    }

		function indexOf(point, list)
		{
			 var index=-1;
			 for(var i=0;i<list.length;i++)
			 		if(point.x==list[i].x&&point.y==list[i].y)
					return i;
			 return index;
		}

    function hullSet( A,  B,  set,  hull)
    {
        var insertPosition = indexOf(B,hull);
        if (set.length == 0)
            return;
        if (set.length == 1)
        {
            var P = set[0];
						set.splice(0,1);
						hull.splice(insertPosition,0,P);
            return;
        }
        var dist = Number.MIN_VALUE;
        var furthestPoint = -1;
        for (var i = 0; i < set.length; i++)
        {
            var P = set[i];
            var distance1 = distance(A, B, P);
            if (distance1 > dist)
            {
                dist = distance1;
                furthestPoint = i;
            }
        }
        var P = set[furthestPoint];
				set.splice(furthestPoint,1);
				hull.splice(insertPosition,0,P);
			  // Determine who's to the left of AP
        var leftSetAP = new Array();//= new ArrayList<Point>();
        for (var i = 0; i < set.length; i++)
        {
            var M = set[i];
            if (pointLocation(A, P, M) == 1)
            {
                leftSetAP.push(M);
            }
        }

        // Determine who's to the left of PB
        var leftSetPB= new Array();//= new ArrayList<Point>();
        for (var i = 0; i < set.length; i++)
        {
            var M = set[i];
            if (pointLocation(P, B, M) == 1)
            {
                leftSetPB.push(M);
            }
        }
        hullSet(A, P, leftSetAP, hull);
        hullSet(P, B, leftSetPB, hull);
				//return;
    }

    function pointLocation( A,  B,  P)
    {
        var cp1 = (B.x - A.x) * (P.y - A.y) - (B.y - A.y) * (P.x - A.x);
        if (cp1 > 0)
            return 1;
        else if (cp1 == 0)
            return 0;
        else
            return -1;
    }
/**
 * [areaOfPolygon A helper function to get an area of a Polygon]
 * @param  {Array} points [The array of the mouse stroks]
 * @return {DOUBLE}        [The area of the polygon.]
 */
function areaOfPolygon(points)
{
	var result = 0;
	var temp1=0,temp2=0;
	for (var i=1;i<points.length;i++)
	{
		temp1+= points[i-1].x * points[i].y;
		temp2+=points[i-1].y*points[i].x;
	}
	result =Math.abs((temp1-temp2)/2);
	return result;
}
/**
 * [perimeterOfPolygon  A helper function to get the perimeter of a Polygon]
 * @param  {Array} points [The array of the mouse stroks]
 * @return {DOUBLE}        [The perimeter of the polygon.]
 */
function perimeterOfPolygon(points)
{
	var result = 0;
	for(var i=1;i<points.length;i++)
	{
		result+= distanceBetween2Points(points[i-1].x,points[i-1].y,points[i].x, points[i].y);
	}
	return result;
}
/**
 * [distanceBetween2Points A helper function to get the distance between 2 points]
 * @param  {Int} aX [The x position for the 1st point]
 * @param  {Int} aY [The y position for the 1st point]
 * @param  {Int} bX [The x position for the 2nd point]
 * @param  {Int} bY [The y position for the 2nd point]
 * @return {DOUBLE}    [the distance between the 2 points]
 */
function distanceBetween2Points(aX, aY, bX,bY)
{
	var xDiff= bX-aX;
	var yDiff= bY-aY;
	var result = Math.sqrt(Math.pow(xDiff, 2)+Math.pow(yDiff, 2));
	return result;
}
/**
 * [DetectShape The main function to detect the shape. According to the paper "Online Sketch Recognition: Geometric Shapes" in https://aaltodoc.aalto.fi/handle/123456789/3747, It works as follows:
 * <ol>
 * <li>It calcualtes the ThinnessRatio=Math.pow(convexHull_P, 2)/convexHull_A. If the (ThinnessRatio <= 16) then it is a circle </li>
 * <li>If the (ThinnessRatio>=100) then it is a line</li>
 * <li>If the (convexHull_A/EnclosedRec_A>=0.65) then it is a rectangle</li>
 * <li>If the (LargestTrian_A/LargestQuad_A>=0.45 && LargestTrian_A/LargestQuad_A<=0.6) then it is a diamond</li>
 * </ol>
 * ]
 * @param       {Int} new_id [The generated Id for the shape.]
 * @return {Object} The shape object
 */
function DetectShape(new_id)
{
	var v_shape = {
		shape_x1:0,// the starting point of the shape, or the center for the circles.
		shape_y1:0,// the starting point of the shape, or the center for the circles.
		shape_type:"unkown", //rectangle, diamond, circle, ...
		shape_name:"", // for example Employees.
		shape_line:1, //1 for solid line, 0 for dotted line, 2 for double line
		shape_width:0, //used for rectangles and diamonds.
		shape_height:0,//used for rectangles and diamonds.
		shape_x2:0, //used for lines
		shape_y2:0,//used for lines
		shape_direction:1, //used for the arrows
		shape_diameter:0,//used for circles.
		id_shape_1:-1,
		id_shape_2:-1,
		id:new_id,
    key:-1//1 for priamry key, 0 for partial key, -1 otherwise
	};
	/*if(diameter_circle<10){
		return v_shape;
	}*/
	var ThinnessRatio=Math.pow(convexHull_P, 2)/convexHull_A;
/*	alert("To detect a line the ThinnessRatio >=100, now the ThinnessRatio ="+ ThinnessRatio +"\n\n To detect a circle or ellipse the ThinnessRatio <16 and A_tr/A_ch <0.5 "+
			"Now the ThinnessRatio = "+ThinnessRatio +" and A_tr/A_ch = "+(LargestTrian_A/convexHull_A) +"\n\n To detect a dimaond the  0.65 > A_tr/A_ch > 0.45 and A_ch^2/(A_tr*A_re)<1.6 , now the A_tr/A_ch = "+
		(LargestTrian_A/convexHull_A)+" and A_ch^2/(A_tr*A_re)= " +(Math.pow(convexHull_A,2)/(LargestTrian_A*EnclosedRec_A))+ "\n\n To detect a rectangle the 22>ThinnessRatio>15 and P_ch/P_re>0.85 and A_tr/A_re>=0.45 Now the ThinnessRatio= "+
	ThinnessRatio+" P_ch/P_re = "+(convexHull_P/EnclosedRec_P)+" A_tr/A_re = " +(LargestTrian_A/EnclosedRec_A));
*/
//alert("Line: ThinnessRatio >=100, it is"+ ThinnessRatio+"\nRectangle: P_ch/P_re>0.88, it is "+ (convexHull_P/EnclosedRec_P)+"\nCircle: A_tr/A_ch<0.4, it is "+ (LargestTrian_A/convexHull_A)+"\nDiamond:  A_ch^2/(A_tr*A_re)<1.6, it is " +(Math.pow(convexHull_A,2)/(LargestTrian_A*EnclosedRec_A)));

	if(ThinnessRatio >=100 )//correct one//&& (EnclosedRec_Height/EnclosedRec_Width)<=0.2)
	{
		v_shape.shape_x1=start_point.x;
		v_shape.shape_y1=start_point.y;
		v_shape.shape_x2=end_point.x;
		v_shape.shape_y2=end_point.y;
		v_shape.shape_type=LINE_SHAPE;
		return v_shape;
	}

	//	alert(Math.pow(convexHull_A,2)/(LargestTrian_A*EnclosedRec_A));
	//	if((LargestTrian_A/convexHull_A>=0.45 && LargestTrian_A/convexHull_A<=0.65) && (Math.pow(convexHull_A,2)/(LargestTrian_A*EnclosedRec_A)<1.6))
		if(Math.pow(convexHull_A,2)/(LargestTrian_A*EnclosedRec_A)<1.6) //most accurate
	//	if(LargestTrian_A/LargestQuad_A>=0.45 && LargestTrian_A/LargestQuad_A<=0.6)  //correct one
		{
			if(diameter_circle<10){
				return v_shape;
			}
			v_shape.shape_x1=start_point.x;
			v_shape.shape_y1=start_point.y;
			v_shape.shape_x2=start_point.x;//+diameter_circle;
			v_shape.shape_y2=start_point.y;//+diameter_circle;
			v_shape.shape_diameter=diameter_circle;
			v_shape.shape_width = EnclosedRec_Width;
			v_shape.shape_height = EnclosedRec_Height;
			v_shape.shape_type=RELATION_SHAPE;
			return v_shape;
		}
/*
//if(ThinnessRatio <= 17.5  && (convexHull_A/EnclosedRec_A)<0.85) //correct one
//if(ThinnessRatio <= 17.5 )
//if(ThinnessRatio <= 17.5  && (LargestTrian_A/convexHull_A)<0.5)
if( ThinnessRatio<18 &&(LargestTrian_A/EnclosedRec_A)<0.4 &&((convexHull_P/EnclosedRec_P)<0.85))//most accurate
{
	if(diameter_circle<10){
		return v_shape;
	}
	v_shape.shape_x1 = centerX_circle;
	v_shape.shape_y1=centerY_circle;
	v_shape.shape_x2 = centerX_circle;
	v_shape.shape_y2=centerY_circle;
	v_shape.shape_diameter=diameter_circle;
	v_shape.shape_width = EnclosedRec_Width;
	v_shape.shape_height = EnclosedRec_Height;
	v_shape.shape_type= ATTRIBUTE_SHAPE;
	return v_shape;
}
//if(convexHull_A/EnclosedRec_A>=0.6)//correct one
//if(convexHull_A/EnclosedRec_A>=0.6 && ThinnessRatio>18 && ThinnessRatio <25 && convexHull_P/EnclosedRec_P>0.85 && LargestTrian_A/EnclosedRec_A>=0.45)
if((convexHull_P/EnclosedRec_P)>0.88)//most accurate
{
	if(diameter_circle<10){
		return v_shape;
	}
	v_shape.shape_x1 = start_point.x;
	v_shape.shape_y1 = start_point.y;
	//fine_shapes[selected_index].shape_x2=mouseX+(0.5*fine_shapes[selected_index].shape_width);
	//fine_shapes[selected_index].shape_y2=mouseY+(0.5*fine_shapes[selected_index].shape_height);
	v_shape.shape_x2 = v_shape.shape_x1+(0.5*EnclosedRec_Width);//start_point.x;
	v_shape.shape_y2 = v_shape.shape_y1+(0.5*EnclosedRec_Height);// start_point.y;
	//v_shape.shape_x2 = start_point.x+(0.5*EnclosedRec_Width);
	//v_shape.shape_y2 = start_point.y+(0.5*EnclosedRec_Height);
	v_shape.shape_width = EnclosedRec_Width;
	v_shape.shape_height = EnclosedRec_Height;
	v_shape.shape_diameter=diameter_circle;
	v_shape.shape_type = ENTITY_SHAPE;
	return v_shape;
}

 */

else {
	var circle=false,rectangle=false;
//	var circle2=false,rectangle2=false;

//ThinnessRatio<18 -->circle, <18ThinnessRatio<22 --> rectangle
	var ratioBetweenTriangleAreaAndRectangleArea=LargestTrian_A/EnclosedRec_A;//circle --> this ratio is less than 0.4, rectangle --> this ratio is between 0.45 and 0.5
	var ratioBetweenCHperimeterAndRectangleperimeter=(convexHull_P/EnclosedRec_P);//circle --> this ratio is less than 0.85 , rectangle --> this ratio is greater than 0.85
//alert(ThinnessRatio+" \n"+ratioBetweenCHperimeterAndRectangleperimeter);
	if(ThinnessRatio<18){
		circle=true;
	}
	if(ratioBetweenCHperimeterAndRectangleperimeter>0.85){
		rectangle=true;
	}
	if (circle && !rectangle) {
		if(diameter_circle<10){
			return v_shape;
		}
		v_shape.shape_x1 = centerX_circle;
		v_shape.shape_y1=centerY_circle;
		v_shape.shape_x2 = centerX_circle;
		v_shape.shape_y2=centerY_circle;
		v_shape.shape_diameter=diameter_circle;
		v_shape.shape_width = EnclosedRec_Width;
		v_shape.shape_height = EnclosedRec_Height;
		v_shape.shape_type= ATTRIBUTE_SHAPE;
		return v_shape;
	}
	 else if(!circle && rectangle){
		 if(diameter_circle<10){
			 return v_shape;
		 }
		 v_shape.shape_x1 = start_point.x;
		 v_shape.shape_y1 = start_point.y;
		 //fine_shapes[selected_index].shape_x2=mouseX+(0.5*fine_shapes[selected_index].shape_width);
		 //fine_shapes[selected_index].shape_y2=mouseY+(0.5*fine_shapes[selected_index].shape_height);
		 v_shape.shape_x2 = v_shape.shape_x1+(0.5*EnclosedRec_Width);//start_point.x;
		 v_shape.shape_y2 = v_shape.shape_y1+(0.5*EnclosedRec_Height);// start_point.y;
		 //v_shape.shape_x2 = start_point.x+(0.5*EnclosedRec_Width);
		 //v_shape.shape_y2 = start_point.y+(0.5*EnclosedRec_Height);
		 v_shape.shape_width = EnclosedRec_Width;
		 v_shape.shape_height = EnclosedRec_Height;
		 v_shape.shape_diameter=diameter_circle;
		 v_shape.shape_type = ENTITY_SHAPE;
		 return v_shape;
	}
	else if (circle && rectangle) {
		if((ThinnessRatio/18) < (0.85/ratioBetweenCHperimeterAndRectangleperimeter) && ratioBetweenTriangleAreaAndRectangleArea<0.3){
//alert("ThinnessRatio/18 = "+ThinnessRatio/18 +" \n0.85/ratioBetweenCHperimeterAndRectangleperimeter = "+0.85/ratioBetweenCHperimeterAndRectangleperimeter+"\nratioBetweenTriangleAreaAndRectangleArea = "+ratioBetweenTriangleAreaAndRectangleArea);
			if(diameter_circle<10){
				return v_shape;
			}
			v_shape.shape_x1 = centerX_circle;
			v_shape.shape_y1=centerY_circle;
			v_shape.shape_x2 = centerX_circle;
			v_shape.shape_y2=centerY_circle;
			v_shape.shape_diameter=diameter_circle;
			v_shape.shape_width = EnclosedRec_Width;
			v_shape.shape_height = EnclosedRec_Height;
			v_shape.shape_type= ATTRIBUTE_SHAPE;
			return v_shape;
		}
		else {
			if(diameter_circle<10){
 			 return v_shape;
 		 }
 		 v_shape.shape_x1 = start_point.x;
 		 v_shape.shape_y1 = start_point.y;
 		 //fine_shapes[selected_index].shape_x2=mouseX+(0.5*fine_shapes[selected_index].shape_width);
 		 //fine_shapes[selected_index].shape_y2=mouseY+(0.5*fine_shapes[selected_index].shape_height);
 		 v_shape.shape_x2 = v_shape.shape_x1+(0.5*EnclosedRec_Width);//start_point.x;
 		 v_shape.shape_y2 = v_shape.shape_y1+(0.5*EnclosedRec_Height);// start_point.y;
 		 //v_shape.shape_x2 = start_point.x+(0.5*EnclosedRec_Width);
 		 //v_shape.shape_y2 = start_point.y+(0.5*EnclosedRec_Height);
 		 v_shape.shape_width = EnclosedRec_Width;
 		 v_shape.shape_height = EnclosedRec_Height;
 		 v_shape.shape_diameter=diameter_circle;
 		 v_shape.shape_type = ENTITY_SHAPE;
 		 return v_shape;
		}
	}
}

	return v_shape;
}
