/**
 * [draw_constraint_menu A function used to fill in the constraints dropdown box in the GUI ]
 * @param  {Int} id [The attribute id]
 * @return {String}    [Menu to be drawn]
 */
function draw_constraint_menu(id,ranks_of_constraint){
	var menu="<select id=\"constrians_"+id+"\" multiple style=\"width:100%\" id=\""+id+"\">";
		menu+= "<option value=\""+-1+"\">NONE</option>";
	 for (var i = 0; i < SQLConstraints.length; i++)
	 {
		 if(!ranks_of_constraint.includes(SQLConstraints[i]))
	 		menu+= "<option value=\""+SQLConstraints[i]+"\">"+SQLConstraints[i]+"</option>";
		else
			menu+= "<option selected=\"selected\" value=\""+SQLConstraints[i]+"\">"+SQLConstraints[i]+"</option>";
	 }
 	 menu+= "</select>";
	 return menu;
}
/**
 * [draw_types_menu A function used to fill the types dropdown box in the GUI according to the SQL_engine.]
 * @param  {Int} id         [The attribute id]
 * @param  {String} SQL_engine [The SQL engine, in our case either oracle or mysql]
 * @return {string}            [menu to be drawn]
 */
function draw_types_menu(id, SQL_engine,rank_of_type){
	var menu="<select id=\"types_"+id +"\" style=\"width:100%\" id=\""+id+"\">";
	var selected_SQL_engine=MySQLDataTypes;
	if(SQL_engine=="oracle")
	{
		selected_SQL_engine=OracleDataTypes;
	}
	else if(SQL_engine=="mysql")
	{
			selected_SQL_engine=MySQLDataTypes;
	}
	else {
			selected_SQL_engine=SQLServerDataTypes;
	}
	menu+= "<option value=\""+""+"\">"+""+"</option>";
	 for (var i = 0; i < selected_SQL_engine.length; i++)
	 {
		 if(rank_of_type==i)
		 {
			 	menu+= "<option selected=\"selected\" value=\""+selected_SQL_engine[i]+"\">"+selected_SQL_engine[i]+"</option>";
		 }
		 else {
		 		menu+= "<option value=\""+selected_SQL_engine[i]+"\">"+selected_SQL_engine[i]+"</option>";
		 }

	 }
 	 menu+= "</select>";
	 return menu;
}
/**
 * [get_selected_sql_engine A helper function used to return the selected SQL SQL_engine]
 * @return {String} [the SQL_engine]
 */
function get_selected_sql_engine(){
 	return $( "#sql_engine" ).val();
}

/**
 * [get_selected_nosql_engine A helper function used to return the selected NOSQL // NOTE: SQL_engine]
 * @return {String} [the NOSQL_engine]
 */
function get_selected_nosql_engine(){
 	return $( "#nosql_engine" ).val();
}

function get_rank_type(attribute_type,sql_engine)
{
	var rank=-1;
	var selected_SQL_engine=MySQLDataTypes;
	if(sql_engine=="oracle")
	{
		selected_SQL_engine=OracleDataTypes;
	}
	else if(sql_engine=="mysql")
	{
			selected_SQL_engine=MySQLDataTypes;
	}
	else {
			selected_SQL_engine=SQLServerDataTypes;
	}
	for(var i=0;i<selected_SQL_engine.length;i++)
	{
		if(selected_SQL_engine[i]==attribute_type)
			return i;
	}
	return rank;
}
function getNeo4jdbValuesGUI(initialValues,relationsDetails,dataTypes, relations)
{
	 content = "<table style=\" border: none;color:black\">";
	 content+= "<tr>" + "<td/><td> <u>Node attributes</u></td><td/></tr>";
	 content+= "<tr style=\"height: 30px\" ></tr>";
	 content+= "<tr>" + "<td style=\"width:20%\">Node</td>" + "<td style=\"width:20%\">Attribute </td>"+ "<td style=\"width:20%\" > Initial value </td>"+"</tr>";
	 content+="<tr style=\"height: 30px\" ></tr>";
	 for (var i = 0; i < initialValues.length; i++)
	 {
		 if(initialValues[i].attributeOFEntityOrRelation!=ENTITY_SHAPE)
		 		continue;
		 var attribute_Initialvalue=initialValues[i].attribute_Initialvalue;
		 if(dataTypes!=null ){
			 var attribute_temp=searchDataTypes(initialValues[i],dataTypes);
			 	attribute_Initialvalue= attribute_temp.attribute_Initialvalue;
		 }
		//	attribute_Initialvalue= dataTypes[i].attribute_Initialvalue;
	 content+= "<tr>" + "<td>"+initialValues[i].entity_name +"</td>" + "<td>"+initialValues[i].attribute_name+"</td>"+ "<td><input value=\""+attribute_Initialvalue+"\" id=\""+ initialValues[i].entityName_attributeName+"_1_input_nosql\""+ "style=\"width:100px\"> </input></td>"+"</tr>";
	 content+="<tr style=\"height: 30px\" ></tr>";
	 }
	 content+= "<tr>" + "<td/><td> <u>Relation attributes</u></td><td/></tr>";
	 content+= "<tr style=\"height: 30px\" ></tr>";
	 content+= "<tr>" + "<td style=\"width:20%\">Relation</td>" + "<td style=\"width:20%\">Attribute </td>"+ "<td style=\"width:20%\" > Initial value </td>"+"</tr>";
	 content+="<tr style=\"height: 30px\" ></tr>";
	 for (var i = 0; i < initialValues.length; i++)
	 {
		 if(initialValues[i].attributeOFEntityOrRelation==ENTITY_SHAPE)
				continue;
		 var attribute_Initialvalue=initialValues[i].attribute_Initialvalue;
		 if(dataTypes!=null ){
				 var attribute_temp=searchDataTypes(initialValues[i],dataTypes);
				 	attribute_Initialvalue= attribute_temp.attribute_Initialvalue;
		 }
			//attribute_Initialvalue=dataTypes[i].attribute_Initialvalue;
	 content+= "<tr>" + "<td>"+initialValues[i].entity_name +"</td>" + "<td>"+initialValues[i].attribute_name+"</td>"+ "<td><input value=\""+attribute_Initialvalue+"\" id=\""+ initialValues[i].entityName_attributeName+"_1_input_nosql\""+ "style=\"width:100px\"> </input></td>"+"</tr>";
	 content+="<tr style=\"height: 30px\" ></tr>";
	 }
	 content+= "<tr>" + "<td/><td> <u>Relation details</u></td><td/></tr>";
	 content+= "<tr style=\"height: 30px\" ></tr>";
	 content+= "<tr>" + "<td style=\"width:20%\">Relation</td>" + "<td style=\"width:20%\">Start node </td>"+ "<td style=\"width:20%\" > Selected child if exists </td>"+"</tr>";
	 content+="<tr style=\"height: 30px\" ></tr>";
	 for(var i=0;i<relationsDetails.length;i++)
	 {
		content+= "<tr>" + "<td>"+relationsDetails[i].relation.shape_name +"<td>"+getconnectedEntitiesGUI(relationsDetails[i],i,relations)+"</td>"+"<td>"+getChildEntitiesGUI(relationsDetails[i],i,relations)+"</td>"+"</tr>";
		content+="<tr style=\"height: 30px\" ></tr>";
	 }

	 content+="<table>";
 	return content;
}
function getconnectedEntitiesGUI(relationsDetail,id,relations)
{
	var previousValue="";
	if(relations!=null)
	previousValue=	relations[id].directionFrom;
	var menu="<select id=select_connectedEntities_"+id+">";
	var temp="";
	menu+="<option value="+temp +">"+temp+"</option>";
	for (var i = 0; i < relationsDetail.connectedEntitiesToRelation.length; i++)
	 {
		 temp= relationsDetail.connectedEntitiesToRelation[i].shape_name;
		 if(temp!=previousValue)
		 {
					menu+="<option value="+temp +">"+temp+"</option>";
		 }
		else {
					menu+="<option value="+temp +" selected=\"selected\">"+temp+"</option>";
		}
	 }

	menu+="</select>";
	return menu;

}
function getChildEntitiesGUI(relationsDetail,id, relations)
{
	if (relationsDetail.connectedEntitiesToParentEntity.length==0)
	{
		var menu="<select disabled/>";
		return menu;
	}

	var previousValue="";
	if(relations!=null)
	previousValue=	relations[id].mainRole;
	var menu="<select id=select_connectedEntitiesToParentEntity_"+id+">";
	var temp="";
	menu+="<option value="+temp +">"+temp+"</option>";
	for (var i = 0; i < relationsDetail.connectedEntitiesToParentEntity.length; i++)
	 {
		 var temp= relationsDetail.connectedEntitiesToParentEntity[i].shape_name;
		 if(temp!=previousValue)
		 {
					menu+="<option value="+temp +">"+temp+"</option>";
		 }
		else {
					menu+="<option value="+temp +" selected=\"selected\">"+temp+"</option>";
		}
	 }

	menu+="</select>";
	return menu;

}

function getMongodbValuesGUI(initialValues, dataTypes)
{
	 content = "<table style=\" border: none;color:black\">";
	 content+= "<tr>" + "<td style=\"width:20%\">Collection</td>" + "<td style=\"width:20%\">Document </td>"+ "<td style=\"width:20%\" > Initial value </td>"+"</tr>";
	 content+="<tr style=\"height: 30px\" ></tr>";
	for (var i = 0; i < initialValues.length; i++)
	{
			 var attribute_Initialvalue=initialValues[i].attribute_Initialvalue;
			 if(dataTypes!=null )
			 {
				 var attribute_temp=searchDataTypes(initialValues[i],dataTypes);
				 	attribute_Initialvalue= attribute_temp.attribute_Initialvalue;
			 }
			 	//attribute_Initialvalue=dataTypes[i].attribute_Initialvalue;
 		 content+= "<tr>" + "<td>"+initialValues[i].entity_name +"</td>" + "<td>"+initialValues[i].attribute_name+"</td>"+ "<td><input value=\""+attribute_Initialvalue+"\" id=\""+ initialValues[i].entityName_attributeName+"_1_input_nosql\""+ "style=\"width:100px\"> </input></td>"+"</tr>";
 		 content+="<tr style=\"height: 30px\" ></tr>";
	}
	content+="<table>";
	return content;
}

/**
 * [draw_nosql_types_constraints_interface A function used to draw the table that contains the types and the constraints in the GUI.]
 * @param  {String} nosql_engine [the NOSQL_engine selected ]
 * @param  {Array} dataTypes [Array of the dataTypes saved in database earlier  ]
 */
 function draw_nosql_types_constraints_interface (nosql_engine,dataTypes){
	 nosql_engine=get_selected_nosql_engine();

	$.get( "/getSchemaDatatypes", { ID: project_id ,schemaName:nosql_engine} )
	.done(function( docs ) {
		if(docs=="Project not saved yet"){
			alert("Project is not saved yet, please save the project and add then add the schema details");
		}else{
		$("#nosql_types_constriants_options").empty();
		var content="";
		dataTypes=null;
		relations=null;
		if(docs.length>=1)
		{
			dataTypes=docs[0].dataTypes;
			relations=docs[0].relations;
		}
		if( nosql_engine=="mongodb")
		{
			var initialValues=ExtractAttributesForMongoDB(fine_shapes);
			content=getMongodbValuesGUI(initialValues,dataTypes);
			//here deve
			//content="waiting for mongodb values";
		}
		else {

			var initialValues=ExtractAttributesInitialValuesForNeo4j(fine_shapes);
			var relationsDetails=ExtractRelationsDetails(fine_shapes);
			content=getNeo4jdbValuesGUI(initialValues,relationsDetails,dataTypes,relations);
		}
	  $("#nosql_types_constriants_options").append( content );
	}});

	/*var content="";
	if( nosql_engine=="mongodb")
	{
		var initialValues=ExtractAttributesForMongoDB(fine_shapes);
		content=getMongodbValuesGUI(initialValues);
		//here deve
		//content="waiting for mongodb values";
	}
	else {
		//getNeo4jdbValuesGUI();
		content="waiting for Neo4j values";
	}
   $("#nosql_types_constriants_options").append( content );*/
	// $( "#nosql_engine" ).change(function() {
	//	 $("#nosql_types_constriants_options").empty();
	// nosql_engine= get_selected_nosql_engine();
	// draw_nosql_types_constraints_interface(nosql_engine,dataTypes);
	//});
}

/**
 * [draw_types_constraints_interface A function used to draw the table that contains the types and the constraints in the GUI.]
 * @param  {String} sql_engine [the SQL_engine selected ]
 * @param  {Array} dataTypes [Array of the dataTypes saved in database earlier  ]
 */
function draw_types_constraints_interface (sql_engine,dataTypes){
	 sql_engine=get_selected_sql_engine();

	$("#types_constriants_options").empty();
	$.get( "/getSchemaDatatypes", { ID: project_id ,schemaName:sql_engine} )
	.done(function( docs ) {
		if(docs=="Project not saved yet"){
			alert("Project is not saved yet, please save the project and add then add the schema details");
		}
		else{
		dataTypes=null;
		if(docs.length>=1)
			dataTypes=docs[0].dataTypes;
			var all_attributes = ExtractTypes(fine_shapes);
			var entity="";
			var content = "<table style=\" border: none;color:black\">";
			content+= "<tr>" + "<td style=\"width:20%\">Entity name</td>" + "<td style=\"width:20%\">Attribute name </td>"+ "<td style=\"width:20%\" > Attribute type </td>"+ "<td style=\"width:10%\"> Extra info</td>"+ "<td style=\"width:20%\"> Constraints</td>"+"<td style=\"width:10%\"> Extra info</td>"+"</tr>";
			for (var i = 0; i < all_attributes.length; i++)
			{
			 var previousValue=searchDataTypes(all_attributes[i],dataTypes);
			 rank_of_type=-1;
			 value_of_type_parameter="";
			 ranks_of_constraint=[];
			 value_of_constraint_parameter="";
			 if(previousValue!=null)
			 {
				 rank_of_type=get_rank_type(previousValue.attribute_type,sql_engine);
				 value_of_type_parameter=previousValue.attribute_type_parameters;
				 ranks_of_constraint=previousValue.attribute_constraints;
				 value_of_constraint_parameter=previousValue.attribute_constraints_parameters;
			 }
			 /*if(value_of_constraint_parameter.includes("\""))
			 {
				 value_of_constraint_parameter="\""+value_of_constraint_parameter+"\"";
				 alert(value_of_constraint_parameter);
			 }*/
			 var constraints_menu=draw_constraint_menu(i,ranks_of_constraint);//,rank_of_constraint,value_of_constraint_parameter);
			 var types_menu=draw_types_menu(i, sql_engine,rank_of_type);
				 if(entity!=all_attributes[i].entity_name)
				 {
					 entity= all_attributes[i].entity_name;
					 content+= "<tr>" + "<td>"+entity +"</td></tr>";
					 content+="<tr style=\"height: 30px\" ></tr>";
					 content+= "<tr>" + "<td></td>"+ "<td>"+all_attributes[i].attribute_name +"</td>" + "<td>"+types_menu+"</td>"+ "<td><input value=\""+value_of_type_parameter+"\" id=\""+ all_attributes[i].entityName_attributeName+"_1_input\""+ "style=\"width:100px\"> </input></td>"+"<td>"+constraints_menu+"</td>"+ "<td><input value= \""+value_of_constraint_parameter+"\" id=\""+ all_attributes[i].entityName_attributeName+"_2_input\""+"style=\"width:100px\"> </input></td>"+"</tr>";
				 }
				 else {
					 content+= "<tr>" + "<td></td>"+ "<td>"+all_attributes[i].attribute_name +"</td>" + "<td>"+types_menu+"</td>"+ "<td><input value=\""+value_of_type_parameter+"\" id=\""+ all_attributes[i].entityName_attributeName+"_1_input\""+ "style=\"width:100px\"> </input></td>"+"<td>"+constraints_menu+"</td>"+ "<td><input value= \""+value_of_constraint_parameter+"\" id=\""+ all_attributes[i].entityName_attributeName+"_2_input\""+"style=\"width:100px\"> </input></td>"+"</tr>";
				 }
					 content+="<tr style=\"height: 30px\" ></tr>";
			}
			content+="</table>";
			$("#types_constriants_options").append( content );
		//	$( "#sql_engine" ).change(function() {
		//		sql_engine= get_selected_sql_engine();
		//		draw_types_constraints_interface(sql_engine,dataTypes);
			//});
		}});
}

function sql_engine_change_handler()
{
	$("#sql_types_constriants_options").empty();
	sql_engine= get_selected_sql_engine();
	draw_types_constraints_interface(sql_engine,null);
}

function nosql_engine_change_handler()
{
	$("#nosql_types_constriants_options").empty();
	nosql_engine= get_selected_nosql_engine();
	draw_nosql_types_constraints_interface(sql_engine,null);
}
/**
 * [searchDataTypes A function used to search the attribute in the data types that is returned from the database, to fill the attribute details as it was saved before on the database.]
 * @param  {Object} attribute [The attribute details]
 * @param  {Array} dataTypes [The array of the datatypes]
 * @return {Object}           [The attribute details]
 */
function searchDataTypes(attribute,dataTypes){
	if(dataTypes==null)
		return attribute;
	for (var i = 0; i < dataTypes.length; i++) {
		if(attribute.entityName_attributeName == dataTypes[i].entityName_attributeName){
			((attribute.attribute_constraints=="PRIMARY KEY")?((!dataTypes[i].attribute_constraints.includes("PRIMARY KEY"))? dataTypes[i].attribute_constraints+="PRIMARY KEY":dataTypes[i].attribute_constraints):dataTypes[i].attribute_constraints);
			return dataTypes[i];
		}

	}
	return attribute;
}

function nosql_types_constriants_options_generate_btn_click(){
	nosql_engine=get_selected_nosql_engine();

		var SchemaExtracted ="";
		var initialValues=new Array();
		if(nosql_engine=="mongodb")
		{
			initialValues=ExtractAttributesForMongoDB(fine_shapes);
			for(var i=0;i<initialValues.length;i++)
			{
				var id="#"+initialValues[i].entityName_attributeName+"_1_input_nosql";
				var input=$(id);
				initialValues[i].attribute_Initialvalue=input.val();
				//alert(types[i].attribute_Initialvalue);
			}

			$.post( "/saveSchemaConfiguration", { ID: project_id, schemaName:nosql_engine /*,keys:primaryKeys */,dataTypes: initialValues,relation_extradetails:[]} )
			.done(function( result ) {
					if(result=="updated"){
						alert("Data types saved successfully");
					}
					else {
						alert("There was a problem during saving the data types.");
					}
				});
			SchemaExtracted= ExtractMongodbSchema(fine_shapes,initialValues);
		}
		else if(nosql_engine=="neo4j") {

				initialValues=ExtractAttributesInitialValuesForNeo4j(fine_shapes);
				var relationDetails=ExtractRelationsDetails(fine_shapes);
			for(var i=0;i<initialValues.length;i++)
			{
				var id="#"+initialValues[i].entityName_attributeName+"_1_input_nosql";
				var input=$(id);
				initialValues[i].attribute_Initialvalue=input.val();
				//alert(types[i].attribute_Initialvalue);
			}
			for(var i=0;i<relationDetails.length;i++)
			{
					 var id = "#select_connectedEntities_"+i;
					 var menu=$(id);
					 if(menu.val())
						relationDetails[i].directionFrom=menu.val();
					id = "#select_connectedEntitiesToParentEntity_"+i;
					menu=$(id);
					if(menu.val())
						relationDetails[i].mainRole=menu.val();
			}

			$.post( "/saveSchemaConfiguration", { ID: project_id, schemaName:nosql_engine /*,keys:primaryKeys */,dataTypes: initialValues,relation_extradetails:relationDetails} )
			.done(function( result ) {
					if(result=="updated"){
						alert("Data types saved successfully");
					}
					else {
						alert("There was a problem during saving the data types.");
					}
				});
			SchemaExtracted= ExtractNeo4jSchema(fine_shapes,initialValues,relationDetails);
		}
		$("#schema_textbox").text(SchemaExtracted);
		$("#schema_copy_btn").click(function()
		{
			selectText('schema_textbox');
			document.execCommand("Copy");
		});
		$("#schema_modal").modal();
		if(nosql_engine=="neo4j"){//not implemented yet
			$("#connect_to_db_btn").hide();
		}
		else{//not implemented yet
			$("#connect_to_db_btn").show();
		}
}

/**
 * [types_constriants_options_generate_btn_click A function used to take the user inputs in the types_constrints window and fill in the Types array.]
 */
function types_constriants_options_generate_btn_click(){
	var types=ExtractTypes(fine_shapes);
	//var primaryKeys=new Array();
	for (var i = 0; i < types.length; i++)
	{
		var type=$("#types_"+i);
		var constraint=$("#constrians_"+i);
		types[i].attribute_type=type.val();
	  types[i].attribute_type_parameters =	$("#"+types[i].entityName_attributeName+"_1_input").val();
		types[i].attribute_constraints=constraint.val();
		types[i].attribute_constraints_parameters=$("#"+types[i].entityName_attributeName+"_2_input").val();
		/*if(types[i].attribute_constraints.includes("PRIMARY KEY")){
			primaryKeys.push(types[i]);
		}*/
		//alert("constraint.val() = "+constraint.val()+" attribute_constraints_parameters= "+$("#"+types[i].attribute_name+"_2_input").val());
	}

	$.post( "/saveSchemaConfiguration", { ID: project_id, schemaName:get_selected_sql_engine() /*,keys:primaryKeys */,dataTypes: types,relation_extradetails:[]} )
	.done(function( result ) {
			if(result=="updated"){
				alert("Data types saved successfully");
			}
			else {
				alert("There was a problem during saving the data types.");
			}
		});

	  var SchemaExtracted = ExtractSQLschema_fn(fine_shapes,types);
		$("#schema_textbox").text(SchemaExtracted);
		$("#schema_copy_btn").click(function()
		{
			selectText('schema_textbox');
			document.execCommand("Copy");
		});
	$("#schema_modal").modal();
		$("#connect_to_db_btn").show();
}
/**
 * [extract_show_NOSQL_schema A function used to draw the window for the MongoDB in the GUI]
 * @param  {Array} fine_shapes [The shapes array]
 */
function extract_show_NOSQL_schema(fine_shapes){
 if(ValidateERDModel(fine_shapes)){
	  draw_nosql_types_constraints_interface("mongodb",[]);
	 $("#nosql_schema_types_constriants_modal").modal({
 		backdrop: 'static',
 		keyboard: false
 		});
	}
}
/**
 * [extract_show_Neo4j_schema A function used to draw the window for the MongoDB in the GUI]
 * @param  {Array} fine_shapes [The shapes array]
 */
function extract_show_Neo4j_schema(fine_shapes){
 if(ValidateERDModel(fine_shapes)){
	var SchemaExtracted = ExtractNeo4jSchema(fine_shapes,[],[]);
	$("#schema_textbox").text(SchemaExtracted);
	$("#schema_copy_btn").click(function()
	{
		selectText('schema_textbox');
		document.execCommand("Copy");
	});
	$("#schema_modal").modal();
	}
	sql_engine="neo4j";
}
/**
 * [extract_show_SQL_schema A function used to draw the types_constriants_options window ]
 * @param  {Array} fine_shapes [The shapes array]
 */
function extract_show_SQL_schema(fine_shapes){
	if(ValidateERDModel(fine_shapes))
	{
	draw_types_constraints_interface("mysql",[]);
	$("#schema_types_constriants_modal").modal({
		backdrop: 'static',
		keyboard: false
		});
	}
}
/**
 * [selectText A function used to select the script to copy it.]
 * @param  {Object} containerid [The container id]
 */
function selectText(containerid) {
			 if (document.selection) {
					 var range = document.body.createTextRange();
					 range.moveToElementText(document.getElementById(containerid));
					 range.select();
			 } else if (window.getSelection) {
					 var range = document.createRange();
					 range.selectNode(document.getElementById(containerid));
					 window.getSelection().removeAllRanges();
					 window.getSelection().addRange(range);
			 }
	 }
