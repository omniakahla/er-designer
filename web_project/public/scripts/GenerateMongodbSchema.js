/**
* <h1>ExtractMongodbSchema!</h1>
* Extract Mongodb script is used to extract MongoDB script as follows:
* Start with a normal entity which is neither parent entity nor weak entity. Then, we check the conneted objects:
* <ol>
* <li>Linked to attribute, then check the attribute type:</li>
* <ol>
* <li>If Structured attribute, then add it as embedded document.<\li>
* <li>If Multivalued attribute, then add it as array.<\li>
* <li>Otherwise, it is a normal attibute, add it as Field <\li>
* </ol>
* <li>Linked to Entity, then it should be a parent entity. So, we add it and its related attributes as embedded document in the child entity. <\li>
* <li>Linked to relation, then check the relationship type:<\li>
* <ol>
* <li>If it is a weak relation, then add the weak relation's attributes and the weak entities as embedded document <\li>
* <li>If it is a recursive relation, add the relation's attributes and a reference to the table as embedded document if it is 1..1 or 1..M<\li>
* <li>If it is a recursive relation, add the relation's attributes and a reference to the table as an array if it is a M..N relation<\li>
* <li>If it is 1..1 relation, add one table to the other as embedded document<\li>
* <li>If it is 1..M or N..M add a reference to the M table in the 1 table.<\li>
* <\ol>
* </ol>
*
* @author  Omnia Kahla
* @version 1.0
* @since   2018-10-05
*
*/
var checkedShapes=new Array();
var ParentEntities=new Array();
var relationsArray=new Array();//to save the M..N relation and the 1..1 relation so it is added to only one table not both
const TYPE="\"\"";
const ARRAYTYPE="\[\]";
const OBJECTTYPE="ObjectId";
/**
 * [ExtractMongodbSchema A function used to initialize the global array variables and to call the main function (check all entities)]
 * @param       {Array} shapes [The shapes array]
 * @param {Array} initialValues [An array of initialValues]
 */
function ExtractMongodbSchema(shapes,initialValues)
{
  var schema="";
  checkedShapes.splice(0);
  ParentEntities.splice(0);
  relationsArray.splice(0);
  schema = CheckAllEntites(shapes,initialValues);
  console.log(schema);
  return schema;
}
/**
 * [CheckAllEntites A function used to search for all entities to build a collection.]
 * @param       {Array} shapes [Shapes array]
 * @param {Array} initialValues [An array of initialValues]
 */
function CheckAllEntites(shapes,initialValues)
{
  var schema="";
  for (var i = 0; i < shapes.length; i++) {
    if((shapes[i].shape_type==ENTITY_SHAPE  ) && !(shapes[i].shape_name.toLowerCase().endsWith("_p") || checkedShapes.includes(shapes[i].id))){
      checkedShapes.push(shapes[i].id);
      var connectedObjects=GetConnectedObjects(shapes,shapes[i].id,ALL_SHAPES);
      schema+="db."+shapes[i].shape_name+".insertOne({";
      var generatedEntityName=shapes[i].shape_name;
      schema+=BuildACollection(shapes,connectedObjects,shapes[i],true,initialValues,generatedEntityName);
      schema+="});\n";
      }
  }
  return schema;
}
/**
 * [BuildACollection A function used to build a collection, which is the same as the tables in SQL]
 * @param       {Array} shapes           [Shapes array]
 * @param       {Array} connectedObjects [List of connected objects to the Entity]
 * @param       {Object} Entity           [The Entity we want to build its collection]
 * @param       {Boolean} AskForARelation  [A boolean var, if true  then we are adding the relation to the collection, Otherwise we don't extract the schema for this relation.]
 * @param {Array} initialValues [An array of initialValues]
 * @param {String} generatedEntityName [The generated entity name used for searching in initial values]
 */
function BuildACollection(shapes,connectedObjects,Entity,AskForARelation,initialValues,generatedEntityName)//AskForARelation is a boolean variable so if the fn is called from a relation we shouldn't ask about the relation again.
{
  var schema="",temp="";
  for(var i=0;i<connectedObjects.length;i++)
  {
    if(connectedObjects[i].shape_type==ENTITY_SHAPE && connectedObjects[i].shape_name.toLowerCase().endsWith("_p")){//it must be linked to a parent shape
      temp+=ExtractSchemaFromParentEntity(shapes,connectedObjects[i].id,initialValues,generatedEntityName+"_"+connectedObjects[i].shape_name);
    }
    else if(connectedObjects[i].shape_type==ATTRIBUTE_SHAPE){
      temp+=ExtractSchemaForAttribute(shapes,connectedObjects[i].id,initialValues,generatedEntityName);
    }
    else if(connectedObjects[i].shape_type==RELATION_SHAPE&&AskForARelation){//linked to a relation

      temp+=ExtractSchemaForRelation(shapes,connectedObjects[i].id,Entity,initialValues,generatedEntityName);
    }
      if(temp!=""){
        schema+=temp+",";
        temp="";
    }
  }
  if(schema.endsWith(",")){
    schema=schema.slice(0,-1);
  }
  return schema;
}
/**
 * [ExtractSchemaForRelation extract the sript for the relation]
 * @param       {Array} shapes     [Shapes array]
 * @param       {Int} relationId [The generated id in the shape object]
 * @param       {Object} Entity     [The entity object]
 * @param {Array} initialValues [An array of initialValues]
 * @param {String} generatedEntityName [The generated entity name used for searching in initial values]
 */
function ExtractSchemaForRelation(shapes,relationId,Entity,initialValues,generatedEntityName)
{
  var relation=shapes[SearchById(shapes,relationId)];
  var v_generatedEntityName=generatedEntityName+"_"+relation.shape_name;
  var schema=relation.shape_name+":{";
  var SchemaExtracted="";
  var AddAttributes=false;
  //var linkedObjects=GetConnectedObjects(shapes,relationId,ALL_SHAPES);
  var attributesList= GetConnectedObjects(shapes,relationId,ATTRIBUTE_SHAPE);
  var connectedEntities=GetConnectedObjects(shapes,relationId,ENTITY_SHAPE);
  var Lines= GetAllConnectedLines(shapes,relationId);
  var StartembeddedDoc=false, EndEmbeddedDoc=false;
  var line=getLinebetweenRelationAndEntity(shapes,relation.id,Entity.id);
  if(connectedEntities.length==1){//add embedded doc with the attributes of the relation and a ref to the same table.
    SchemaExtracted+=TransformRecursiveRelation(connectedEntities[0],shapes,initialValues,Lines,relation);//Entity.shape_name+"_id:"+/*OBJECTTYPE*/TYPE+",";
    SchemaExtracted+="}";
    return schema+SchemaExtracted;
  }
  else{
      for(var i=0;i<connectedEntities.length;i++){
          if(connectedEntities[i].id!=Entity.id){
            StartembeddedDoc=true;
            var ParentEntitySchemaExtracted="";
            var lineBetweenRelationAndOtherEntity = getLinebetweenRelationAndEntity(shapes,relation.id,connectedEntities[i].id);
          /*  if(connectedEntities[i].shape_name.toLowerCase().endsWith("_p")){
              ParentEntitySchemaExtracted+=connectedEntities[i].shape_name+":{";
              ParentEntitySchemaExtracted+=connectedEntities[i].shape_name+"_id:"+/*OBJECTTYPE*//*TYPE+",Type:"+TYPE;
              ParentEntitySchemaExtracted+="}";
            }*/
            if(!SearchInRelationArray(Entity.id,connectedEntities[i].id,relation.id)){
            /*if(relation.shape_line==DOUBLE_LINE ){
              var linkedObjectsOfEntity=GetConnectedObjects(shapes,connectedEntities[i].id,ALL_SHAPES);
              SchemaExtracted+=connectedEntities[i].shape_name+":{";
              SchemaExtracted+=BuildACollection(shapes,linkedObjectsOfEntity,false);
              SchemaExtracted+="}";
            }
            else*/ if (line.shape_name=="1..1"&&lineBetweenRelationAndOtherEntity.shape_name=="1..1"){//&& !SearchInRelationArray(Entity.id,connectedEntities[i].id,relation.id)){
              if(ParentEntitySchemaExtracted!=""){
                SchemaExtracted+=ParentEntitySchemaExtracted;
              }
              else{
                    var linkedObjectsOfEntity=GetConnectedObjects(shapes,connectedEntities[i].id,ALL_SHAPES);
                    SchemaExtracted+=connectedEntities[i].shape_name+":{";
                    var newGeneratedEntityName=v_generatedEntityName+"_"+connectedEntities[i].shape_name;
                    SchemaExtracted+=BuildACollection(shapes,linkedObjectsOfEntity,connectedEntities[i],false,initialValues,newGeneratedEntityName);
                    SchemaExtracted+="}";

              var OnetoOnerelation={entity1_id:Entity.id,
                                      entity2_id:connectedEntities[i].id,
                                      relation_id:relation.id
              };
              AddAttributes=true;
              if(!Entity.shape_name.toLowerCase().endsWith("_p")){
                relationsArray.push(OnetoOnerelation);
              }

            }
          }
            else if(lineBetweenRelationAndOtherEntity.shape_name=="1..*"){// && !SearchInRelationArray(Entity.id,connectedEntities[i].id,relation.id)){//so the other link either 1..* so it is a M..N or 1..1 so it is a 1..* relation
              if(ParentEntitySchemaExtracted!=""){
                SchemaExtracted+=ParentEntitySchemaExtracted;
              }
              else{
                SchemaExtracted+=connectedEntities[i].shape_name+"_id:"+/*OBJECTTYPE*/ARRAYTYPE;
                var ManytoManyrelation={entity1_id:Entity.id,
                                        entity2_id:connectedEntities[i].id,
                                        relation_id:relation.id
                };
                AddAttributes=true;
                if(!Entity.shape_name.toLowerCase().endsWith("_p")){
                relationsArray.push(ManytoManyrelation);
              }
            }
          }
        }
            //else if((lineBetweenRelationAndOtherEntity.shape_name=="1..*" && SearchInRelationArray(Entity.id,connectedEntities[i].id,relation.id))||(line.shape_name=="1..1"&&lineBetweenRelationAndOtherEntity.shape_name=="1..1"&& SearchInRelationArray(Entity.id,connectedEntities[i].id,relation.id))){//so the relation is already added to the other table we could skip safely.
            else if(SearchInRelationArray(Entity.id,connectedEntities[i].id,relation.id)){
              return "";
            }
          //else if(connectedEntities[i].shape_type==ENTITY_SHAPE&&connectedEntities[i].id==Entity.id){
          //  continue;
          //}
          //else if(SearchInRelationArray(Entity.id,connectedEntities[i].id,relation.id)){
          //AddAttributes=false;
        //  }
          else{///we will check the attributes later in the function
            break;
          }
        }
      }
  if(StartembeddedDoc&&SchemaExtracted!=""){
          SchemaExtracted+=",";
        }
     StartembeddedDoc=true;
     if(AddAttributes){


    for(var i=0;i<attributesList.length;i++){

        schemaForAttribute=ExtractSchemaForAttribute(shapes,attributesList[i].id,initialValues,v_generatedEntityName);
        if(schemaForAttribute.includes(","))
        {
          if(StartembeddedDoc){
            SchemaExtracted+="{";
            StartembeddedDoc=false;
          }
        }
        SchemaExtracted+=schemaForAttribute+",";
        if(i==attributesList.length-1){
          EndEmbeddedDoc=true;
        }
      }
    }
    }
      if(SchemaExtracted.endsWith(",")){
        SchemaExtracted=SchemaExtracted.slice(0,-1);
      }
      if(EndEmbeddedDoc&&!StartembeddedDoc){
        SchemaExtracted+="}";
      }
    if(SchemaExtracted==""){
      return "";
    }
    else {
      schema+=SchemaExtracted;
    }
  schema+="}";
  return schema;
}
/**
 * [ExtractSchemaForAttribute extracts the script for the attribute]
 * @param       {Array} shapes      [shapes array]
 * @param       {Int} AttributeId [The generated id in the shape object]
 * @param {Array} initialValues [An array of initialValues]
 * @param {String} generatedEntityName [The generated entity name is used for searching in the initial values]
 */
function ExtractSchemaForAttribute(shapes,AttributeId,initialValues,generatedEntityName)
{
  var Shape=shapes[SearchById(shapes,AttributeId)];
  var schema="";
  if(Shape.shape_line ==DOUBLE_LINE){//Multivalued attribute so it will be an array.
    schema+=Shape.shape_name+":"+ARRAYTYPE;
    return schema;
  }
  var listOfAttribute=GetConnectedObjects(shapes,AttributeId,ATTRIBUTE_SHAPE);
  if(listOfAttribute.length==0){//normal attribute as it is connected only to the Entity
    schema+=Shape.shape_name+":"+SearchTypeOfAttribute(generatedEntityName,Shape.shape_name,initialValues);//TYPE;
    return schema;
  }
  else {//structured attribute
    schema+=Shape.shape_name+":{";
    for(var j=0;j<listOfAttribute.length;j++)  {
      schema+=listOfAttribute[j].shape_name+":"+SearchTypeOfAttribute(generatedEntityName,Shape.shape_name+"_"+listOfAttribute[j].shape_name,initialValues);//TYPE;
        schema+=",";
    }
    if(schema.endsWith(",")){
      schema=schema.slice(0,-1);
    }
    schema+="}";
  }
  return schema;
}
/**
 * [SearchTypeOfAttribute A function used to search in the initialvalues array to return the initial value of attribute]
 * @param       {String} generatedEntityName    [The generated Entity name]
 * @param       {String} attribute_name [The attribute name]
 * @param       {Array} initialValues          [the initial values array]
 */
function SearchTypeOfAttribute(generatedEntityName,attribute_name,initialValues){
  for (var i = 0; i < initialValues.length; i++) {
    if(initialValues[i].entityName_attributeName==generatedEntityName+"_"+attribute_name){
      return initialValues[i].attribute_Initialvalue;
    }
  }
  return TYPE;
}
/**
 * [ExtractSchemaFromParentEntity extracts the script for the parent entity]
 * @param       {Array} shapes   [shapes array]
 * @param       {Int} EntityId [The generated id in the shape object]
 * @param {Array} initialValues [An array of initialValues]
 * @param {String} generatedEntityName [The generated entity name used for searching in initial values]
 */
function ExtractSchemaFromParentEntity(shapes,EntityId,initialValues,generatedEntityName)
{
  //var Done=false;
  //var schema="";
  var parentEntity=shapes[SearchById(shapes,EntityId)];
  //var v_generatedEntityName=generatedEntityName+"_"+parentEntity.shape_name;
/*
  for(var j=0;j<ParentEntities.length;j++)
  {
    if(ParentEntities[j].Entity_name==parentEntity.shape_name){
      schema+=ParentEntities[j].SchemaExtracted;
      Done=true;
    }
  }
  if(!Done){*/
    var schema_Extracted="";//parentEntity.shape_name+":{";
    schema_Extracted+=BuildACollection(shapes,GetConnectedObjects(shapes,parentEntity.id,ALL_SHAPES),parentEntity,true,initialValues,generatedEntityName);
    //schema_Extracted+="}";
    //schema+=schema_Extracted;
  /*  var ParentEntitySchema={Entity_name:parentEntity.shape_name,
                      SchemaExtracted:schema_Extracted};
    ParentEntities.push(ParentEntitySchema);*/
//  }
  return schema_Extracted;
}
/**
 * [TransformRecursiveRelation Transforms a recursive relationship]
 * @param {Object}Entity [The connected Entity]
 * @param       {Array} shapes         [The shapes Array]
 * @param       {Array} initialValues          [The initialValues Array]
 * @param       {Array} Lines          [The connected lines to the relationship]
 * @param       {Object} relation       [The relationship Object]
 * @return {String} [schema]
 */
function TransformRecursiveRelation(Entity,shapes,initialValues,Lines,relation){
  var schema="",AddImportantStmtsAttheEnd="";
  var generatedPK="" ;
  var roles=new Array();
  var Cardinalities=new Array();
  var connectedAttributes=GetConnectedObjects(shapes,relation.id,ATTRIBUTE_SHAPE);
  var mainRole="";
  for (var i = 0; i < Lines.length; i++) {
    if ((Lines[i].id_shape_1==relation.id && Lines[i].id_shape_2==Entity.id)||(Lines[i].id_shape_2==relation.id && Lines[i].id_shape_1==Entity.id)) {
      temp=Lines[i].shape_name.substr(9);//removes the keyword recursive
      temp=temp.trim();
      roles.push(temp.substr(0,temp.indexOf("(")));//added the first role
      Cardinalities.push(temp.substr(temp.indexOf("(")+1,4));//added the first Cardinality
      temp2=temp.substr(temp.indexOf(")")+1);
      roles.push(temp2.substr(0,temp2.indexOf("(")));//added the second role
      Cardinalities.push(temp2.substr(temp2.indexOf("(")+1,4));//added the second Cardinality
      break;
    }
  }
  if((Cardinalities[0]=="1..*" ||Cardinalities[0]=="0..*")&&(Cardinalities[1]=="1..*" ||Cardinalities[1]=="0..*") ){
    if(connectedAttributes.length==0){//it must be connected to attributes, so we will add it as an array of an embedded document of the attributes.
      schema+=Entity.shape_name+":"+ARRAYTYPE;
      return  schema;
    }
  }
  else{
      if(Cardinalities[0]=="1..1"){//||Cardinalities[0]=="0..*"){
        mainRole=roles[0];
      }
      else {
        mainRole=roles[1];
      }
      schema+=mainRole+":"+TYPE;
    }
    /*if(connectedAttributes.length>0){
      for(var i=0;i<connectedAttributes.length;i++){
        schema+=ExtractSchemaForAttribute(shapes,connectedAttributes[i].id,initialValues);
      }
    }*/
  return schema;
}

function getLinebetweenRelationAndEntity(shapes,relationID,entityID)
{
  for (var i = 0; i < shapes.length; i++) {
    if(shapes[i].shape_type=="Line")
    {
      if((shapes[i].id_shape_1 == relationID && shapes[i].id_shape_2 == entityID) || (shapes[i].id_shape_2 == relationID && shapes[i].id_shape_1 == entityID)){
        return shapes[i];
      }
    }
  }
}
/**
 * [SearchInRelationArray A function used to search in the global variable relationsArray, so if the relation is already added to one collection, it is not added to the other. This should be done for the 1..1 and M..N relations]
 * @param       {Int} Entity1_Id [The generated id in the shape object]
 * @param       {Int} Entity2_Id [The generated id in the shape object]
 * @param       {Int} relation_Id [The generated id in the shape object]
 */
function SearchInRelationArray(Entity1_Id,Entity2_Id,relation_Id)
{
  for(var i=0;i<relationsArray.length;i++){
    if(relationsArray[i].relation_id == relation_Id){
      if((relationsArray[i].entity1_id==Entity1_Id && relationsArray[i].entity2_id == Entity2_Id)||(relationsArray[i].entity1_id==Entity2_Id && relationsArray[i].entity2_id == Entity1_Id)){
        return true;
        }
      }
    }
    return false;
}
function searchInParentEntities(parentEntity){
  for (var i = 0; i < ParentEntities.length; i++) {
    if(ParentEntities[i].Entity_name==parentEntity.shape_name){
      return i;
    }
  }
  return -1;
}
