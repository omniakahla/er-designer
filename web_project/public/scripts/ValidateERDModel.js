/**
* <h1>Validate ERD Model!</h1>
* Validate ERD Model is used to validate the diagram as follows:
* <ol>
* <li>If the line between an attribute and any other object or between 2 entities, then it should be a solid line without cardinality</li>
* <li>If the line between an entity and a relation, then it should have a cardinality.</li>
* <li>checks the attribute/entity/relation Name if missing</li>
*  <li> if the attribute is connected to more than one relation or more than one entity or is connected to an entity and a relation, so it is incorrect Otherwise it is ok.</li>
*  <li>checks if the attribute is not linked to any object, it is incorrect.</li>
*  <li>checks if the attribute is connected to another attribute, we have to Validate the other attribute also.</li>
* <li>If there are 2 entities with the same name, it is incorrect</li>
* <li>If there are 2 entities connected to each other, then there should be one of them contains "_p" at the end of its name to indicate the parent entity.</li>
*  <li>If the entity is connected to 2 attributes with the same name, so it is incorrect</li>
*  <li>checks if the parent entity is connected to child entities or not, if not then a confirmation message appear to alert the end-user that he didn't connect the parent entity to child entities. </li>
*  <li>checks if an entity is created without linking to any object, then a confirmation message appear to alert the end-user that the entity is a stand alone entity and not connected to any object.</li>
*  <li>We cannot have a weak entity and at the same time a parent entity.</li>
* <li>We cannot have a weak entity that is not connected to a weak relation.</li>
* <li> We cannot have a relation that is not connected to any object</li>
* <li> if the relation is linked to 2 attributes with the same name</li>
* <li>We cannot have a relation that is only connected to a single entity and the relationsh type is not recursive</li>
* <li>We cannot have a weak relation that is only connected to weak entities, we must specify at least one normal entity.</li>
* <li>The weak relation should be linked to a weak entity with any cardinality 1..M. Please refer to the following link. https://www.youtube.com/watch?v=yQ4SYjMz7wk </li>
* <li>The weak relation should be linked to the strong entity with cardinality 1..M or 1..1. Please refer to the following link. https://www.youtube.com/watch?v=yQ4SYjMz7wk </li>
* <li>We cannot have a relation that is connected to entity with a recursive relation and is connected to any entity.</li>
* </ol>
*
* @author  Omnia Kahla
* @version 1.0
* @since   2018-10-05
*
*/
var entityNames=new Array();
var attributesIds = new Array();
/**
 * ValidateERDModel A function used to validate the ER diagram.
 * @param       {Array} shapes An array of shapes to be validated
 * @return {boolean} Indicates whether the validation completed successfully or not
 */
function ValidateERDModel(shapes)
{
  entityNames.splice(0);//removes all elements
  attributesIds.splice(0);//remove all attributes

  var ret=false;
  ret=  ValidateConnection(fine_shapes);
  entityNames.splice(0);
  if(ret)
  {
    attributesIds.splice(0);
    ret =ValidateShapes(fine_shapes);
  }

//  if(ret)
//  confirm("Validation has been completed and there is no problem. ");
  return ret;
}
/**
 * [ValidateShapes Searches for each shape and send it to its corresponding function to be validated]
 * @param       {Array} shapes [An array of shapes to be validated]
 * @return {boolean} Indicates whether the validation completed successfully or not
 */
function ValidateShapes(shapes)
{
  var ret=false;
  for (var i=0;i<shapes.length;i++)
  {
    if(shapes[i].shape_type==ATTRIBUTE_SHAPE)
    {
      ret=ValidateAttribute(shapes,shapes[i].id);
     if (!ret)
     return false;
       //break;
    }
    else if(shapes[i].shape_type==ENTITY_SHAPE)
    {
       ret=ValidateEntity(shapes,shapes[i].id);
      if (!ret)
      return false;
        //break;
    }
    else if(shapes[i].shape_type ==RELATION_SHAPE)
    {
      ret=  ValidateRelation(shapes,shapes[i].id);
      if(!ret)
      return false;
        //break;
    }
  }
  return true;
}
/**
 * ValidateAttribute Validate the attribute as follows:
 * <ol>
*  <li>checks the attribute Name</li>
*  <li>checks if the attribute is connected to more than one object, for example if the attibute is connected to 2 entities or 2 relations or 1 entity and 1 relation, so it is incorrect, Otherwise it is ok.</li>
*  <li>checks if the attribute is not linked to any object, it is incorrect.</li>
*  <li>checks if it is connected to another attribute, Validate the other attribute also.</li>
*</ol>
*
 * @param       {Array} shapes       [The shapes array is send to check the connected objects for this attribute]
 * @param       {int} id [Index of the shape is used to be able to find the shape in the shapes array]
 * @return {boolean} return true to indicate that the validation completed successfully
 */
function ValidateAttribute(shapes,id)
{
  var attribute=shapes[SearchById(shapes,id)];
  var numOfEntities=0;
  var numOfRelations=0;
  if(attributesIds.includes(attribute.id))
  {
    return true;
  }
  else {
    attributesIds.push(attribute.id);
  }
  if(attribute.shape_name =="")
  {
    alert("You have to rename all the attributes.");
    return false;
  }
  var linkedObjects=GetConnectedObjects(shapes,attribute.id,ALL_SHAPES);
  if(linkedObjects.length==0)
  {
    alert("Cannot have an attribute that is not linked to any object. ");
    return false;
  }
  else {
    for (var i = 0; i < linkedObjects.length; i++) {
      var LinkedShape=linkedObjects[i];
      if(/*shapes[linkedObjects[i]]*/LinkedShape.shape_type==ENTITY_SHAPE)
        numOfEntities++;
      else if(/*shapes[linkedObjects[i]]*/LinkedShape.shape_type==RELATION_SHAPE)
        numOfRelations++;
    }
    if(numOfEntities==0 && numOfRelations==0 )
    {
      attributesIds.pop();
      return true;
    }
    /////check if the attribute is linked to more than one entiy or relation
    if((numOfEntities>1 || numOfRelations>1) || (numOfEntities+numOfRelations)>1)
    {
      alert("Cannot link the same attribute \""+attribute.shape_name+"\" to more than entity or more than relation or to a relation and an entity.");
      return false;
    }
    else if((numOfEntities+numOfRelations)!=linkedObjects.length)//then it is linked to attributes also
    {
      for (var i = 0; i < linkedObjects.length; i++) {
        var LinkedShape=linkedObjects[i];
        if(/*shapes[linkedObjects[i]]*/LinkedShape.shape_type==ATTRIBUTE_SHAPE)
        {
          if(attributesIds.includes(/*shapes[linkedObjects[i]]*/LinkedShape.id) && (numOfEntities+numOfRelations)>0)
          {
            return false;
          }
          var temp= ValidateAttribute(shapes,/*linkedObjects[i]*/LinkedShape.id);
          if(!temp)
          {
           // the attribute is linked to an entity and to another attribute that is also linked to another entity.
           alert("You cannot link the attribute \""+attribute.shape_name+"\" to another attribute \""+/*shapes[linkedObjects[i]]*/LinkedShape.shape_name +"\" that is also linked to another entity or relation.");
           return false;
          }
        }
      }
    }
  }
  return true;
}
/**
* ValidateConnection Validates the line between objects as follows:
* <ol>
* <li>If the line between an attribute and any other object or between 2 entities, then it should be a solid line without cardinality</li>
* <li>If the line between an entity and a relation, then it should have a cardinality.</li>
*</ol>
 @param       {Array} shapes       [The shapes array is send to check the connected objects for the line]
* @return {boolean} return true to indicate that the validation completed successfully
 */
function ValidateConnection(shapes)
{
  var shape1,shape2;
  var CurrentLine;
  for (var i=0; i<shapes.length;i++)
  {
    CurrentLine=shapes[i];
    if(CurrentLine.shape_type =="Line")
    {
      shape1=shapes[SearchById(shapes,CurrentLine.id_shape_1)];
      shape2=shapes[SearchById(shapes,CurrentLine.id_shape_2)];
       if (shape1.shape_type == RELATION_SHAPE && shape2.shape_type == RELATION_SHAPE)//both are diamonds
       {
         alert("Cannot link between 2 relationships.");//+shapes[shapes[i].index_shape_1].shape_name+ " and "+shapes[shapes[i].index_shape_2]);
         return false;
       }
       else if (shape1.shape_type ==ATTRIBUTE_SHAPE|| shape2.shape_type==ATTRIBUTE_SHAPE)
       {
         if (CurrentLine.shape_name!="")
         {
           alert("Cannot link between attributes and any object with any type of cardinality, You can use a solid line only.");
           return false;
         }
       }
       else if (shape1.shape_type ==ENTITY_SHAPE && shape2.shape_type==ENTITY_SHAPE)
        {
          if(CurrentLine.shape_name!="")
          {
            alert("You cannot add a \""+CurrentLine.shape_name+"\" relation between 2 entities, you can use the solid line only between 2 entities.");
            return false;
          }
          if(!(shape1.shape_name.toLowerCase().endsWith("_p")||shape2.shape_name.toLowerCase().endsWith("_p")))
          {
              alert("You must specify the parent entity in the sub-super type relationship. This could be done by adding _p or _P in the parent entity name.");
            return false;
          }
          if(shape1.shape_name.toLowerCase().endsWith("_p")&& shape2.shape_name.toLowerCase().endsWith("_p"))
          {
              alert("You must specify the parent entity in the sub-super type relationship. This could be done by adding _p or _P at the end of the parent entity name but not in both.");
            return false;
          }
        }
        else if(CurrentLine.shape_name =="")
         {
           alert("Cannot link between an entity and a relation with the solid line, Please specify the relationship type. ");
           return false;
         }

    }
  }
  return true;
}
//to check each entity alone first
/**
 * ValidateEntity Validates the entity and its connected objects as follows:
 * <ol>
 * <li>checks the entity name, if it is empty it is incorrect</li>
 * <li>If there are 2 entities with the same name, it is incorrect</li>
* <li>If there are 2 entities connected to each other, then there should be one of them contains "_p" at the end of its name to indicate the parent entity.</li>
*  <li>Check if it is connected to 2 attributes with the same name, it returns false</li>
*  <li>checks if the parent entity is connected to child entities or not, if not then a confirmation message appear </li>
*  <li>checks if an entity is created without linking to any object, then a confirmation message appear</li>
*  <li>checks if there is a weak entity that is also a parent entity, if yes then return false</li>
*  <li>checks if there is a weak entity that is not connected to a weak relation, if yes then return false.</li>
 * </ol>
 * @param       {Array} shapes       [The shapes array is send to check the connected objects for the line]
 * @param       {int} id [Index of the shape is used to be able to find the shape in the shapes array]
 * @return  {boolean} return true to indicate that the validation completed successfully
 */
function ValidateEntity(shapes,id)
{

  var attributesList=new Array();
  var numOfEntities=0, numOfRelations=0;
  var weakrelations=0;
  var Entity=shapes[SearchById(shapes,id)];
  var parentEntity=false;
  if(entityNames.includes(Entity.shape_name.toLowerCase()))
  {
    alert("You cannot have 2 entities with the same name \""+Entity.shape_name+"\" , please differntiate between them.");
    entityNames.splice(0);
    return false;
  }
  else {
    entityNames.push(Entity.shape_name.toLowerCase());
  }
  if(Entity.shape_name=="")
  {
    alert("You have to rename all entities");
    return false;
  }
  var linkedObjects=GetConnectedObjects(shapes,Entity.id,ALL_SHAPES);
  for(var i=0;i<linkedObjects.length;i++)
  {
    var LinkedShape =linkedObjects[i]; //= shapes[linkedObjects[i]];
    if(LinkedShape.shape_type==ENTITY_SHAPE)
    {
      numOfEntities++;
      if(Entity.shape_name.toLowerCase().endsWith("_p"))
        parentEntity = true;
    }
    else if(LinkedShape.shape_type==ATTRIBUTE_SHAPE)
    {
      if(attributesList.includes(LinkedShape.shape_name.toLowerCase()))
      {
        alert("You cannot name 2 attributes with the same name \""+LinkedShape.shape_name+"\" for the same entity \""+Entity.shape_name+"\".");
        return false;
      }
      else {
        attributesList.push(LinkedShape.shape_name.toLowerCase());
      }
    }
    else if(LinkedShape.shape_type==RELATION_SHAPE)
    {
        numOfRelations++;
       if(LinkedShape.shape_line==DOUBLE_LINE)
        {
          weakrelations++;
        }
    }
  }
  if(parentEntity && numOfEntities==0)
  {
    var x =confirm("Are you sure that you do not want to connect the parent entity "+Entity.shape_name+" with sub entities?");
    if (!x)
      return false;
  }
  if(numOfRelations==0 && numOfEntities==0)
  {
      var x= confirm("Are you sure that you want to create the entity "+Entity.shape_name+" without linking to any object?");
      if(!x)
      return false;
  }
  if(Entity.shape_line==DOUBLE_LINE && parentEntity)
  {
    alert("Cannot have a weak entity that is also a parent entity");
    return false;
  }
  if(Entity.shape_line==DOUBLE_LINE && weakrelations==0)
  {
    alert("You must specify at least one weak relation for the weak entity \""+LinkedShape.shape_name+"\"");
    return false;
  }
  return true;
}
/**
 * ValidateRelation Validate the relation and its connected objects and return false in case:
 * <ol>
 * <li>Checks the relation name</li>
 * <li> if the relation is not connected to any object</li>
 * <li> if the relation is linked to 2 attributes with the same name</li>
 * <li>if it is a weak entity and not linked to a weak relation</li>
 * <li>if it is connected to a single entity and the relationship is not recursive</li>
 * <li>if the relation is weak relation and all entities are weak entities</li>
 * <li>if the weak relation is linked to a weak entity with any cardinality not 1..M</li>
 * <li>if the weak relation linked with the strong entity with any cardinality not 1..M or 1..1 </li>
 * <li>if there is a recursive that is connected to more than one entity</li>
 *  </ol>
 * @param       {Array} shapes [The shapes array is send to check the connected objects for the line]
 * @param       {int} id    [Index of the shape is used to be able to find the shape in the shapes array]
 * @return  {boolean} return true to indicate that the validation completed successfully
 */
function ValidateRelation(shapes,id)
{
  var attributesList=new Array();
  var weakentities= 0, numOfEntities=0;
  var relation = shapes[SearchById(shapes,id)];
  if(relation.shape_name=="")
  {
    alert("You have to rename all relations");
    return false;
  }
  var linkedObjects=GetConnectedObjects(shapes,relation.id,ALL_SHAPES);
  if(linkedObjects.length==0)
  {
    alert("Cannot create relations without linking to an entity");
    return false;
  }
  var lines=new Array();
  for(var i=0;i<linkedObjects.length;i++)
  {
    var LinkedShape=linkedObjects[i];// shapes[linkedObjects[i]];
    if(LinkedShape.shape_type==ATTRIBUTE_SHAPE)
    {
      if(attributesList.includes(LinkedShape.shape_name.toLowerCase()))
      {
        alert("You cannot name 2 attributes with the same name \""+LinkedShape.shape_name+"\" for the same relation.");
        return false;
      }
      else {
        attributesList.push(LinkedShape.shape_name.toLowerCase());
      }
    }
    else if(LinkedShape.shape_type==ENTITY_SHAPE)
    {
      numOfEntities++;
      lines.push(GetLineConnection(shapes,LinkedShape.id,relation.id));
       if(LinkedShape.shape_line==DOUBLE_LINE)
        {
          weakentities++;
        }
    }
  }
  if(relation.shape_line==DOUBLE_LINE && weakentities==0)
  {
    alert("You must specify at least one weak entity for the weak relationship \""+LinkedShape.shape_name+"\"");
    return false;
  }
  if(numOfEntities==1 && !lines[0].shape_name.startsWith(RECURSIVE_RELATION))
  {
    alert("The relation \""+relation.shape_name+"\" has only one entity linked so the relation should be recursive not \""+ lines[0].shape_name+"\"");
    return false;
  }
  if(relation.shape_line==DOUBLE_LINE && weakentities==numOfEntities)
  {
    alert("You cannot have a weak relation \""+relation.shape_name+"\" that has all connected entities weak entities, at least you should specify one entity as a stong entity");
    return false;
  }
  if(lines.length>0)
  {
    var recursiverelation=false;
    for (var i = 0; i < lines.length; i++) {
      if(lines[i].shape_name==RECURSIVE_RELATION)
        recursiverelation=true;
      if(relation.shape_line==DOUBLE_LINE)
      {
        var shape1=shapes[SearchById(shapes,lines[i].id_shape_1)];
        var shape2=shapes[SearchById(shapes,lines[i].id_shape_2)];
        if((shape1.id ==relation.id && shape2.shape_line==DOUBLE_LINE)||(shape2.id ==relation.id && shape1.shape_line==DOUBLE_LINE))
        {//links with a weak entity should be 1..M as per this link https://www.youtube.com/watch?v=yQ4SYjMz7wk
          if(lines[i].shape_name != "1..*")
          {
              alert("The weak relation \""+relation.shape_name+"\" should be linked with the weak entity with a 1..M cardinality");
            return false;
          }
        }
        else if(lines[i].shape_name == "0..*" || lines[i].shape_name == "0..1"){
          alert("The weak relation \""+relation.shape_name+"\" should be linked with the strong entities with a 1..M or 1..1 cardinality but not \""+lines[i].shape_name+"\"");
        return false;
      }
      }
    }
    if(recursiverelation&& lines.length>1)
    {
      alert("Cannot link between a recursive relation \""+relation.shape_name+"\" and more than one entity.");
      return false;
    }
  }
  return true;
}
/**
 * [GetLineConnection description]
 * @param       {Array} shapes [The shapes array is send to check the connected objects for the line]
 * @param       {int} shape1_id [The first connected shape]
 * @param       {int} shape2_id [The second connected shape]
 * @return {shape} returns the line that connect between the 2 shapes.
 */
function GetLineConnection(shapes,shape1_id,shape2_id)
{
  for (var i = 0; i < shapes.length; i++) {
    if(shapes[i].shape_type=="Line" && ((shapes[i].id_shape_1==shape1_id && shapes[i].id_shape_2==shape2_id)||(shapes[i].id_shape_1==shape2_id && shapes[i].id_shape_2==shape1_id)))
      return shapes[i];//return line that connects between 2 shapes.
  }
}
//return an array that contains all the connected shapes to specific shape.
/*function GetConnectedObjects(shapes, id,Type)
{
  var shapes_indexes=new Array();
  var currentshape;
  for (var i = 0; i <shapes.length; i++)
	{
    currentshape=shapes[i];
		if(currentshape.shape_type=="Line")
		{
			if(currentshape.id_shape_1== id )
        shapes_indexes.push(SearchById(shapes,currentshape.id_shape_2));
			else if (shapes[i].id_shape_2==id)
        shapes_indexes.push(SearchById(shapes,currentshape.id_shape_1));
		}
	}
  return shapes_indexes;
}*/

/*function SearchById(shapes,id)
{
  for (var i=0;i<shapes.length;i++)
  {
    if(shapes[i].id==id)
    return i;
  }
}*/
