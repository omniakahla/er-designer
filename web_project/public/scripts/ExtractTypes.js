var attributesIds= new Array();
var schema_types_array=new Array();
var checkedShapes=new Array();
/**
 * [ExtractTypes A function used to search for entities and its related attributes to show to the end-user  ]
 * @param       {Array} shapes shapes Array
 * @return {Array} returns an array for all entities and its related attributes
 */
function ExtractTypes(shapes){
  attributesIds.splice(0);
  checkedShapes.splice(0);
  schema_types_array.splice(0);
  var currentShape;
  var connectedObjects=new Array();
  for(var i=0; i<shapes.length;i++){
  currentShape=shapes[i];
  if(currentShape.shape_type !=LINE_SHAPE){
    if(currentShape.shape_type ==ENTITY_SHAPE ){
      addEntitytoSchemaTypes(shapes,currentShape);
    }
    else if(currentShape.shape_type==RELATION_SHAPE){
        addRelationToSchemaTypes(shapes,currentShape);
      }
    }
  }
      return schema_types_array;
}
/**
 * [addRelationToSchemaTypes A function used to add a relation in the types array if it has linked attributes ]
 * @param {Array} shapes       shapes array
 * @param {Object} currentShape [The relation object]
 */
function addRelationToSchemaTypes(shapes,currentShape){
  if(!checkedShapes.includes(currentShape.id))
  {
    checkedShapes.push(currentShape.id);
    var connectedObjectstoRelation=getAllConnectedObjects(shapes,currentShape.id);
    var lines= new Array();
    var newTable=false;
    for (var i = 0; i < connectedObjectstoRelation.length; i++) {
      if(connectedObjectstoRelation[i].shape_type==ATTRIBUTE_SHAPE && connectedObjectstoRelation[i].shape_line==DOUBLE_LINE){//connected to a Multivalued attribute
        checkedShapes.push(connectedObjectstoRelation[i].id);
        addMultiAttributeToSchemaTypes(connectedObjectstoRelation[i],currentShape);
        newTable=true;
      }
      else if(connectedObjectstoRelation[i].shape_type==ATTRIBUTE_SHAPE){//a normal attribute
        checkedShapes.push(connectedObjectstoRelation[i].id);
        addAttributeToSchemaTypes(shapes,connectedObjectstoRelation[i],currentShape);
        newTable=true;
      }
    }
  }
}
/**
 * [addEntitytoSchemaTypes A function used to add the entity and all its connected attributes to the types array]
 * @param {Array} shapes       shapes array
 * @param {Object} currentShape [The entity object]
 */
function addEntitytoSchemaTypes(shapes,currentShape){
    if(!checkedShapes.includes(currentShape.id))
    {
      checkedShapes.push(currentShape.id);
    var connectedObjectstoEntity=getAllConnectedObjects(shapes,currentShape.id);
    for (var i = 0; i < connectedObjectstoEntity.length; i++) {
      if(connectedObjectstoEntity[i].shape_type==ATTRIBUTE_SHAPE && connectedObjectstoEntity[i].shape_line==DOUBLE_LINE){//connected to a Multivalued attribute
        addMultiAttributeToSchemaTypes(connectedObjectstoEntity[i],currentShape);
      }
      else if(connectedObjectstoEntity[i].shape_type==ATTRIBUTE_SHAPE){//a normal attribute
        addAttributeToSchemaTypes(shapes,connectedObjectstoEntity[i],currentShape);
      }
      else if(connectedObjectstoEntity[i].shape_type==RELATION_SHAPE){//It is connected to a relation
        addRelationToSchemaTypes(shapes,connectedObjectstoEntity[i]);
      }
    }
  }
}
/**
 * [addAttributeToSchemaTypes A helper function to add the attribute to the types array according to the current shape either relation or entity]
 * @param {Array} shapes       Shapes array
 * @param {Object} attribute   The attribute object
 * @param {Object} currentShape The entity or the relation object
 */
function addAttributeToSchemaTypes(shapes,attribute,currentShape){

  var connectedAttributes=getAllattributestoStructuredAttribute(shapes,attribute.id);
    /*if(connectedAttributes.length>0)
    {
      */for (var i = 0; i < connectedAttributes.length; i++) {
        var schema_types = {
          entity_name:"",//used in the UI
          attribute_name: "",//used in the UI
          attribute_type:"",//returned from UI
          attribute_type_parameters:"",
          attribute_constraints:"",//returned from UI
          attribute_constraints_parameters:"",
//  attribute_defaultvalue:"",//returned from UI
          entityName_attributeName:""//used for searching
        };
        schema_types.entity_name=currentShape.shape_name;
        schema_types.attribute_name=connectedAttributes[i];
        schema_types.entityName_attributeName=schema_types.entity_name+"_"+schema_types.attribute_name;
        if(attribute.key==1 || attribute.key==0) //if it is a primaryKey or partialKey then add the constraint primaryKey
        {
          schema_types.attribute_constraints="PRIMARY KEY";
        }
        schema_types_array.push(schema_types);
        /*console.log("{"+
          "entity_name: "+ schema_types.entity_name+","+//used in the UI
          "attribute_name: "+schema_types.attribute_name+","+//used in the UI
          "attribute_type: \"\","+//returned from UI
          "attribute_type_parameters:\"\","+
          "attribute_constraints: \"\","+//returned from UI
          "attribute_constraints_parameters: \"\","+
          "entityName_attributeName:\"\"}"//used for searching
      );*/
      }
    /*}
    else {
      schema_types.entity_name=currentShape.shape_name;
      schema_types.attribute_name=attribute.shape_name;
      schema_types.entityName_attributeName=schema_types.entity_name+"_"+schema_types.attribute_name;
      schema_types_array.push(schema_types);
    }*/

}
/**
 * [addMultiAttributeToSchemaTypes Add a Multiattribute value in the types array]
 * @param {Object} Multivaluedattribute [The Multivalued attribute]
 * @param {Object} Entity               [The Entity attribute]
 */
function addMultiAttributeToSchemaTypes(Multivaluedattribute,Entity){
  var schema_types = {
    entity_name:"",//used in the UI
    attribute_name: "",//used in the UI
    attribute_type:"",//returned from UI
    attribute_type_parameters:"",
    attribute_constraints:"",//returned from UI
    attribute_constraints_parameters:"",
  //  attribute_defaultvalue:"",//returned from UI
    entityName_attributeName:""//used for searching
  };
  schema_types.entity_name=Entity.shape_name+"_"+Multivaluedattribute.shape_name;
  schema_types.attribute_name=Multivaluedattribute.shape_name;
  schema_types.entityName_attributeName=schema_types.entity_name+"_"+schema_types.attribute_name;
  schema_types_array.push(schema_types);
  /*console.log("{"+
    "entity_name: "+ schema_types.entity_name+","+//used in the UI
    "attribute_name: "+schema_types.attribute_name+","+//used in the UI
    "attribute_type: \"\","+//returned from UI
    "attribute_type_parameters:\"\","+
    "attribute_constraints: \"\","+//returned from UI
    "attribute_constraints_parameters: \"\","+
    "entityName_attributeName:\"\"}"//used for searching
);*/
}
/**
 * [getConnectedLine a helper function to return the line that connects between 2 objects]
 * @param  {Array} shapes          shapes Array
 * @param  {Object} currentShape    [1st Object]
 * @param  {Object} connectedObject [2nd Object]
 * @return {Object}                 [return the line that connects the 2 objects]
 */
function getConnectedLine(shapes,currentShape, connectedObject){
  var line;
  for (var i = 0; i < shapes.length; i++) {
    line=shapes[i];
    if(line.shape_type==LINE_SHAPE && ((line.id_shape_1==currentShape.id && line.id_shape_2==connectedObject.id)||
      (line.id_shape_2==currentShape.id && line.id_shape_1==connectedObject.id)))
      return line;
  }
}
/**
 * [getAllattributestoStructuredAttribute returns all the connected attributes to the structured attribute.
 *  For instance, if we have an attribute "Name" that has a linked attributes "Fname","Lname","Minit",
 *  so the return will be as array that contains all the connected attributes ["Fname","Lname","Minit"]]
 * @param  {Array} shapes Shapes array
 * @param  {int} id     Id of the structured attribute
 * @return {Array}        [An array that has all the connected attributes.]
 */
function getAllattributestoStructuredAttribute(shapes,id){
  var shape1,shape2,line;
  var currentShapeName=shapes[SearchById(shapes,id)].shape_name;
  var structuredArray=new Array();
  var returnedArray=new Array();
  for (var i = 0; i < shapes.length; i++) {
      line=shapes[i];
      if(line.shape_type==LINE_SHAPE){
        shape1=shapes[SearchById(shapes,line.id_shape_1)];
        shape2=shapes[SearchById(shapes,line.id_shape_2)];
        if(id==line.id_shape_1 && shape2.shape_type==ATTRIBUTE_SHAPE){
          if(!attributesIds.includes(shape2.id)){
            attributesIds.push(shape2.id);
            structuredArray.push(shape2.shape_name);
            }
          }
        else if (id==line.id_shape_2 && shape1.shape_type==ATTRIBUTE_SHAPE) {
          if(!attributesIds.includes(shape1.id)) {
            attributesIds.push(shape1.id);
          structuredArray.push(shape1.shape_name);
          }
        }
      }
    }
    returnedArray=updateAllFields(structuredArray,currentShapeName);
    return returnedArray;
}
/**
 * [updateAllFields a helper function to update the attributes that are connected to a structured attribute.
 * For instance, if we have an attribute "Name" -"current shape name" in the parameters-that has a linked attributes "Fname","Lname","Minit",
 *  so the return will be as array that contains all with updated names of the connected attributes as follows["Name_Fname","Name_Lname","Name_Minit"]]
 * @param  {Array} attributeArrayOfAttribute [The list of connected attributes]
 * @param  {Object} currentShapeName          [the structured attribute]
 * @return {Array}                           [an array that contains the updated names]
 */
function updateAllFields(attributeArrayOfAttribute,currentShapeName){
  var returnedArray=new Array();
  if(attributeArrayOfAttribute.length==0){
    returnedArray.push(currentShapeName);
  }
  else {
    for (var i = 0; i < attributeArrayOfAttribute.length; i++) {
      returnedArray.push(currentShapeName+"_"+attributeArrayOfAttribute[i]);
    }
  }
  return returnedArray;
}
/**
 * [getAllConnectedObjects returns all the connected objects to an object]
 * @param  {Array} shapes [shapes Array]
 * @param  {Int} id     [The id of the object we are searching for the connected items to it]
 * @return {Array}        [returns an array that contains all the connected objects]
 */
function getAllConnectedObjects(shapes,id){
  var connectedObjectsArray=new Array();
  var shape1,shape2,line;
  for (var i = 0; i < shapes.length; i++) {
    {
      line=shapes[i];
      if(line.shape_type==LINE_SHAPE)
      {
        shape1=shapes[SearchById(shapes,line.id_shape_1)];
        shape2=shapes[SearchById(shapes,line.id_shape_2)];
        if(id==line.id_shape_1)
        {
          if(!attributesIds.includes(line.id_shape_2))
          {
            attributesIds.push(line.id_shape_2);
            connectedObjectsArray.push(shape2);
          }
        }
        else if (id==line.id_shape_2) {
          if(!attributesIds.includes(line.id_shape_1))
          {
            attributesIds.push(line.id_shape_1);
            connectedObjectsArray.push(shape1);
          }
        }
      }
    }
  }
  return connectedObjectsArray;
}
