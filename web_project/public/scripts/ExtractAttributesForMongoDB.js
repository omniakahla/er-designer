/**
* <h1>ExtractAttributesForMongoDB!</h1>
* Extract Attributes For Mongodb script is used to extract all the attributes that are linked to a collection in a MongoDB schema.
*
* @author  Omnia Kahla
* @version 1.0
* @since   2018-10-05
*
*/

var initialValues=new Array();
var ParentEntities=new Array();
var relationsArray=new Array();//to save the M..N relation and the 1..1 relation so it is added to only one table not both
/**
 * [ExtractAttributesForMongoDB A function used to initialize the global array variables and to call the main function (check all entities)]
 * @param       {Array} shapes [The shapes array]
 */
function ExtractAttributesForMongoDB(shapes)
{
  initialValues.splice(0);
  relationsArray.splice(0);
  ParentEntities.splice(0);
  CheckAllEntities(shapes);
  for(var i=0;i<initialValues.length;i++){
  console.log("Entity_name: "+initialValues[i].entity_name);
  console.log("attribute_name: "+initialValues[i].attribute_name);
  console.log("attribute_Initialvalue: "+initialValues[i].attribute_Initialvalue);
  console.log("entityName_attributeName: "+initialValues[i].entityName_attributeName);
  }

  return initialValues;

}
/**
 * [CheckAllEntities A function used to search for all entities and return their attributes.]
 * @param       {Array} shapes [Shapes array]
 */
function CheckAllEntities(shapes)
{
  for (var i = 0; i < shapes.length; i++) {
    var generatedEntityName="";
    if(shapes[i].shape_name.toLowerCase().endsWith("_p")&& SearchParentEntities(shapes[i],ParentEntities)==null){
      AddParentEntityToParentEntitiesArray(shapes,shapes[i]);
    }
    else if(shapes[i].shape_type==ENTITY_SHAPE && shapes[i].shape_line !=DOUBLE_LINE  && !shapes[i].shape_name.toLowerCase().endsWith("_p")){
      var connectedObjects=GetConnectedObjects(shapes,shapes[i].id,ALL_SHAPES);
        for(var j=0;j<connectedObjects.length;j++){
          generatedEntityName=shapes[i].shape_name;//+"_"+connectedObjects[j].shape_name;//Entity_parentEntity
          if(connectedObjects[j].shape_type==ENTITY_SHAPE){//it must be a parentEntity so we will add it as an embedded document.
            AddParentEntityToChildEntity(shapes,shapes[i],connectedObjects[j],generatedEntityName);
          }
          else if(connectedObjects[j].shape_type==ATTRIBUTE_SHAPE){
            Array.prototype.push.apply(initialValues, AddAttributeToEntity(shapes, shapes[i],connectedObjects[j],generatedEntityName));
          }
          else if (connectedObjects[j].shape_type==RELATION_SHAPE) {
            Array.prototype.push.apply(initialValues, AddRelationToEntity(shapes, shapes[i],connectedObjects[j],null,generatedEntityName));
          }
            generatedEntityName="";
        }
      }
    }
  }
  /**
   * [AddParentEntityToChildEntity adds the parent entity and all its attribute as an embedded document to the child entity.]
   * @param {Array}shapes [The shapes array]
   * @param       {Object} entity       [Child entity]
   * @param       {Object} parentEntity [parent Entity]
   * @param {String} generatedEntityName [The generated entity name that is used for searching]
   */
  function AddParentEntityToChildEntity(shapes,entity,parentEntity,generatedEntityName){
    var V_parentEntity=SearchParentEntities(parentEntity,ParentEntities);
    if(V_parentEntity==null){
      V_parentEntity=AddParentEntityToParentEntitiesArray(shapes,parentEntity);
    }
    //var linkedAttributesToParentEntity=Array.from(V_parentEntity.attributes);
    var linkedAttributesToParentEntity=$.extend(true, [], V_parentEntity.attributes);//Deep copy
    for(var i=0;i<linkedAttributesToParentEntity.length;i++){

      //var generatedAttributeName=generatedEntityName.replace(entity.shape_name,"");
      //generatedAttributeName=((generatedAttributeName.startsWith("_"))?generatedAttributeName.replace("_",""):generatedAttributeName);
      generatedEntityName=((generatedEntityName=="")?entity.shape_name:generatedEntityName);

        linkedAttributesToParentEntity[i].entity_name=generatedEntityName;
        linkedAttributesToParentEntity[i].attribute_name=linkedAttributesToParentEntity[i].entityName_attributeName;//((generatedAttributeName!="")?generatedAttributeName+"_":"")+parentEntity.shape_name+"_"+linkedAttributesToParentEntity[i].attribute_name;
        linkedAttributesToParentEntity[i].entityName_attributeName=generatedEntityName+"_"+linkedAttributesToParentEntity[i].entityName_attributeName;//linkedAttributesToParentEntity[i].attribute_name;//linkedAttributesToParentEntity[i].attribute_name;
        initialValues.push( Object.assign({}, linkedAttributesToParentEntity[i]));
        //initialValues.push(linkedAttributesToParentEntity[i]);
      //Array.prototype.push.apply(initialValues,linkedAttributesToParentEntity[i]);
    }
  }

  /**
   * [AddAttributeToEntity Adds the attribute to the entity]
   * @param       {Array} shapes    [the shapes array]
   * @param       {Object} entity    [The entity object]
   * @param       {Object} attribute [The attribute object to be added to an entity]
   *  @param {String} generatedEntityName [The generated entity name that is used for searching]
   */
function AddAttributeToEntity(shapes, entity,attribute,generatedEntityName){
  var connectedAttributes=GetConnectedObjects(shapes,attribute.id,ATTRIBUTE_SHAPE);
  var attributes=new Array();
  var generatedAttributeName=generatedEntityName.replace(entity.shape_name,"");
  generatedAttributeName=((generatedAttributeName.startsWith("_"))?generatedAttributeName.replace("_",""):generatedAttributeName);
  generatedEntityName=((generatedEntityName=="")?entity.shape_name:generatedEntityName);
  if(connectedAttributes.length>0){
    for(var i=0;i<connectedAttributes.length;i++){
       var attribute_type = {
          entity_name:entity.shape_name,//used in the UI
          attribute_name: ((generatedAttributeName!="")?generatedAttributeName+"_":"")+attribute.shape_name+"_"+connectedAttributes[i].shape_name,//used in the UI
          attribute_Initialvalue:"",//returned from UI
          entityName_attributeName:generatedEntityName+"_"+attribute.shape_name+"_"+connectedAttributes[i].shape_name//used for searching
        };
        //if(searchInAttributeInitialValues(attribute_type.entityName_attributeName,initialValues)=="")
          attributes.push(attribute_type);
    }
  }
  else{
    var attribute_type = {
       entity_name:entity.shape_name,//used in the UI
       attribute_name: ((generatedAttributeName!="")?generatedAttributeName+"_":"")+attribute.shape_name,//used in the UI
       attribute_Initialvalue:"",//returned from UI
       entityName_attributeName:generatedEntityName+"_"+attribute.shape_name//used for searching
     };
     //if(searchInAttributeInitialValues(attribute_type.entityName_attributeName,initialValues)=="")
        attributes.push(attribute_type);
  }
  return attributes;
}

function searchInAttributeInitialValues(Entity_ConnectedAttribute,initialValues){
  if(initialValues.length>1){
    for (var i = 0; i < initialValues.length; i++) {
      if(initialValues[i].entityName_attributeName==Entity_ConnectedAttribute){
        return initialValues[i];
      }
    }
  }
  return "";
}

/**
 * [AddRelationToEntity Adds the relation to the entity]
 * @param       {Array} shapes    [the shapes array]
 * @param       {Object} entity    [The main entity object]
 * @param       {Object} relation [The relation object to be added to an entity]
 * @param {Object}parentEntity [In case the relation is connected to parentEntity and we want to add its attribute to the main Entity ]
 *  @param {String} generatedEntityName [The generated entity name that is used for searching]
 */
function AddRelationToEntity(shapes, entity,relation,parentEntity,generatedEntityName){
  if(entity.shape_name.toLowerCase().endsWith("_p")){
    return [];
  }
  //if parentEntity parameter is null, so the relation is connected to entity, otherwise the relation is connected to parentEntity
  var temp_generatedEntityName=generatedEntityName+"_"+relation.shape_name;
  var line=getLinebetweenRelationAndEntity(shapes,relation.id,((parentEntity==null)?entity.id:parentEntity.id));
  var attributelist= GetConnectedObjects(shapes,relation.id,ATTRIBUTE_SHAPE);
  var connectedEntities=GetConnectedObjects(shapes,relation.id,ENTITY_SHAPE);
  if(line.shape_name.toLowerCase().startsWith(RECURSIVE_RELATION)){
    AddRecursiveRelationToEntity(shapes,entity,relation,temp_generatedEntityName);
  }
  else{
    for (var i = 0; i < connectedEntities.length; i++) {
      if(connectedEntities[i].id!=entity.id){
        var line2 = getLinebetweenRelationAndEntity(shapes,relation.id,connectedEntities[i].id);

        if  (line.shape_name ==line2.shape_name && line.shape_name=="1..1")
        {
          if( !SearchInRelationArray(((parentEntity==null)?entity.id:parentEntity.id),entity.id,relation.id)){
            var relationDetails={entity1_id:((parentEntity==null)?entity.id:parentEntity.id),
                                    entity2_id:connectedEntities[i].id,
                                    relation_id:relation.id
            };
            relationsArray.push(relationDetails);
          }
          if(attributelist.length!=0){
            for(var j=0;j<attributelist.length;j++){
              Array.prototype.push.apply(initialValues, AddAttributeToEntity(shapes, entity,attributelist[j],temp_generatedEntityName));
            }
          }
          temp_generatedEntityName=temp_generatedEntityName+"_"+connectedEntities[i].shape_name;
            var attributesConnectedToConnectedEntity=GetConnectedObjects(shapes,connectedEntities[i].id,ATTRIBUTE_SHAPE);
            if(attributesConnectedToConnectedEntity.length!=0){
              for(var j=0;j<attributesConnectedToConnectedEntity.length;j++){
                Array.prototype.push.apply(initialValues, AddAttributeToEntity(shapes, entity,attributesConnectedToConnectedEntity[j],temp_generatedEntityName));
              }
            }
        }
        else {//either 1..* or M..N so we will need to add the reference only and the relations attributes in the 1 table or in the first table
          //if(line.shape_name=="1..1" || (line.shape_name=="1..*" && line2.shape_name=="1..*")){
            if( !SearchInRelationArray(((parentEntity==null)?entity.id:parentEntity.id),entity.id,relation.id)){
              var relationDetails={entity1_id:((parentEntity==null)?entity.id:parentEntity.id),
                                      entity2_id:connectedEntities[i].id,
                                      relation_id:relation.id
              };
              relationsArray.push(relationDetails);
            }
            if(attributelist.length!=0){
              for(var j=0;j<attributelist.length;j++){
                Array.prototype.push.apply(initialValues, AddAttributeToEntity(shapes, entity,attributelist[j],temp_generatedEntityName));
              }
            }
          //}
        }
      }
    }
  }
}
/**
 * [SearchInRelationArray A function used to search in the global variable relationsArray, so if the relation is already added to one collection, it is not added to the other. This should be done for the 1..1 and M..N relations]
 * @param       {Int} Entity1_Id [The generated id in the shape object]
 * @param       {Int} Entity2_Id [The generated id in the shape object]
 * @param       {Int} relation_Id [The generated id in the shape object]
 */
function SearchInRelationArray(Entity1_Id,Entity2_Id,relation_Id)
{
  for(var i=0;i<relationsArray.length;i++){
    if(relationsArray[i].relation_id == relation_Id){
      if((relationsArray[i].entity1_id==Entity1_Id && relationsArray[i].entity2_id == Entity2_Id)||(relationsArray[i].entity1_id==Entity2_Id && relationsArray[i].entity2_id == Entity1_Id)){
        return true;
        }
      }
    }
    return false;
}

/**
 * [AddRecursiveRelationToEntity adds a recursive relation with all its attributes to an entity]
 * @param       {Array} shapes        [the shapes array]
 * @param       {Object} entity        [entity object]
 * @param       {Object} relation        [relation object]
 * @param {String} generatedEntityName [The generated entity name that is used for searching]
 */
function AddRecursiveRelationToEntity(shapes,entity,relation,generatedEntityName){
  var temp_generatedEntityName=generatedEntityName+"_"+relation.shape_name;
  var attributelist=GetConnectedObjects(shapes,relation.id,ATTRIBUTE_SHAPE);
  if(attributelist.length!=0){
    for (var i = 0; i < attributelist.length; i++) {
      Array.prototype.push.apply(initialValues, AddAttributeToEntity(shapes, entity,attributelist[i],temp_generatedEntityName));
    }
  }
}
function SearchParentEntities(parentEntity,ParentEntitiesArray){
  for (var i = 0; i < ParentEntitiesArray.length; i++) {
    if(ParentEntitiesArray[i].parentEntityId ==parentEntity.id){
      return ParentEntitiesArray[i];
    }
  }
  return null;
}
function AddParentEntityToParentEntitiesArray(shapes,p_parentEntity){
  var parentEntity_attributes={
    parentEntityId:p_parentEntity.id,
    attributes:[]
  };
  var V_parentEntity=SearchParentEntities(p_parentEntity.id,ParentEntities)
  var connectedObjects=GetConnectedObjects(shapes,p_parentEntity.id,ALL_SHAPES);
  if(V_parentEntity==null){
    ParentEntities.push(parentEntity_attributes);
    for (var i = 0; i < connectedObjects.length; i++) {
      if(connectedObjects[i].shape_type==ATTRIBUTE_SHAPE){
        Array.prototype.push.apply(ParentEntities[ParentEntities.length-1].attributes, AddAttributeToEntity(shapes, p_parentEntity,connectedObjects[i],""));
      }
      else if (connectedObjects[i].shape_type==RELATION_SHAPE) {
        Array.prototype.push.apply(ParentEntities[ParentEntities.length-1].attributes, AddRelationToEntity(shapes, p_parentEntity,connectedObjects[i],null,""));
      }
    }
    V_parentEntity=ParentEntities[ParentEntities.length-1];
  }
  return V_parentEntity;
}
