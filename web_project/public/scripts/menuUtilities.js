/**
 * [moveShapes A function used for the GUI to change the cursor style]
 */
function moveShapes()
{
  $("#canvasDiv").css('cursor', 'move');
  clickStatus="move";
   selected_index=-1;
  return;
}
/**
 * [deleteShapes  A function used for the GUI to change the cursor style]
 */
function deleteShapes()
{
  $("#canvasDiv").css('cursor', 'crosshair');
  clickStatus="delete";
   selected_index=-1;
  return;
}

/**
 * [clearAllShapes  A function used to clear the form from all shapes.]
 */
function clearAllShapes()
{
  var r = confirm("Do you really want to delete all shapes?");
  if (r == true) {
    clearDrawings();
  }
}
/**
 * [drawShapesonCanvas  A function for the GUI to change the cursor style]
 */
function drawShapesonCanvas ()
{
  $("#canvasDiv").css('cursor', 'hand');
  clickStatus="draw";
  selected_index=-1;
  return;
}
/**
 * [ParentEntity A flag that indicates if the entity is a parent entity]
 */
function ParentEntity(){
  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    var Entity=fine_shapes[selected_index];
    if(Entity.shape_name=="")
    {
      Entity.shape_name="_p";
    }
    else {
      Entity.shape_name+="_p";
    }
    selected_index=-1;
  }

}
/**
 * [deleteSpecificShape A function used to clear a specific shape that is selected by user with all its linked lines]
 */
function deleteSpecificShape()
{
  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    var target_ID=fine_shapes[selected_index].id;
    fine_shapes.splice(selected_index,1);
    DeleteMyLines(fine_shapes,target_ID);
    //delete fine_shapes[selected_index];
    selected_index=-1;
  }
}
/**
 * [solidShape A function used to change the shape style to be a normal shape and redraw the shape]
 */
function solidShape()
{
  //selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    fine_shapes[selected_index].shape_line = NORAML_LINE;
    DrawFineShapes(fine_shapes[selected_index],null);
  }
}
/**
 * [dashedShape A function used to change the shape style to be a dashed shape and redraw the shape]
 */
function dashedShape()
{
  //  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    fine_shapes[selected_index].shape_line = DOTTED_LINE;
    DrawFineShapes(fine_shapes[selected_index],null);
  }
}
/**
 * [doubleShape A function used to change the shape style to be a double line shape and redraw the shape]
 */
function doubleShape()
{
  //  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    fine_shapes[selected_index].shape_line = DOUBLE_LINE;
    DrawFineShapes(fine_shapes[selected_index],null);
  }
}
/**
 * [oneAndOnlyOneRelation A function used to write 1..1 on the line]
 */
function oneAndOnlyOneRelation()
{
  //  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    BindTextToShape(fine_shapes[selected_index], '1..1');
    fine_shapes[selected_index].shape_name = '1..1';
  }
}
/**
 * [zeroOrMoreRelation A function used to write 0..M on the line]
 */
function zeroOrMoreRelation()
{
  //  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    BindTextToShape(fine_shapes[selected_index], '0..*');
    fine_shapes[selected_index].shape_name = '0..*';
  }
}
/**
 * [oneOrMoreRelation A function used to write 1..M on the line]
 */
function oneOrMoreRelation()
{
  //  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    BindTextToShape(fine_shapes[selected_index], '1..*');
    fine_shapes[selected_index].shape_name = '1..*';
  }
}
/**
 * [zeroOrOneRelation A function used to write 0..1 on the line]
 */
function zeroOrOneRelation()
{
  if(selected_index!=-1)
  {
    BindTextToShape(fine_shapes[selected_index], '0..1');
    fine_shapes[selected_index].shape_name = '0..1';
  }
}
/**
 * [RecursiveRelation A function used to draw the recursive relation]
 */
function RecursiveRelation()
{
  if(selected_index!=-1)
  {
    //BindTextToShape(fine_shapes[selected_index], 'recursive');
    var line=fine_shapes[selected_index];
    var shape1=findShapeById(fine_shapes,line.id_shape_1);
    var shape2=findShapeById(fine_shapes,line.id_shape_2);
    var Rect=-1, Dia=-1;
    if(shape1.shape_type=='Rectangle')
    {
      Rect=shape1.id;
    }
    else if(shape2.shape_type=='Rectangle') {
      Rect=shape2.id;
    }
    if(shape1.shape_type=='Diamond')
    {
      Dia=shape1.id;
    }
    else if(shape2.shape_type=='Diamond') {
      Dia=shape2.id;
    }
    if(Rect==-1|| Dia==-1)
    {
      alert("You can only draw a recursive line between a rectangle and diamond.");
      return ;
      //fine_shapes.splice(fine_shapes.length-1,1);
    }
    else
    {
      if(!line.shape_name.startsWith(RECURSIVE_RELATION)){
        line.shape_name= RECURSIVE_RELATION;
      }

      confirm("Please add the roles and its cardinality each in a seperate line");//, and do not remove the recursive keyword from the first line. For instance:\n recursive\n role x and cardinality (1..1)\n role y and cardinality (1..M)");
      showeditor(selected_index,line.shape_name);//"recursive\n Supervisor (1..1)\n Employee (1..M)");
      line.id_shape_1=Rect;
      line.id_shape_2=Dia;
    }
  }
}
/**
 * [solidlinewithNorelation A function used to draw a solid line]
 */
function solidlinewithNorelation()
{
  if(selected_index!=-1)
  {
    BindTextToShape(fine_shapes[selected_index], '');
    fine_shapes[selected_index].shape_name = '';
  }
}
/**
 * [Renamefunction A function used to set a name for a specific object]
 */
function Renamefunction()
{
  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    /*if(fine_shapes[selected_index].shape_name !="")
    {
      showeditor(selected_index,fine_shapes[selected_index].shape_name);
    }
    else {*/
        showeditor(selected_index,fine_shapes[selected_index].shape_name);//,"");
    //  }
  }
}
/**
 * [ChangeShape a function used to change the object for example from rectangle to circle and so on.]
 * @param       {String} newShapeType The new shape type
 */
function ChangeShape(newShapeType)
{
  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  if(selected_index!=-1)
  {
    var x1=fine_shapes[selected_index].shape_x1, y1=fine_shapes[selected_index].shape_y1, w= ((fine_shapes[selected_index].shape_width==0)?fine_shapes[selected_index].shape_diameter*2 : fine_shapes[selected_index].shape_width), h=((fine_shapes[selected_index].shape_height==0)?fine_shapes[selected_index].shape_diameter*2 : fine_shapes[selected_index].shape_height);
    if(fine_shapes[selected_index].shape_type=="Rectangle" && newShapeType !="Rectangle")
    {
      fine_shapes[selected_index].shape_type=newShapeType;
      fine_shapes[selected_index].shape_x1=x1+(w/2);
      fine_shapes[selected_index].shape_y1=y1+(h/2);
      fine_shapes[selected_index].shape_x2=x1+(w/2);
      fine_shapes[selected_index].shape_y2=y1+(h/2);
      fine_shapes[selected_index].shape_diameter=((w/2 >h/2)? h/2 : w/2);
    }
    else if (fine_shapes[selected_index].shape_type!="Rectangle" && newShapeType =="Rectangle") {
      fine_shapes[selected_index].shape_type=newShapeType;
      fine_shapes[selected_index].shape_x1=x1-(w/2);
      fine_shapes[selected_index].shape_y1=y1-(h/2);
      fine_shapes[selected_index].shape_width=w;
      fine_shapes[selected_index].shape_height=h;
    }
      fine_shapes[selected_index].shape_type=newShapeType;
  }
  DrawFineShapes(fine_shapes[selected_index],null);
  selected_index=-1;
}

/**
 * [UnderlineAttribute under line the attribute to mark it as a primary/partial key Field]
 * @param       {Boolean} dotted To indicate wether it is a primary key or partial key. In case primary key, it is false Otherwise it is true.
 */
function UnderlineAttribute(dotted){
  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  shape=fine_shapes[selected_index];
  if(selected_index!=-1&& shape.shape_type==ATTRIBUTE_SHAPE){
    if(shape.shape_name == "")
    {
      alert("Please add the attribute name first.");
      return;
    }
      if(dotted){
        shape.key=0;
      }
      else{
        shape.key=1;
      }
      DrawFineShapes(shape,null);
  }
}
/**
 * [NormalAttribute If the attribute is a key, this function returns the attribute as a normal attribute]
 */
function NormalAttribute(){
  selected_index=SelectShape(fine_shapes,canvasX,canvasY);
  shape=fine_shapes[selected_index];
  if(shape.key==-1){
    confirm("It is already a normal attribute!");
    return;
  }
  shape.key=-1;
  DrawFineShapes(shape,null);
}
