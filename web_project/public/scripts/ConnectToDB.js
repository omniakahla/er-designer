var sql_engine="";
/**
 * [connect_to_db_btn_click A function used to open the window that is used to enter the details of the connection to connect to database.]
 */
function connect_to_db_btn_click(){
	var temp=$("#schema_textbox").text();

	 if(temp.includes("CREATE TABLE")){
 		sql_engine=get_selected_sql_engine();
 	}
	else{
		 sql_engine=get_selected_nosql_engine();
	 }
	 //alert(sql_engine);
	 	$("#tr_dbname").show();
	 	$("#tr_username").show();
	 	$("#tr_password").show();
	 if(sql_engine=="mysql"){


	 }
	else if(sql_engine=="oracle"){
		//$("#tr_username").show();
		//$("#tr_password").show();
		$("#tr_dbname").hide();

	}
	else if(sql_engine=="mongodb"){
	//	$("#tr_dbname").show();
		$("#tr_username").hide();
		$("#tr_password").hide();

	}
	else if(sql_engine=="neo4j")
	{
		$('#connection_modal').modal('hide');
		alert("You can use the copy schema only");
			//$("#connection_modal").
	}
		$("#connection_modal").modal();
}
/**
 * [connect_to_db_create A helper functino used when the user hits the create button in the interface to create the database according to the connection details he entered.]
 */
function connect_to_db_create(){
	create=true;//$("#chb_create").is(":checked");
	db="";
	 schema_parameter="";
	var temp=$("#schema_textbox").text();

	 if(temp.includes("CREATE TABLE")){
 		sql_engine=get_selected_sql_engine();
 	}
	else{
		 sql_engine=get_selected_nosql_engine();
	 }
	 if(sql_engine=="mysql")
		{
				db=$("#txt_dbname").val();
				schema_parameter="DROP DATABASE IF EXISTS "+db+"; CREATE DATABASE IF NOT EXISTS "+db+";";
		}
		else if(sql_engine=="mongodb")
		{
			db= $("#txt_dbname").val();
			if(db==""){
				alert("Please specify the database name");
				return;
			}
			schema_parameter="db=db.getSiblingDB(\""+db+"\");";
		}
		var start_index=0;

		var last_index=temp.lastIndexOf("dbname.");
		while (start_index!=-1)
		 {
			temp=temp.replace(/dbname\./,((db=="")?"":db+"."));
			start_index=temp.indexOf("dbname.");
		}
		schema_parameter+=temp;
	username=$("#txt_username").val();
	password=$("#txt_password").val();
	connectionString=$("#txt_url").val();

	connect_to_db(sql_engine,username, password, db, connectionString,schema_parameter,create);
	sql_engine="";
}
/**
 * [connect_to_db It takes all the parameters the user entered to connect to the corresponding databse and create the database. ]
 * @param  {String} sql_engine       [In our case it is either oracle, mysql or mongodb]
 * @param  {String} username         [The user name to connect to the database.]
 * @param  {String} password         [The password to connect to the database.]
 * @param  {String} db               [The database name to connect to.]
 * @param  {String} connectionString [The server to connect to]
 * @param  {String} schema_parameter [The script of the database.]
 * @param  {Boolean} create           [A variable that specify either to create the database or just to connect to it. In this version, it is always true. we are always deleting the database and then creating it according to the script extracted. ]
 */
function connect_to_db(sql_engine,username, password, db, connectionString,schema_parameter,create){
	if(sql_engine=="oracle")
	{
		$.post( '/connectToOracle', {schema: schema_parameter,host:connectionString, user: username, password:password, database:db ,CreateDB:true}).done(function (result)
		{
			if(result=="Created")
			{
				alert("DB created successfully");
			}
			else
			{
				alert(result);
			}});
		}
    else if (sql_engine=="mysql")
    {
    	$.post( '/connectToMySQL', {schema: schema_parameter,host:connectionString, user: username, password:password, database:db ,CreateDB:true}).done(function (result)
    	{
    		if(result=="Created")
    		{
    			alert("DB created successfully");
    		}
    		else
    		{
					alert(result);
    			//alert("There was a probelm during connecting, please try again.");
    		}});
    }
    else if (sql_engine=="mongodb")
    {
    	$.post( '/executeBashCMD',{script:schema_parameter,host:connectionString, user: username, password:password, database:db ,CreateDB:true}).done(function (result)
    	{
				if(result=="Created")
    		{
    			alert("DB created successfully");
    		}
    		else
    		{
					alert(result);
    			//alert("There was a probelm during connecting, please try again.");
    		}
			});
    }
		else if (sql_engine=="neo4j")
    {
    	$.post( '/connectToNeo4j',{script:schema_parameter,host:connectionString, user: username, password:password, database:db ,CreateDB:true}).done(function (result)
    	{});
    }
}
