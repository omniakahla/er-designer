/**
 * [save A function used to save the project diagram]
 * @param  {Boolean} exit [A boolean variable to indicate whether to close the project or not]
 */
function save(exit)
{
  var project_id = url_query('id');
  if(project_id)
  {
    $.post( '/updateProject', {ID:project_id, Diagram: fine_shapes}).done(function (result)
    {
      if(result=="updated")
      {
        alert("The diagram has been saved successfully");
        location.assign(window.location.href);
        if(exit)
        {
          var loc=window.location.href;
          loc=loc.substring(0,loc.indexOf("draw"));
          loc=loc+"projects";
          location.assign(loc);
        }
//	location.replace("http://localhost:8080/projects");
      }
      else
      {
        alert("There was a probelm during saving, please try again.");
      }});
  }
  else
  {
    var projectName = prompt("Please enter project name: ");

    if (projectName != null && projectName !="")
    {
      console.log("user id from the draw.js is :"+user_id);
      $.post( '/saveDiagram', {ProjectName:projectName, User:user_id, Diagram: fine_shapes, Connections:[""], schemaIds:[null], model:"ERD" }).done(function (result) {
      //if(result=="saved")
      //{
          alert("The Diagram has been saved successfully");
          location.assign(window.location.href+"?id="+result);
					if(exit)
          {
            var loc=window.location.href;
            loc=loc.substring(0,loc.indexOf("draw"));
            loc=loc+"projects";
            location.assign(loc);
          }

					//location.replace(document.referrer);
					//location.replace("http://localhost:8080/projects");
        /*}
        else
        {
            alert("There was a probelm during saving, please try again.");
        }*/
      });
    }
  }
}
