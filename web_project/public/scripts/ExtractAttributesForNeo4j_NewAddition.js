/**
* <h1>ExtractAttributesForNeo4j!</h1>
* Extract Attributes For Neo4j script is used to extract all the attributes that are linked to a collection in a MongoDB schema.
*
* @author  Omnia Kahla
* @version 1.0
* @since   2018-10-05
*
*/
var initialValues=new Array();
var checkedShapes=new Array();
var NodesInitialValues=new Array();
var RelationsInitialValues=new Array();
var RelationsArray=new Array();
/**
 * [ExtractAttributesInitialValuesForNeo4j A function used to initialize the global array variables and returns the nodes and relations initialvalues]
 * @param       {Array} shapes [The shapes array]
 */
function ExtractAttributesInitialValuesForNeo4j(shapes)
{
  NodesInitialValues.splice(0);
  RelationsInitialValues.splice(0);
  initialValues.splice(0);
  checkedShapes.splice(0);
  for (var i = 0; i < shapes.length; i++) {
    if(shapes[i].shape_type==ENTITY_SHAPE && !shapes[i].shape_name.toLowerCase().endsWith("_p")){
      checkedShapes.push(shapes[i].id);
      var connectedAttributes=GetConnectedObjects(shapes,shapes[i].id,ATTRIBUTE_SHAPE);
      ExtractNodeAttributes(shapes,shapes[i].shape_name,connectedAttributes,ENTITY_SHAPE);
      }
      else if(shapes[i].shape_type==ENTITY_SHAPE && shapes[i].shape_name.toLowerCase().endsWith("_p")){
        checkedShapes.push(shapes[i].id);
        var connectedAttributes=GetConnectedObjects(shapes,shapes[i].id,ATTRIBUTE_SHAPE);
        ExtractNodeAttributes(shapes,shapes[i].shape_name,connectedAttributes,ENTITY_SHAPE);
      }
      else if(shapes[i].shape_type==RELATION_SHAPE){
        checkedShapes.push(shapes[i].id);
        var connectedAttributes=GetConnectedObjects(shapes,shapes[i].id,ATTRIBUTE_SHAPE);
        ExtractRelationshipAttributes(shapes,shapes[i].shape_name,connectedAttributes);
        }
  }

 return initialValues;
}
/**
 * [ExtractNodeAttributes description]
 * @param       {[type]} shapes              [description]
 * @param       {[type]} Entity_name         [description]
 * @param       {[type]} connectedAttributes [description]
 * @param {String}EntityOrRelation [indicates wether the function is called by a node or relation]
 */
function ExtractNodeAttributes(shapes,Entity_name,connectedAttributes,EntityOrRelation){
  var NodeAttributes=new Array();
  for (var i = 0; i < connectedAttributes.length; i++) {
    var connectedAttributesToAttribute=GetConnectedObjects(shapes,connectedAttributes[i].id,ATTRIBUTE_SHAPE);
    if(connectedAttributesToAttribute.length ==0){
      var attribute_type = {
        attributeOFEntityOrRelation:EntityOrRelation,
         entity_name:Entity_name,//used in the UI
         attribute_name: connectedAttributes[i].shape_name,//used in the UI
         attribute_Initialvalue:"",//returned from UI
         entityName_attributeName:Entity_name+"_"+connectedAttributes[i].shape_name//used for searching
       };
       initialValues.push(Object.assign({},attribute_type));
    }
    else {
      for (var j = 0; j < connectedAttributesToAttribute.length; j++) {
        var attribute_type = {
          attributeOFEntityOrRelation:EntityOrRelation,
           entity_name:Entity_name,//used in the UI
           attribute_name: connectedAttributes[i].shape_name+"_"+connectedAttributesToAttribute[j].shape_name,//used in the UI
           attribute_Initialvalue:"",//returned from UI
           entityName_attributeName:Entity_name+"_"+connectedAttributes[i].shape_name+"_"+connectedAttributesToAttribute[j].shape_name//used for searching
         };
         initialValues.push(Object.assign({},attribute_type));
      }
    }
  }
  //return NodeAttributes;
}
/**
 * [ExtractRelationshipAttributes description]
 * @param       {[type]} shapes              [description]
 * @param       {[type]} relation_name       [description]
 * @param       {[type]} connectedAttributes [description]
 */
function ExtractRelationshipAttributes(shapes,relation_name,connectedAttributes){
//  var relationAttributes=
  ExtractNodeAttributes(shapes,relation_name,connectedAttributes,RELATION_SHAPE);
  //return relationAttributes;
}
/**
 * [ExtractRelationsDetails description]
 * @param       {[type]} shapes [description]
 */
function ExtractRelationsDetails(shapes){
  RelationsArray.splice(0);
  for (var i = 0; i < shapes.length; i++) {
    if(shapes[i].shape_type==RELATION_SHAPE){
      var connectedEntities=GetConnectedObjects(shapes,shapes[i].id,ENTITY_SHAPE);
      if(connectedEntities.length>1){//not a recursive relation
        var relationdetails={
          //entity1:connectedEntities[0],
        //  entity2:null,
          relationId:shapes[i].id,
          relation:shapes[i],
          connectedEntitiesToParentEntity:[],
          connectedEntitiesToRelation:connectedEntities,
          mainRole:"",
          directionFrom:""
        };
        for (var j = 0; j < connectedEntities.length; j++) {
          if(connectedEntities[j].shape_name.toLowerCase().endsWith("_p")){//so it is a parentEntity we have to check which one of the child entities that will be connected to the other entity
            var v_connectedEntitiesToParentEntity=GetConnectedObjects(shapes,connectedEntities[j].id,ENTITY_SHAPE);
            relationdetails.connectedEntitiesToParentEntity=v_connectedEntitiesToParentEntity;
            relationdetails.connectedEntitiesToParentEntity.push(connectedEntities[j]);
          }
          //relationdetails.entity2=connectedEntities[j];
          //RelationsArray.push(Object.assign({},relationdetails));
        }
        RelationsArray.push(Object.assign({},relationdetails));
      }
    }
  }
  return RelationsArray;
}
