var activeMenu=false;
//var registered=false;
var paint;
var canvas_shapes;
var canvas_lines;
var context;
var context_lines;
var canvasWidth = 1600;
var canvasHeight = 5800;
var padding = 25;
var lineWidth = 8;
var flag=false;
var globalColor="#df4b26";
var hoverColor="#00ff00";
var fine_shapes= new Array();
var drawingCanvas_leftStart;
var drawingCanvas_topStart;
var moves = new Array();
var shapes_simplify=new Array();
var shapes_rectangle_inside=new Array();
var shapes_rectangle_outside=new Array();
var shapes_triangle=new Array();
var shapes_convex_hull=new Array();
var clickStatus="draw";
var selected_index=-1;
var highest_objected_id=0;
var convexHull_P,  convexHull_A,
		EnclosedRec_Height, EnclosedRec_Width, EnclosedRec_P,  EnclosedRec_A,
		LargestQuad_A,
		LargestTrian_P, LargestTrian_A,
		centerX_circle,centerY_circle , diameter_circle,  start_point,end_point;
var sql_engine="";



/**
 * [prepareCanvas Creates a canvas element, loads images, adds events, and draws the canvas for the first time.]
 */
function prepareCanvas()
{
	// Create the canvas (Neccessary for IE because it doesn't know what a canvas element is)
	var canvasDiv = document.getElementById('canvasDiv');
	canvas_lines = document.createElement('canvas');
	canvas_lines.setAttribute('width', canvasWidth);//window.innerWidth);
	canvas_lines.setAttribute('height',canvasHeight);// window.innerHeight);
	canvas_lines.setAttribute('id', 'canvas_lines');
  canvas_lines.setAttribute('style', 'background-color: white');
	canvas_lines.setAttribute('z-index', '2');
//	alert(canvas_lines.getAttribute('position'));
	//canvasDiv.appendChild(canvas_lines);
	//canvas_lines = document.getElementById('canvas_lines');
	//canvasDiv.removeChild(canvas_lines);

	canvas_lines.style.position='absolute';
	canvas_lines.style.left=0;
	canvas_lines.style.top=135;
	canvasDiv.appendChild(canvas_lines);

	//alert(canvas_lines.style.left);

	canvas_shapes = document.createElement('canvas');
	canvas_shapes.setAttribute('width', canvasWidth);//window.innerWidth);
	canvas_shapes.setAttribute('height',canvasHeight);// window.innerHeight);
	canvas_shapes.setAttribute('id', 'canvas_shapes');
//  canvas_shapes.setAttribute('style', 'background-color: white');
	canvas_shapes.setAttribute('z-index', '1');
	//canvasDiv.appendChild(canvas_shapes);
	//canvas_shapes = document.getElementById('canvas_shapes');
	//canvasDiv.removeChild(canvas_shapes);
	canvas_shapes.style.position='absolute';
	canvas_shapes.style.left=0;
	canvas_shapes.style.top=135;
	canvasDiv.appendChild(canvas_shapes);

	drawingCanvas_leftStart = canvas_shapes.offsetLeft;
	drawingCanvas_topStart = canvas_shapes.offsetTop;
	if(typeof G_vmlCanvasManager != 'undefined')
	{
		canvas_shapes = G_vmlCanvasManager.initElement(canvas_shapes);
	  canvas_lines = G_vmlCanvasManager.initElement(canvas_lines);
	}
	context = canvas_shapes.getContext("2d"); // Grab the 2d canvas context
	context_lines=canvas_lines.getContext("2d");
	// Note: The above code is a workaround for IE 8 and lower. Otherwise we could have used:
	//     context = document.getElementById('canvas').getContext("2d");
  $('#canvas_shapes').mousedown(function(e)
	{
		if(e.which==3)
		{
				//right click
		}
		else  if (e.which==1)
		{
			if (activeMenu)
			 {
				$(".custom-menu").hide(100);
				activeMenu=false;
				return"";
			}
			var mouseX = e.pageX - this.offsetLeft;
			var mouseY = e.pageY - this.offsetTop;
			if(clickStatus=="rename")
			{
				selected_index=SelectShape(fine_shapes,mouseX,mouseY);
				if(selected_index!=-1)
				{
						showeditor(selected_index,fine_shapes[selected_index].shape_name);//,"");
				}
				else
				{
					alert("please select a valid shape to rename");
				}
			}
			else if(clickStatus=="draw")
			{
				paint = true;
				addClick(mouseX, mouseY);
			}
			else if( clickStatus=="move" || clickStatus=="delete")
			{
					selected_index=SelectShape(fine_shapes,mouseX,mouseY);
			}
		}
		drawShapes();
	});
$('#canvas_shapes').mousemove(function(e)
	{
		var mouseX = e.pageX - this.offsetLeft;
		var mouseY = e.pageY - this.offsetTop;
		if(clickStatus=="draw")
		{
		   if(paint)
			 {
		     addClick(mouseX, mouseY);
		   }
		}
		else if( clickStatus=="move")
		{

			if(selected_index!=-1)
			{
				if(fine_shapes[selected_index].shape_type==LINE_SHAPE)
				{
					// donot move lines temporary
					/*fine_shapes[selected_index].shape_x1+= mouseX-fine_shapes[selected_index].shape_x1;
					fine_shapes[selected_index].shape_x2+= mouseX-fine_shapes[selected_index].shape_x1;
					fine_shapes[selected_index].shape_y1+= mouseY-fine_shapes[selected_index].shape_y1;
					fine_shapes[selected_index].shape_y2+= mouseY-fine_shapes[selected_index].shape_y1;
					if(fine_shapes[selected_index].shape_name !="")
						BindTextToShape(fine_shapes[selected_index],fine_shapes[selected_index].shape_name);*/
				}
				else
				{
					fine_shapes[selected_index].shape_x1=mouseX;
					fine_shapes[selected_index].shape_y1=mouseY;
					if(fine_shapes[selected_index].shape_type==ATTRIBUTE_SHAPE ||fine_shapes[selected_index].shape_type==RELATION_SHAPE)
					{
						fine_shapes[selected_index].shape_x2=mouseX;
						fine_shapes[selected_index].shape_y2=mouseY;
					}
					else if(fine_shapes[selected_index].shape_type==ENTITY_SHAPE)
					{
						fine_shapes[selected_index].shape_x2=mouseX+(0.5*fine_shapes[selected_index].shape_width);
						fine_shapes[selected_index].shape_y2=mouseY+(0.5*fine_shapes[selected_index].shape_height);
					}

				}
			}
		}
	  drawShapes();

		var target_index=SelectShape(fine_shapes,mouseX,mouseY);
		if(target_index!=-1)
		{
				DrawFineShapes(fine_shapes[target_index],hoverColor);
					BindTextToShape(fine_shapes[target_index],fine_shapes[target_index].shape_name);

		}


  });
$('#canvas_shapes').mouseup(function(e)
	{
		if(e.which==3)
		{
			//right clik 3 left click 1 middle click 2
		}
		else if (e.which==1)
		{
			if(clickStatus=="draw")
			{
				paint = false;
				var convHul=Array.from(moves);
				convHul.push(convHul[0]);
				convexHull_P = perimeterOfPolygon(convHul);
				convexHull_A = areaOfPolygon(convHul) 	;

				var inTri=computeInSideTriangle(moves);
				var outRec=computeOutSideRectangle(moves);
				var inRec=computeInSideRectangle(moves);

				shapes_rectangle_outside.push(outRec);
				shapes_rectangle_inside.push(inRec);
				shapes_triangle.push(inTri);
				shapes_convex_hull.push(convHul);
				clearMoves();
				var output = DetectShape(highest_objected_id);
				var index_first_shape=-1;
				var index_second_shape=-1;
				//var flag1=false;
				if(output.shape_type=="unkown")
					return "";

				if(output.shape_type==LINE_SHAPE)
				{
					index_first_shape= SelectShape(fine_shapes,output.shape_x1,output.shape_y1);
				  index_second_shape= SelectShape(fine_shapes,output.shape_x2,output.shape_y2);
					xshape1=fine_shapes[index_first_shape];
					xshape2=fine_shapes[index_second_shape];
					if(index_first_shape==-1 || index_second_shape==-1 ||  index_first_shape== index_second_shape)
						return "";
					else if(xshape1.shape_type==ENTITY_SHAPE&& xshape2.shape_type==ENTITY_SHAPE){
						if(xshape1.shape_name.toLowerCase().endsWith("_p")&&xshape2.shape_name.toLowerCase().endsWith("_p")){
							alert("You have to specify only the parent with the _p at the end of the its name");
							return;
						}
						else if(!xshape1.shape_name.toLowerCase().endsWith("_p")&&!xshape2.shape_name.toLowerCase().endsWith("_p")){
							alert("You have to specify the parent with the _p at the end of the its name");
							return;
						}
					}
					else if(xshape1.shape_type==LINE_SHAPE && xshape2.shape_type==LINE_SHAPE){
						return;
					}
					else if(SearchTheLineInFineshapes(output,xshape1.id,xshape2.id)){
						return;
					}
				 output=BindLineToShapes(fine_shapes[index_first_shape].id,fine_shapes[index_second_shape].id,output);
				// flag1=true;
				output.shape_x1=fine_shapes[index_first_shape].shape_x2;
				output.shape_y1=fine_shapes[index_first_shape].shape_y2;
				output.shape_x2=fine_shapes[index_second_shape].shape_x2;
				output.shape_y2=fine_shapes[index_second_shape].shape_y2;
			//	 openLineContextMenu(e.pageX,e.pageY);
				}
				fine_shapes.push(output);
				highest_objected_id++;
				//if(~flag1)
				//	fine_shapes.push(output);
				//console.log(output);
			// clearMoves();
			}
			else if(clickStatus=="move")
			{
				selected_index=-1;
			}
			else if(clickStatus=="delete")
			{
				//to edit
				if(selected_index!=-1)
				{
					var target_ID=fine_shapes[selected_index].id;
					fine_shapes.splice(selected_index,1);
					DeleteMyLines(fine_shapes,target_ID);
					//delete fine_shapes[selected_index];
					selected_index=-1;
				}

			}
		}
	drawShapes();
	});
	$('#canvas_shapes').mouseleave(function(e)
	{
		paint = false;
	});
}
/**
 * [SearchTheLineInFineshapes A function used to search if the line is found and already added, so we can use it to stop drawing lines between the same 2 shapes]
 * @param       {Object} line      [the line object]
 * @param       {Integer} id_1 [The generated id for the 1st shape]
 * @param       {Integer} id_2 [The generated id for the 2nd shape]
 * @return {Boolean}whether if found or not
 */
function SearchTheLineInFineshapes(line,id_1,id_2){
	for (var i = 0; i < fine_shapes.length; i++) {
		if(fine_shapes[i].shape_type==line.shape_type && ((fine_shapes[i].id_shape_1==id_1 && fine_shapes[i].id_shape_2==id_2 )||(fine_shapes[i].id_shape_1==id_2 && fine_shapes[i].id_shape_2==id_1 ))){
			return true;
		}
	}
	return false;
}
/**
 * [rename_tool a function used to change the clickStatus global variable ]
 */
function rename_tool()
{
  clickStatus="rename";
  selected_index=-1;
}
/**
 * [draw_tool a function used to change the cursor style and the clickStatus global variable]
 */
function draw_tool()
{
  $("#canvasDiv").css('cursor', 'hand');
  clickStatus="draw";
  selected_index=-1;
}
/**
 * [delete_tool a function used to change the cursor style and the clickStatus global variable]
 */
function delete_tool()
{
  $("#canvasDiv").css('cursor', 'crosshair');
  clickStatus="delete";
   selected_index=-1;
}
/**
 * [move_tool a function used to change the cursor style and the clickStatus global variable]
 */
function move_tool()
{
  $("#canvasDiv").css('cursor', 'move');
  clickStatus="move";
   selected_index=-1;
}

/**
 * [openShapeContextMenu a function used to open the Rectangle and diamond context menu in the Point(x,y)]
 * @param  {Int} x    [The x position]
 * @param  {Int} y    [The y position]
 */
function openShapeContextMenu(x,y)
{
	var menu=$('#contextMenu_shape');;
	openContextMenu(menu,x,y);
}
/**
 * [openDiamondContextMenu a function used to open the diamond context menu in the Point(x,y)]
 * @param  {Int} x    [The x position]
 * @param  {Int} y    [The y position]
 */
function openDiamondContextMenu(x,y)
{
	var menu=$('#contextMenu_diamond');;
	openContextMenu(menu,x,y);
}
/**
 * [openRectangleContextMenu a function used to open the Rectangle and diamond context menu in the Point(x,y)]
 * @param  {Int} x    [The x position]
 * @param  {Int} y    [The y position]
 */
function openRectangleContextMenu(x,y)
{
	var menu=$('#contextMenu_rectangle');;
	openContextMenu(menu,x,y);
}
/**
 * [openCircleContextMenu a function used to open the circle context menu in the Point(x,y)]
 * @param  {Int} x    [The x position]
 * @param  {Int} y    [The y position]
 */
function openCircleContextMenu(x,y)
{
	var menu=$('#contextMenu_Circle');;
	openContextMenu(menu,x,y);
}
/**
 * [openMainContextMenu a function used to open the main context menu in the Point(x,y)]
 * @param  {Int} x    [The x position]
 * @param  {Int} y    [The y position]
 */
function openMainContextMenu(x,y)
{
	var menu=$('#contextMenu_main');
	openContextMenu(menu,x,y);
}
/**
 * [openLineContextMenu a function used to open line context menu in the Point(x,y)]
 * @param  {Int} x    [The x position]
 * @param  {Int} y    [The y position]
 */
function openLineContextMenu(x,y)
{
	var menu=$('#contextMenu_line');
	openContextMenu(menu,x,y);
}
/**
 * [openContextMenu a helper function used to open the context menu in the Point(x,y)]
 * @param  {Object} menu [the menu element]
 * @param  {Int} x    [The x position]
 * @param  {Int} y    [The y position]
 */
function openContextMenu(menu,x,y)
{
	hideAllContextMenus();
	menu.finish().toggle(100).css({top: y + "px", left: x + "px"});
	activeMenu= true;
}
/**
 * [hideAllContextMenus hides all the context menus after the user hit outside the menu or select an item from the menu.]
 */
function hideAllContextMenus()
{
	$(".custom-menu").hide(100);
	activeMenu=false;
}
/**
 * [DeleteMyLines A helper function that is used with the delete shape, so when we delete a shape we should delete all connected lines.]
 * @param       {Array} fine_shapes [The shapes array]
 * @param       {Int} target_ID   [the id of the shape we should delete its lines]
 */
function DeleteMyLines(fine_shapes,target_ID)
{
	for (var i = fine_shapes.length-1; i >= 0; i--)
	{
			if(fine_shapes[i].shape_type==LINE_SHAPE &&(fine_shapes[i].id_shape_1== target_ID || fine_shapes[i].id_shape_2==target_ID))
			{
				//delete fine_shapes[i];
				fine_shapes.splice(i,1);
			}
	}
}
/**
 * [SelectShape A function used to highlight selected object. Searches for the object that the Point(x,y) belongs to]
 * @param       {Array} fine_shapes The shapes array
 * @param       {int} x           [The x position in the canvas]
 * @param       {Int} y           [The y position in the canvas]
 */
function SelectShape(fine_shapes,x,y)
{
	//var lasterror;
	//var errors=[];
	for (var i = 0; i < fine_shapes.length; i++)
	 {
		if(fine_shapes[i].shape_type==ATTRIBUTE_SHAPE)
		{
		if(Math.pow((x -fine_shapes[i].shape_x1),2) +
		 	Math.pow((y -fine_shapes[i].shape_y1),2) <= Math.pow(fine_shapes[i].shape_diameter,2) )
		  	return i;
		}
		else if(fine_shapes[i].shape_type==ENTITY_SHAPE)
		{
			if(x>=fine_shapes[i].shape_x1 && x< fine_shapes[i].shape_x1+fine_shapes[i].shape_width &&
			   y>=fine_shapes[i].shape_y1 && y<fine_shapes[i].shape_y1+fine_shapes[i].shape_height )
				 return i;
		}
		else if (fine_shapes[i].shape_type==RELATION_SHAPE)
		{
			if(x<=fine_shapes[i].shape_x1+fine_shapes[i].shape_diameter && x> fine_shapes[i].shape_x1-fine_shapes[i].shape_diameter &&
			    y<=fine_shapes[i].shape_y1+fine_shapes[i].shape_diameter && y>fine_shapes[i].shape_y1-fine_shapes[i].shape_diameter )
			 return i;

		}
		else if (fine_shapes[i].shape_type==LINE_SHAPE)
		{
			// equation of line y1-mx1=b  (m is slope) *(b is displacement)
			var m = (fine_shapes[i].shape_y2 - fine_shapes[i].shape_y1 )/(fine_shapes[i].shape_x2 - fine_shapes[i].shape_x1 );
			var b = fine_shapes[i].shape_y1 - (m * fine_shapes[i].shape_x1 );

			// any point belongs to the line should satisfy its equation
			var left_side= y - (m * x);
			var right_side=b;
			//var error = 100;
			lasterror=Math.abs(left_side-right_side);
			//if(lasterror  <= error )
			//{
			//	errors.push({index:i, error:lasterror});
			//}
			var xstart,xend;
			if(fine_shapes[i].shape_x1<fine_shapes[i].shape_x2)
			{
				xstart=fine_shapes[i].shape_x1;
				xend=fine_shapes[i].shape_x2;
			}
			else {
				xstart=fine_shapes[i].shape_x2;
				xend=fine_shapes[i].shape_x1;
			}

			if(lasterror<5 && xstart <= x && xend>=x)
			return i;
		}
	}
		return -1;
 }
 /**
  * [BindTextToShape writes the text inside the shape]
  * @param       {Object} shape [The shape object]
  * @param       {String} text  [The text to be written]
  */
function BindTextToShape(shape, text)
{
	var x1,y1,w,h,fontSize;
	if(shape.shape_type==LINE_SHAPE)
	{
		x1=(shape.shape_x1+shape.shape_x2)/2;
		y1=(shape.shape_y1+shape.shape_y2)/2;
		w=5;
		h=5;
		fontSize=20;
		if(shape.shape_name.startsWith(RECURSIVE_RELATION))
		{
			out=shape.shape_name.substr(9);//the length of the reserved keyword recursive
			out=out.trim();
			text=out.substr(0,out.indexOf(")")+1);
		}
	}
	else if(shape.shape_type==ENTITY_SHAPE)
	{
		x1=shape.shape_x1;
		y1=shape.shape_y1;
		w=shape.shape_width;
		h=shape.shape_height;
		fontSize=shape.shape_width/6;
	}
	else
	{
		y1=shape.shape_y1-shape.shape_diameter;
		w= 2*shape.shape_diameter;
		h= 2*shape.shape_diameter;
		if(shape.shape_type==RELATION_SHAPE)
		{
			x1=shape.shape_x1-shape.shape_diameter+5;
			fontSize=shape.shape_diameter/3;
		}

		else {
			x1=shape.shape_x1-shape.shape_diameter;
			fontSize=shape.shape_diameter/2;
		}
	}

	context.rect(x1,y1,w,h);
	context.fillStyle = "black";
	context.font = fontSize+"px sans-serif";
	context.fillText(text, x1+5, y1+(0.5*h));
}
/**
 * [BindLineToShapes Connects the line between 2 shapes]
 * @param       {Int} id_first_shape  [The Id of the 1st shape]
 * @param       {Int} id_second_shape [The Id of the 2nd shape]
 * @param       {Object} output          [the line object]
 */
function BindLineToShapes(id_first_shape,id_second_shape,output)
{
	output.id_shape_1=id_first_shape;
	output.id_shape_2=id_second_shape;
	return output;
}
/**
* Adds a point to the drawing array.
* @param Px
* @param Py
*/
function addClick(Px, Py)
{
	var point={x:Px,y:Py};
	moves.push(point);
}

/**
 * [clearCanvas Clears the canvas.]
 */
function clearCanvas()
{
	context.clearRect(0, 0, canvasWidth, canvasHeight);
	context_lines.clearRect(0, 0, canvasWidth, canvasHeight);
}
/**
 * [draw A function used to draw the mouse stroks]
 * @param  {Array} points [Array of the points to be drawn]
 * @param  {Int} color  [The color of the shape]
 */
function draw(points,color)
{
	context.strokeStyle = color;//"#df4b26";
  context.lineJoin = "round";
  context.lineWidth = 1;
  for(var i=0; i < points.length; i++)
	{
    context.beginPath();
		if(i)
		 {
			context.moveTo(points[i-1].x, points[i-1].y);
     }
		 else
		 {
			context.moveTo(points[i].x, points[i].y);
     }
	   context.lineTo(points[i].x, points[i].y);
     context.closePath();
     context.stroke();
  }
}
/**
 * [drawList A function used with the drawShapes to draw the whole shapes array]
 * @param  {Array} shapes [The shapes array]
 * @param  {Int} color  [The color of the shapes]
 */
function drawList(shapes, color)
{
	//to clear text on the canvas
	context.rect(0,0,canvasWidth,canvasHeight);
	context.fillStyle = "black";
	context.font = "20pt sans-serif";
	context.fillText("", 0, 0);
	var tempcol;
	for(var i=0;i<shapes.length;i++)
	{
		//draw(shapes[i],color);
		tempcol=color;
		if(shapes[i].shape_type==LINE_SHAPE)
		{
			tempcol='DarkCyan';
		}
		DrawFineShapes(shapes[i],tempcol);
		if(shapes[i].shape_name!="")
		{
			BindTextToShape(shapes[i],shapes[i].shape_name);
		}
	}
}
/**
 * [drawShapes A function used to draw the whole shapes]
 */
function drawShapes()
{
 clearCanvas();
 draw(moves,"#000000");
 		if(fine_shapes.length!=0)
			drawList(fine_shapes,globalColor);//,"#fff000");
	}
/**
 * [clearDrawings A function used to clear all drawn shapes form the canvas]
 */
function clearDrawings ()
{
  context.clearRect(0, 0, canvasWidth, canvasHeight);
   moves=[];
	 shapes_simplify=[];
	 shapes_rectangle_inside=[];
	 shapes_rectangle_outside=[];
	 shapes_triangle=[];
	 shapes_convex_hull=[];
	 fine_shapes=[];
}
/**
 * [clearMoves a helper function to clear the moves array, that holds the mouse strokes]
 */
function clearMoves()
{
	moves=[];
}

//////////////////////
/**
 * [findShapeById a helper function used to search for a shape by its generated ID]
 * @param  {Array} fine_shapes [Array of shapes]
 * @param  {Int} target_ID   [The generated Id of the object]
 * @return {Object}             [returns the shape if found Otherwise ""]
 */
function findShapeById(fine_shapes,target_ID)
{
	for (var i = 0; i < fine_shapes.length; i++) {
		if(fine_shapes[i].id==target_ID)
			return fine_shapes[i];
	}
	return "";
}
//////////////////////////////////////

/**
 * [DrawFineShapes A function used to draw each shape according to the LINE_SHAPE if it is dashed/dotted/Normal line and the color]
 * @param       {Object} shape [The Object to be drawn]
 * @param       {Int} color [The color of the shape ]
 */
function DrawFineShapes(shape, color)
{
	//clearCanvas();
	//drawShapes();
	if (color ==null)
		color=globalColor;
	context.strokeStyle =color;
	context.fillStyle = 'white';
		context.beginPath();
		context_lines.strokeStyle =color;
		context_lines.fillStyle = 'DarkCyan';
			context_lines.beginPath();
		//canvas.setAttribute('z-index', 1);
	if(shape.shape_type == ATTRIBUTE_SHAPE)
	{
		if(shape.shape_line ==DOTTED_LINE)//dotted line =0
		{
			context.setLineDash([5, 3]);/*dashes are 5px and spaces are 3px*/
		}
		else if (shape.shape_line ==DOUBLE_LINE)//double line=2
		{
			context.ellipse(shape.shape_x1,shape.shape_y1,shape.shape_diameter+3,shape.shape_diameter+23,90 * Math.PI/180, 0, 2 * Math.PI);
		}
			context.ellipse(shape.shape_x1,shape.shape_y1,shape.shape_diameter,shape.shape_diameter+20,90 * Math.PI/180, 0, 2 * Math.PI);
			context.fill();
			context.stroke();if(shape.key!=-1)
			{
				if(shape.key==0){
					context.setLineDash([5, 3]);
				}
		       context.moveTo(shape.shape_x1-shape.shape_diameter, shape.shape_y1+3);
		       context.lineTo(shape.shape_x1+shape.shape_diameter, shape.shape_y1+3);
		       context.stroke();
			}
			context.setLineDash([]);
	}
	else if (shape.shape_type == ENTITY_SHAPE) {
		/*if(shape.shape_line ==0)//dotted line
		{
			context.setLineDash([5, 3]); //dashes are 5px and spaces are 3px
		}
		else */if (shape.shape_line ==DOUBLE_LINE)//double line
		{
			context.rect(shape.shape_x1-3,shape.shape_y1-3,shape.shape_width+6,shape.shape_height+6);
		}
					context.rect(shape.shape_x1,shape.shape_y1,shape.shape_width,shape.shape_height);
					context.fill();
			context.stroke();

			context.setLineDash([]);
	}
	else if (shape.shape_type == RELATION_SHAPE) {
	  if(shape.shape_line ==DOUBLE_LINE)//double line
		{
			context.moveTo(shape.shape_x1,shape.shape_y1+shape.shape_diameter+3);
			context.lineTo(shape.shape_x1-shape.shape_diameter-3,shape.shape_y1);//-diameter_circle);
			context.lineTo(shape.shape_x1,shape.shape_y1-shape.shape_diameter-3);//2*diameter_circle
			context.lineTo(shape.shape_x1+shape.shape_diameter+3,shape.shape_y1);//-diameter_circle);
			//context.closePath();
			context.lineTo(shape.shape_x1,shape.shape_y1+shape.shape_diameter+3);
			context.stroke();
		}
		/*else if(shape.shape_line ==0)// dotted line
			{
				context.setLineDash([5, 3]); //dashes are 5px and spaces are 3px
			}*/
			///Otherwise we have to draw the shape
		context.moveTo(shape.shape_x1,shape.shape_y1+shape.shape_diameter);
		context.lineTo(shape.shape_x1-shape.shape_diameter,shape.shape_y1);//-diameter_circle);
		context.lineTo(shape.shape_x1,shape.shape_y1-shape.shape_diameter);//2*diameter_circle
		context.lineTo(shape.shape_x1+shape.shape_diameter,shape.shape_y1);//-diameter_circle);
		context.lineTo(shape.shape_x1,shape.shape_y1+shape.shape_diameter);
		//context.closePath();
		context.fill();
				context.stroke();
		context.setLineDash([]);
	}
	else if (shape.shape_type==LINE_SHAPE) {
	//	canvas.setAttribute('z-index', 2);
		var shape1=findShapeById(fine_shapes,shape.id_shape_1) ;
		var shape2=findShapeById(fine_shapes,shape.id_shape_2) ;
		shape.shape_x1=shape1.shape_x2;
		shape.shape_y1=shape1.shape_y2;
		shape.shape_x2=shape2.shape_x2;
		shape.shape_y2=shape2.shape_y2;
	context_lines.moveTo(shape.shape_x1,shape.shape_y1);
	context_lines.lineTo(shape.shape_x2,shape.shape_y2);

	if(shape1.shape_type ==ENTITY_SHAPE && shape2.shape_type ==ENTITY_SHAPE && (shape1.shape_name.toLowerCase().endsWith("_p")||shape2.shape_name.toLowerCase().endsWith("_p"))){
		parentEntity=((shape1.shape_name.toLowerCase().endsWith("_p"))?shape1:(shape2.shape_name.toLowerCase().endsWith("_p"))?shape2:null);
		childEntity=((shape1.shape_name.toLowerCase().endsWith("_p"))?shape2:shape1);
		drawArrowbetweenParentandChildEntities(parentEntity,childEntity,shape);
	}

	context_lines.stroke();

	drawRecursiveLine(context_lines,shape,fine_shapes,color);
	}
	context.closePath();
	context_lines.closePath();
	centerX_circle=0;centerY_circle=0;  diameter_circle=0; EnclosedRec_Width=0; EnclosedRec_Height=0; start_point=null;end_point=null;
}
/**
 * [drawArrowbetweenParentandChildEntities A function used to draw an arrow between the parent and the child entities]
 * @param  {Object} parentEntity [The parent entity object]
 * @param  {Object} childEntity  [The child entity object]
 * @param  {Object} line         [The line that connects between the 2 entities object]
 */
function drawArrowbetweenParentandChildEntities(parentEntity,childEntity,line){
	Midpoint_x= (line.shape_x1+line.shape_x2)/2;
	Midpoint_y= (line.shape_y1+line.shape_y2)/2;

	var headlen = 15;// length of head in pixels
	//var tox=Midpoint_x, toy=Midpoint_y;
		var angle = Math.atan2(parentEntity.shape_y2-childEntity.shape_y2,parentEntity.shape_x2-childEntity.shape_x2);
		//context.moveTo(fromx, fromy);
		context_lines.moveTo(Midpoint_x, Midpoint_y);
		context_lines.lineTo(Midpoint_x-headlen*Math.cos(angle-Math.PI/6),Midpoint_y-headlen*Math.sin(angle-Math.PI/6));
		context_lines.moveTo(Midpoint_x, Midpoint_y);
		context_lines.lineTo(Midpoint_x-headlen*Math.cos(angle+Math.PI/6),Midpoint_y-headlen*Math.sin(angle+Math.PI/6));
}
/**
 * [drawRecursiveLine A function used to draw the recursive relation between an entity and a relation]
 * @param  {Object} context     [The context of the canvas we are drawing in.]
 * @param  {Object} line        [The recursive line object]
 * @param  {Array} fine_shapes [Array of shapes]
 * @param  {Int} color       [the line color in hexadecimal]
 */
function drawRecursiveLine(context,line,fine_shapes,color)
{
	if(line.shape_name.startsWith(RECURSIVE_RELATION))
	{

		context.closePath();
		var rect=findShapeById(fine_shapes,line.id_shape_1);
		context.strokeStyle =color;
		context.beginPath();
		context.moveTo(line.shape_x2,line.shape_y2);
		context.lineTo(rect.shape_x1+rect.shape_width,rect.shape_y1);
		context.stroke();
		x1=(line.shape_x2+rect.shape_x1+rect.shape_width)/2;
		y1=(line.shape_y2+rect.shape_y1+rect.shape_height)/2;
		w=5;
		h=5;
		fontSize=20;
		context.rect(x1,y1,w,h);
		context.fillStyle = "black";
		context.font = fontSize+"px sans-serif";
		var text=line.shape_name.substr(line.shape_name.indexOf(")")+1);
		//len=text.length;
		context.fillText(text/*[len-1]*/, x1+5, y1+(0.5*h));
	}
}
/**
 * [setHighestObjectID A function that is used to generate the id for each object. This generated id is used to search for an object.]
 * @param {Array} fine_shapes [The array of shapes, to search for the highest_id and then add 1 and return the new id]
 * @return {Int}the highest_id
 */
function setHighestObjectID(fine_shapes)
{
	if(fine_shapes.length==1)
		return fine_shapes[0].id+1;
	var highest_id=fine_shapes[0].id;
	for (var i = 1; i < fine_shapes.length; i++)
	 {
			if(highest_id<fine_shapes[i].id)
				highest_id=fine_shapes[i].id;
	 }
	 return highest_id+1;
}
