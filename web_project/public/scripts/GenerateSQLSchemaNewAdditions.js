/**
* <h1>Extract SQL schema!</h1>
* Extract SQL schema is used to extract the SQL script as follows:
* <ol>
* <li>transformCoreEntity: Generates the SQL script for all entities that are not weak or child entities.</li>
* <li>transformSubtypesEntities:  Generate the SQL script for the child entities</li>
* <li>transformWeakEntity: Generate the SQL script for the weak entities</li>
* <li>ExtractSQLforAllRelations: Generate the SQL script for each relation</li>
* <li>TransformStructuredAttributes: Generate the SQL script for the structured attribute</li>
* <li>TransformMultivaluedattributes: Generate the SQL script for the Multivaluedattribute</li>
* </ol>
 * [checkedShapes description]
 * @author  Omnia Kahla
 * @version 1.0
 * @since   2018-10-05
 *
 */
var checkedShapes=new Array();//array of ids so I am not checking the same object again and again.
var dbname="dbname";
var droppedTables=new Array();
/**
 * [ExtractSQLschema_fn The main function to extract the SQL script]
 * @param       {Array} shapes [Shapes array]
 * @param       {Array} Types  [The Types array]
 * @return {String} SQL script
 */
function ExtractSQLschema_fn(shapes, Types)
{
  var SQLschema="";
  checkedShapes.splice(0);
  SQLschema+=ExtractSQLforAllEntities(shapes,Types);
  if(SQLschema!="")
    SQLschema+=ExtractSQLforAllRelations(shapes,Types);
  else {
    alert("You cannot extract schema with no entities.");
  }
  return SQLschema;
}
/**
 * [ExtractSQLforAllEntities Searches for all entities and extract the sql script for each entity]
 * @param       {Array} shapes [Shapes array]
 * @param       {Array} Types  [The Types array]
 * @return {String}returns the SQL script
 */
function ExtractSQLforAllEntities(shapes,Types)
{
  var schema="";
  for(var i=0;i<shapes.length;i++)
  {
    var currentshape=shapes[i];
    if(currentshape.shape_type==ENTITY_SHAPE)
    {
      if(!checkedShapes.includes(currentshape.id))
      {
        if(currentshape.shape_name.toLowerCase().endsWith("_p"))
        {
          schema+=transformCoreEntity(shapes,shapes[i].id,Types);
          var EntitiesList=GetConnectedObjects(shapes,shapes[i].id,ENTITY_SHAPE);
          if(EntitiesList.length>0)
            schema+=transformSubtypesEntities(shapes,EntitiesList,currentshape,Types);
        }
      else if(currentshape.shape_line==2)
      {
        schema+=transformWeakEntity(shapes,shapes[i].id,Types);
      }
      else
      {
        schema+=transformCoreEntity(shapes,shapes[i].id,Types);
      }
    }
  }
  }
  return schema;
}
/**
 * [ExtractSQLforAllRelations Generate the SQL script for each relation]
 * @param      {Array} shapes [Shapes array]
 * @param       {Array} Types  [The Types array]
 *  @return {String}returns the SQL script
 */
function ExtractSQLforAllRelations(shapes,Types)
{
  var schema="",pk="",col="";
  for(var i=0;i<shapes.length;i++)
  {
    var currentshape=shapes[i];
    if(currentshape.shape_type==RELATION_SHAPE && currentshape.shape_line!=2) //if it is a weak relation we already have called transfer weak entity
    {
      if(!checkedShapes.includes(currentshape.id))
      {
        var attributesList=GetConnectedObjects(shapes,shapes[i].id,ATTRIBUTE_SHAPE);
        var connectedEntities=GetConnectedObjects(shapes,shapes[i].id,ENTITY_SHAPE);
        var Lines= GetAllConnectedLines(shapes,shapes[i].id);
        var col_Type;
        if(attributesList.length>0 ||connectedEntities.length>2)
        {
          schema+=TransformRelationToANewTable(connectedEntities,shapes[i],shapes,Types,[],false);
        }
        else if(connectedEntities.length==1){//it is a RECURSIVE_RELATION
          schema+=TransformRecursiveRelation_SQL(connectedEntities[0],attributesList,shapes,Types,Lines,shapes[i]);
        }
        else
        {
            var Linescardinality=new Array();
            for (var j = 0; j < Lines.length; j++) {
              if((Lines[j].id_shape_1==shapes[i].id && shapes[SearchById(shapes,Lines[j].id_shape_2)].shape_type==ENTITY_SHAPE)||
              (Lines[j].id_shape_2==shapes[i].id && shapes[SearchById(shapes,Lines[j].id_shape_1)].shape_type==ENTITY_SHAPE))
                Linescardinality.push(Lines[j].shape_name);
            }
            if(Linescardinality[0].shape_name=="1..*" && Linescardinality[1].shape_name=="1..*") //so it is a M..N relation
            {
              schema+=TransformRelationToANewTable(connectedEntities,shapes[i],shapes,Types,[],true);
            }
            else if(Linescardinality[0].shape_name=="1..1" && Linescardinality[1].shape_name=="1..1") //so it is a 1..1 relation
            {
              schema+=Transform1To1And1ToMRelation(connectedEntities[0],connectedEntities[1],shapes[i],Types);
            }
            else  //so it is a 1..M relation
            {
              var MainEntity=((Linescardinality[0].shape_name=="1..M")?connectedEntities[1]:connectedEntities[0]);
              var SecondEntity =((Linescardinality[0].shape_name=="1..M")?connectedEntities[0]:connectedEntities[1]);
              schema+=Transform1To1And1ToMRelation(MainEntity,SecondEntity,shapes[i],Types);
            }
          }
        }
      }
    }
    return schema;
  }
/**
 * [Transform1To1And1ToMRelation description]
 * @param       {Object} MainEntity [The first Entity]
 * @param       {Object} SecondEntity       [The Second Entity]
 * @param       {Object} relation          [The relationship object]
 * @param       {Array} Types             [The Types array]
 * @return {String}[schema]
 */
function Transform1To1And1ToMRelation(MainEntity,SecondEntity,relation,Types){
  var schema="";
  pk=getParentShape_Key(MainEntity,Types);
  col_Type=GetTypeOfAttribute(MainEntity.shape_name,pk,Types);
  schema+="ALTER TABLE "+dbname+"."+SecondEntity.shape_name +" ADD "+ MainEntity.shape_name +"_"+pk+ " ";
  schema+=((col_Type.attribute_type_parameters =="") ? col_Type.attribute_type: col_Type.attribute_type.substring(0, (col_Type.attribute_type.indexOf("("))))
                           +((col_Type.attribute_type_parameters !="") ? " ("+col_Type.attribute_type_parameters+") " : "");
  schema+=" ;\n";
  schema+="ALTER TABLE "+dbname+"."+SecondEntity.shape_name +" ADD FOREIGN KEY ("+MainEntity.shape_name +"_"+pk+") REFERENCES ";
  schema+=dbname+"."+MainEntity.shape_name +"("+pk+");\n";
  return schema;
}
/**
 * [TransformRecursiveRelation_SQL Transforms a recursive relationship]
 * @param {Object}Entity [The connected Entity]
 * @param       {Array} attributesList [The connected attributes Array]
 * @param       {Array} shapes         [The shapes Array]
 * @param       {Array} Types          [The Types Array]
 * @param       {Array} Lines          [The connected lines to the relationship]
 * @param       {Object} relation       [The relationship Object]
 * @return {String} [schema]
 */
function TransformRecursiveRelation_SQL(Entity,attributesList,shapes,Types,Lines,relation){
  var schema="",AddImportantStmtsAttheEnd="";
  var generatedPK="" ;
  var roles=new Array();
  var Cardinalities=new Array();
  var mainRole="";
  for (var i = 0; i < Lines.length; i++) {
    if ((Lines[i].id_shape_1==relation.id && Lines[i].id_shape_2==Entity.id)||(Lines[i].id_shape_2==relation.id && Lines[i].id_shape_1==Entity.id)) {
      temp=Lines[i].shape_name.substr(9);//removes the keyword recursive
      temp=temp.trim();
      roles.push(temp.substr(0,temp.indexOf("(")));//added the first role
      Cardinalities.push(temp.substr(temp.indexOf("(")+1,4));//added the first Cardinality
      temp2=temp.substr(temp.indexOf(")")+1);
      roles.push(temp2.substr(0,temp2.indexOf("(")));//added the second role
      Cardinalities.push(temp2.substr(temp2.indexOf("(")+1,4));//added the second Cardinality
    }
  }
  if((Cardinalities[0]=="1..*" ||Cardinalities[0]=="0..*")&&(Cardinalities[1]=="1..*" ||Cardinalities[1]=="0..*") ){
    schema+=TransformRelationToANewTable([Entity], relation,shapes,Types,roles,true);
  }
  else{
      if(Cardinalities[0]=="1..1"){//||Cardinalities[0]=="0..*"){
        mainRole=roles[0];
      }
      else {
        mainRole=roles[1];
      }
      col=getParentShape_Key(Entity,Types);
      col_Type=GetTypeOfAttribute(Entity.shape_name,col,Types);
      schema+="ALTER TABLE "+dbname+"."+Entity.shape_name +" ADD "+ mainRole+ " ";
      schema+=((col_Type.attribute_type_parameters =="") ? col_Type.attribute_type: col_Type.attribute_type.substring(0, (col_Type.attribute_type.indexOf("("))))
                             +((col_Type.attribute_type_parameters !="") ? " ("+col_Type.attribute_type_parameters+") " : "");
      schema+=";\n";
      schema+="ALTER TABLE "+dbname+"."+Entity.shape_name +" ADD FOREIGN KEY ("+mainRole+") REFERENCES ";
      schema+=dbname+"."+Entity.shape_name +"("+col+");\n";
    }
  return schema;
}
/**
 * [TransformRelationToANewTable A function used to transform the relationship if  it has multiple connected entities to a new table]
 * @param  {Array} connectedEntities [The connectedEntities Array]
 * @param  {Object} relation          [The relationship Object]
 * @param  {Array} shapes            [The shapes Array]
 * @param  {Array} Types             [The Types Array]
 * @param {Array}roles [In case it is a recursive relation, so we will need the roles]
 * @param {Boolean } ManyToManyRelation [A Boolean var to indicate whether it is a M..N relation or we have to check it first]
 * @return {String}                   [schema]
 */
function TransformRelationToANewTable(connectedEntities,relation,shapes,Types,roles,ManyToManyRelation){
  var schema="";
  schema+=transformCoreEntity(shapes,relation.id,Types);
  generatedPK=getParentShape_Key(relation,Types);
  pk="";
  if(roles.length==0){
    for (var j = 0; j < connectedEntities.length; j++) {
      col=getParentShape_Key(connectedEntities[j],Types);
      pk+=col;
      col_Type=GetTypeOfAttribute(connectedEntities[j].shape_name,col,Types);
      schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" ADD "+col+ " ";
      schema+=((col_Type.attribute_type_parameters =="") ? col_Type.attribute_type: col_Type.attribute_type.substring(0, (col_Type.attribute_type.indexOf("("))))
                               +((col_Type.attribute_type_parameters !="") ? " ("+col_Type.attribute_type_parameters+") " : "");
      schema+=" NOT NULL;\n";

      schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" ADD FOREIGN KEY ("+col+") REFERENCES ";
      schema+=dbname+"."+connectedEntities[j].shape_name+"("+col+");\n";

      if(j<connectedEntities.length-1)
      pk+=", ";
    }
  }
  else if(roles.length!=0){ //it is a M..N recursive relation
    col=getParentShape_Key(connectedEntities[0],Types);
    pk=roles[0]+","+roles[1];
    col_Type=GetTypeOfAttribute(connectedEntities[0].shape_name,col,Types);
    attribute_type=((col_Type.attribute_type_parameters =="") ? col_Type.attribute_type: col_Type.attribute_type.substring(0, (col_Type.attribute_type.indexOf("("))))
                             +((col_Type.attribute_type_parameters !="") ? " ("+col_Type.attribute_type_parameters+") " : "");
    schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" ADD "+roles[0]+ " "+attribute_type;
    schema+=" NOT NULL;\n";
    schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" ADD "+roles[1]+ " "+attribute_type;
    schema+=" NOT NULL;\n";

    schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" ADD FOREIGN KEY ("+roles[0]+") REFERENCES ";
    schema+=dbname+"."+connectedEntities[0].shape_name+"("+col+");\n";
    schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" ADD FOREIGN KEY ("+roles[1]+") REFERENCES ";
    schema+=dbname+"."+connectedEntities[0].shape_name+"("+col+");\n";

  }
  schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" DROP PRIMARY KEY;\n";
  schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" DROP COLUMN "+generatedPK+";\n";
  //if(ManyToManyRelation){
    schema+="ALTER TABLE "+dbname+"."+relation.shape_name +" ADD CONSTRAINT PK_"+relation.shape_name +" PRIMARY KEY ("+pk+");\n";
  //  return schema;
//  }

//  for (var j = 0; j < connectedEntities.length; j++) {
//    var line=getLinebetweenRelationAndEntity(shapes,relation.id,connectedEntities[j].id);

//  }
  return schema;
}
/**
 * [transformCoreEntity Generate the SQL script for all entities that are not weak entities or child entites]
 * @param {Array} shapes [Shapes array]
 * @param  {Int} id     [The generated Id for the entity]
 * @param  {Array} Types  [The Types array]
 *  @return {String}returns the SQL script
 */
function transformCoreEntity(shapes,id,Types)
{
  var attributesList=GetConnectedObjects(shapes,id,ATTRIBUTE_SHAPE);
  var Entity=shapes[SearchById(shapes,id)];
  droppedTables.push(Entity.shape_name);
  var schema="CREATE TABLE "+dbname+"."+Entity.shape_name +" (";
  var pk=getParentShape_Key(Entity,Types) ;//Entity.shape_name +"_ID";
  var Multivaluedattribute=new Array;
  var col_Type;
  checkedShapes.push(Entity.id);
  if(attributesList.length>0){
    for (var i = 0; i < attributesList.length; i++) {
      var ConnectedAttributestotheAttribute = GetConnectedObjects(shapes,attributesList[i].id,ATTRIBUTE_SHAPE);
      if(ConnectedAttributestotheAttribute.length>0)//structured attribute
      {
        checkedShapes.push(attributesList[i].id);
        schema+=TransformStructuredAttributes(shapes,Entity.shape_name,ConnectedAttributestotheAttribute,attributesList[i],Types);
        schema+=", ";
      }
      else if(attributesList[i].shape_line==2)//Multivalued attribute
      {
        checkedShapes.push(attributesList[i].id);
        Multivaluedattribute.push(attributesList[i]);
      }
      else if(attributesList[i].shape_name==pk)
       {
           continue;
       }
       else { //normal attibute
         checkedShapes.push(attributesList[i].id);
         col_Type=GetTypeOfAttribute(Entity.shape_name,attributesList[i].shape_name,Types);
         schema+=attributesList[i].shape_name +" "+((col_Type.attribute_type_parameters =="") ? col_Type.attribute_type: col_Type.attribute_type.substring(0, (col_Type.attribute_type.indexOf("("))))
                                  +((col_Type.attribute_type_parameters !="") ? " ("+col_Type.attribute_type_parameters+")  " : "  ");
          if(col_Type.attribute_constraints.length!=0)
          {
            for(var x=0;x<col_Type.attribute_constraints.length;x++)
            {
              if (col_Type.attribute_constraints[x] =="CHECK"){
                schema+=col_Type.attribute_constraints[x]+" "+((col_Type.attribute_constraints_parameters.trim().startsWith("("))?col_Type.attribute_constraints_parameters+((col_Type.attribute_constraints_parameters.trim().endsWith(")"))?" ":") ") :"("+col_Type.attribute_constraints_parameters+((col_Type.attribute_constraints_parameters.endsWith(")"))?" ":") ")) ;
              }
              else if (col_Type.attribute_constraints[x]=="DEFAULT") {
                schema+=col_Type.attribute_constraints[x]+" ";
                attribute_constraints_parameters=col_Type.attribute_constraints_parameters.trim();
                attribute_constraints_parameters=((attribute_constraints_parameters.endsWith(")"))?attribute_constraints_parameters.substring(0,attribute_constraints_parameters.length-1):attribute_constraints_parameters);
                attribute_constraints_parameters=((attribute_constraints_parameters.startsWith("("))?attribute_constraints_parameters.substring(1):attribute_constraints_parameters);
                //attribute_constraints_parameters=attribute_constraints_parameters.replace(/\"/g,'\'');
                schema+=attribute_constraints_parameters;
                //schema+=((col_Type.attribute_constraints_parameters.startsWith("("))?col_Type.attribute_constraints_parameters.substring(1):col_Type.attribute_constraints_parameters);
                //schema+=((col_Type.attribute_constraints_parameters.endsWith(")"))?schema.substring(0,schema.length-1):" ");
              }
              else {
                schema+=col_Type.attribute_constraints[x] +" ";
              }
            }
          }
          schema+=" , ";
       }
    }
  }
  else{
    var pk_Type=GetTypeOfAttribute(Entity.shape_name,pk,Types);
    schema+=pk+" "+((pk_Type.attribute_type_parameters =="") ? pk_Type.attribute_type: pk_Type.attribute_type.substring(0, (pk_Type.attribute_type.indexOf("("))))
                             +((pk_Type.attribute_type_parameters !="") ? " ("+pk_Type.attribute_type_parameters+")" : "");
    schema+=" NOT NULL PRIMARY KEY);\n";
    return schema;
    }
    var pk_Type=GetTypeOfAttribute(Entity.shape_name,pk,Types);
    schema+=pk+" "+((pk_Type.attribute_type_parameters =="") ? pk_Type.attribute_type: pk_Type.attribute_type.substring(0, (pk_Type.attribute_type.indexOf("("))))
                             +((pk_Type.attribute_type_parameters !="") ? " ("+pk_Type.attribute_type_parameters+")" : "");
    schema+=" NOT NULL PRIMARY KEY);\n";
    if(Multivaluedattribute.length>0)
    {
      for (var i = 0; i < Multivaluedattribute.length; i++) {
        schema+=TransformMultivaluedattributes(Multivaluedattribute[i],pk,Entity.shape_name,Types);
      }
    }
    return schema;
}
/**
 * [transformSubtypesEntities Generate the SQL script for the child entities for a parent entity] *
 * @param {Array} shapes [Shapes array]
 * @param  {Array} EntitiesList [Array of the Entities list]
 * @param  {Object} parentShape  [The parent shape object]
 * @param  {Array} Types  [The Types array]
 *  @return {String}returns the SQL script
 */
function transformSubtypesEntities(shapes,EntitiesList,parentShape,Types)
{
  var pk= getParentShape_Key(parentShape,Types);
  var schema="";
  var pk_Type_temp = GetTypeOfAttribute(parentShape.shape_name, pk,Types);
  var pk_Type=((pk_Type_temp.attribute_type_parameters =="") ? pk_Type_temp.attribute_type: pk_Type_temp.attribute_type.substring(0, (pk_Type_temp.attribute_type.indexOf("("))))
                           +((pk_Type_temp.attribute_type_parameters !="") ? " ("+pk_Type_temp.attribute_type_parameters+") " : "");
  pk_Type+=" NOT NULL;\n";
  for (var i = 0; i < EntitiesList.length; i++) {
    if(!checkedShapes.includes(EntitiesList[i].id))
    {
      schema+=transformCoreEntity(shapes,EntitiesList[i].id,Types);
      schema+="ALTER TABLE "+dbname+"."+EntitiesList[i].shape_name +" ADD "+ parentShape.shape_name+"_"+pk+" "+ pk_Type;
      schema+="ALTER TABLE "+dbname+"."+EntitiesList[i].shape_name +" DROP PRIMARY KEY;\n";
      schema+="ALTER TABLE "+dbname+"."+EntitiesList[i].shape_name +" DROP COLUMN "+EntitiesList[i].shape_name+"_ID"+";\n";
      schema+="ALTER TABLE "+dbname+"."+EntitiesList[i].shape_name +" ADD CONSTRAINT PK_"+EntitiesList[i].shape_name +" PRIMARY KEY ("+parentShape.shape_name+"_"+pk+");\n";
      schema+="ALTER TABLE "+dbname+"."+EntitiesList[i].shape_name +" ADD FOREIGN KEY ("+parentShape.shape_name+"_"+pk+") REFERENCES ";
      schema+=dbname+"."+parentShape.shape_name +"("+pk+");\n";
    }
  }
  return schema;
}
/**
 * [transformWeakEntity Generate the SQL script for the weak entities]
 * @param {Array} shapes [Shapes array]
 * @param  {Int} id     [The generated Id for the entity]
 * @param  {Array} Types  [The Types array]
 *  @return {String}returns the SQL script
 */
function transformWeakEntity(shapes,id,Types)
{
  var schema="",pk="",col="";
  var coreEntity=shapes[SearchById(shapes,id)];
  var col_Type;
  if(!checkedShapes.includes(id))
  {
    schema=transformCoreEntity(shapes,id,Types);
    schema+="ALTER TABLE "+dbname+"."+coreEntity.shape_name +" DROP PRIMARY KEY;\n";
    var entities = GetAllconnectedEntitiesbyaWeakrealtion(shapes,coreEntity.id);
    pk+=getParentShape_Key(coreEntity,Types);
    for (var i = 0; i < entities.length; i++) {
      col=getParentShape_Key(entities[i],Types);
      col_Type=GetTypeOfAttribute(entities[i].shape_name,col,Types);
      pk+=", "+col;
      schema+="ALTER TABLE "+dbname+"."+coreEntity.shape_name +" ADD "+col+" ";
      schema+=((col_Type.attribute_type_parameters =="") ? col_Type.attribute_type: col_Type.attribute_type.substring(0, (col_Type.attribute_type.indexOf("("))))
                               +((col_Type.attribute_type_parameters !="") ? " ("+col_Type.attribute_type_parameters+") " : "");
      schema+=" NOT NULL;\n";
      schema+="ALTER TABLE "+dbname+"."+coreEntity.shape_name +" ADD FOREIGN KEY ("+col+") REFERENCES ";
      schema+=dbname+"."+entities[i].shape_name+"("+col+");\n";
    }
    schema+="ALTER TABLE "+dbname+"."+coreEntity.shape_name +" ADD CONSTRAINT PK_"+coreEntity.shape_name +" PRIMARY KEY ("+pk+");\n";
    return schema;
  }
  return schema;
}
/**
 * [TransformMultivaluedattributes Generate the SQL script for the Multivaluedattribute]
 * @param       {Object} Multivaluedattribute [The Multivaluedattribute object]
 * @param       {String} pk                   [The pk of the main Entity]
 * @param       {String} Entity_shape_name    [The Name of the main entity]
 * @param  {Array} Types  [The Types array]
*  @return {String}returns the SQL script
 */
function TransformMultivaluedattributes(Multivaluedattribute,pk,Entity_shape_name,Types)
{
  var schema="";
  var NewEntityName=Entity_shape_name +"_"+Multivaluedattribute.shape_name ;
  droppedTables.push(NewEntityName);
  schema+="CREATE TABLE "+dbname+"."+NewEntityName+" (";
  var pk_Type=GetTypeOfAttribute(Entity_shape_name,pk,Types);
  schema+=pk+" ";
  schema+=((pk_Type.attribute_type_parameters =="") ? pk_Type.attribute_type: pk_Type.attribute_type.substring(0, (pk_Type.attribute_type.indexOf("("))))
                           +((pk_Type.attribute_type_parameters !="") ? " ("+pk_Type.attribute_type_parameters+") " : "");
  schema+=" NOT NULL, ";
  var Multivalued_Type= GetTypeOfAttribute(NewEntityName,Multivaluedattribute.shape_name,Types);
  schema+=Multivalued_Type.attribute_name+" ";
  schema+=((Multivalued_Type.attribute_type_parameters =="") ? Multivalued_Type.attribute_type: Multivalued_Type.attribute_type.substring(0, (Multivalued_Type.attribute_type.indexOf("("))))
                           +((Multivalued_Type.attribute_type_parameters !="") ? " ("+Multivalued_Type.attribute_type_parameters+") " : " ");
  schema+=" NOT NULL, ";
  schema+="CONSTRAINT PK_"+NewEntityName+" PRIMARY KEY ("+pk_Type.attribute_name+" , "+Multivalued_Type.attribute_name+"));\n";//Entity_shape_name.replace(/\s+/g, '')+"_"+pk+" , "+ Multivaluedattribute.shape_name.replace(/\s+/g, '')+");\r ";

  schema+="ALTER TABLE "+dbname+"."+NewEntityName +" ADD FOREIGN KEY ("+pk_Type.attribute_name+") REFERENCES ";
  schema+=dbname+"."+Entity_shape_name+"("+pk+");\n";
  return schema;
}
/**
 * [TransformStructuredAttributes Generate the SQL script for the structured attribute]
 * @param {Array} shapes [The shapes array]
 * @param       {String} EntityName     [The Name of the main entity]
 * @param       {Array} attributelist [An array that contains all connected attributes to the main attribute]
 * @param       {Object} attribute     [The main attribute]
 * @param  {Array} Types  [The Types array]
*  @return {String}returns the SQL script
 */
function TransformStructuredAttributes(shapes,EntityName,attributelist,attribute,Types)
{
  var schema="";
  for (var i = 0; i < attributelist.length; i++) {
    if(!checkedShapes.includes(attributelist[i].id))
      {
      checkedShapes.push(attributelist[i].id);
      var ConnectedAttributestotheAttribute = GetConnectedObjects(shapes,attributelist[i].id,ATTRIBUTE_SHAPE);
      if(ConnectedAttributestotheAttribute.length>0)
      {
        schema+=TransformStructuredAttributes(shapes,EntityName,ConnectedAttributestotheAttribute,attributelist[i],Types);
      }
      var attributeName=attribute.shape_name +"_"+attributelist[i].shape_name ;
      var attributeType=GetTypeOfAttribute(EntityName,attributeName,Types);
      schema+=attributeName+" "+((attributeType.attribute_type_parameters =="") ? attributeType.attribute_type: attributeType.attribute_type.substring(0, (attributeType.attribute_type.indexOf("("))))
                               +((attributeType.attribute_type_parameters !="") ? " ("+attributeType.attribute_type_parameters+") " : "");

     if(attributeType.attribute_constraints.length!=0)
     {
       for(var x=0;x<attributeType.attribute_constraints.length;x++)
       {
         if (attributeType.attribute_constraints[x] =="CHECK"){
           schema+=attributeType.attribute_constraints[x]+" "+((attributeType.attribute_constraints_parameters.trim().startsWith("("))?attributeType.attribute_constraints_parameters+((attributeType.attribute_constraints_parameters.trim().endsWith(")"))?" ":") ") :"("+attributeType.attribute_constraints_parameters+((attributeType.attribute_constraints_parameters.endsWith(")"))?" ":") ")) ;
         }
         else if (attributeType.attribute_constraints[x]=="DEFAULT") {
           schema+=attributeType.attribute_constraints[x]+" ";
           attribute_constraints_parameters=attributeType.attribute_constraints_parameters.trim();
           attribute_constraints_parameters=((attribute_constraints_parameters.endsWith(")"))?attribute_constraints_parameters.substring(0,attribute_constraints_parameters.length-1):attribute_constraints_parameters);
           attribute_constraints_parameters=((attribute_constraints_parameters.startsWith("("))?attribute_constraints_parameters.substring(1):attribute_constraints_parameters);
           //attribute_constraints_parameters=attribute_constraints_parameters.replace(/\"/g,'\'');
           schema+=attribute_constraints_parameters;
           //schema+=((attributeType.attribute_constraints_parameters.startsWith("("))?attributeType.attribute_constraints_parameters.substring(1):attributeType.attribute_constraints_parameters);
           //schema+=((attributeType.attribute_constraints_parameters.endsWith(")"))?schema.substring(0,schema.length-1):" ");
         }
         else {
           schema+=attributeType.attribute_constraints[x] +" ";
         }
       }
     }

      if(i<attributelist.length-1)
        schema+=", ";
    }
  }
  return schema;
}
/**
 * [GetTypeOfAttribute Returns the attribute type from the Types array. In the Types array there is a field that contains entityName_attributeName, and we searches by this field. ]
 * @param       {String} entityName    [The main entity name]
 * @param       {String} attributeName [The attribute name]
 * @param  {Array} Types  [The Types array]
*  @return {Object} returns the Type object, if not found, then we are searching for the primary key and this table doesn't have a PK so we generate one that is int and PRIMARY KEY.
 */
function GetTypeOfAttribute(entityName,attributeName,Types)
{
  var Type;
  for (var i = 0; i < Types.length; i++) {
    if(Types[i].entityName_attributeName==entityName+"_"+attributeName){
      Type= Types[i];
      break;
    }
  }
  if(Type==null){//newly generated ID
    Type= {
      entity_name:entityName,//used in the UI
      attribute_name: attributeName,//used in the UI
      attribute_type:"int",//returned from UI removed attribute type to be int by default
      attribute_type_parameters:"",
      attribute_constraints:"",//returned from UI removed attribute constraint to be PRIMARY KEY by default
      attribute_constraints_parameters:"",
      entityName_attributeName:entityName+"_"+attributeName//used for searching
    };
  }
  return Type;
}
/**
 * [GetAllConnectedLines A helper function used to return all the connected lines to an object]
 * @param {Array} shapes [Shapes array]
 * @param       {Int} id     [The id of the Object we want to return all its connected lines]
 * @return {Array} an array with the connected lines.
 */
function GetAllConnectedLines(shapes,id) //return Lines
{
  var Lines=new Array();
  var CurrentEntity=shapes[SearchById(shapes,id)];
  for(var i=0;i<shapes.length;i++)
  {
      var Line=shapes[i];
  		if(Line.shape_type==LINE_SHAPE)
  		{
        if(Line.id_shape_1== CurrentEntity.id || Line.id_shape_2==CurrentEntity.id)
            Lines.push(Line);
      }
  }
  return Lines;
}
/**
 * [GetAllconnectedEntitiesbyaWeakrealtion A helper function used to return all the connected entites by a weak relation and they are not weak entites to return there pk]
 * @param {Array} shapes [Shapes array]
 * @param       {Int} id     [The id of the weak relation that we want to return all its connected entities]
 * @return {Array} an array with the connected entities.
 */
function GetAllconnectedEntitiesbyaWeakrealtion(shapes,id)//search for the weak relation and all the connected entities that are not weak to return there pk.
{
  var Entities=new Array();
  var WeakEntity=shapes[SearchById(shapes,id)];
  for(var i=0;i<shapes.length;i++)
  {
      var Line=shapes[i];
      if(Line.shape_type==LINE_SHAPE)
      {
        if((Line.id_shape_1== WeakEntity.id && shapes[SearchById(shapes,Line.id_shape_2)].shape_type==RELATION_SHAPE )&& shapes[SearchById(shapes,Line.id_shape_2)].shape_line== 2)
        {
          var EntitiesList=GetConnectedObjects(shapes,Line.id_shape_2,ENTITY_SHAPE);
          for (var j=0;j<EntitiesList.length;j++)
          {
            if (EntitiesList[j].shape_line!=2)
              Entities.push(EntitiesList[j]);
          }
        }
        else if((Line.id_shape_2== WeakEntity.id && shapes[SearchById(shapes,Line.id_shape_1)].shape_type==RELATION_SHAPE )&& shapes[SearchById(shapes,Line.id_shape_1)].shape_line== 2)
        {
          var EntitiesList=GetConnectedObjects(shapes,Line.id_shape_1,ENTITY_SHAPE);
          for (var j=0;j<EntitiesList.length;j++)
          {
            if (EntitiesList[j].shape_line!=2)
              Entities.push(EntitiesList[j]);
          }
        }
      }
  }
  return Entities;
}
/**
 * [getParentShape_Key A helper function to return the PK of an entity]
 * @param  {Object} parentShape [The entity object]
 * @param  {Array} Types  [The Types array]
 * @return {String}             [returns the PK]
 */
function getParentShape_Key(parentShape,Types)
{
  var pk="";
  for (var i = 0; i < Types.length; i++) {
    if(Types[i].entity_name==parentShape.shape_name && Types[i].attribute_constraints.includes("PRIMARY KEY")){
      pk+=Types[i].attribute_name +",";
    }
  }
  if(pk.indexOf(",")==pk.length-1){
    pk=pk.substring(0, pk.length - 1);
  }
  if(pk==""){
    pk=parentShape.shape_name +"_ID";
  }
  return pk;
}
