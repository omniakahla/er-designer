var undoElement,redoElement,Donebtn, importContentField, importContentElement;
/**
 * [initEditorButtons A function used to initialize the buttons in the editor element used for writing purpose]
 * @param  {Object} editorElement [The editor element in the GUI]
 * @param  {String} text          [This parameter is used when the user wants to update a text in object so he could find the old text to import and then update this text ]
 */
function initEditorButtons(editorElement,text){
	 undoElement = document.getElementById('undo');
	 redoElement = document.getElementById('redo');
	 Donebtn = document.getElementById('done');
	 importContentField = document.getElementById('importContentField');
   importContentElement = document.getElementById('importContent');
	 importContentField.innerHTML = ((text=="") ? "ex. property name" : text);

	  var undoElementClone = undoElement.cloneNode(true);
	  undoElement.parentNode.replaceChild(undoElementClone, undoElement);

		var DonebtnClone = Donebtn.cloneNode(true);
	  Donebtn.parentNode.replaceChild(DonebtnClone, Donebtn);

		var redoElementClone = redoElement.cloneNode(true);
		redoElement.parentNode.replaceChild(redoElementClone, redoElement);

		var importContentElementClone = importContentElement.cloneNode(true);
	  importContentElement.parentNode.replaceChild(importContentElementClone, importContentElement);
		undoElementClone.addEventListener('click', function () {
			editorElement.editor.undo();
		});
		redoElementClone.addEventListener('click', function () {
			editorElement.editor.redo();
		});
		editorElement.addEventListener('converted', function(event){
			console.log(event.detail);
		});
		editorElement.addEventListener('changed', function (event) {
			undoElementClone.disabled = !event.detail.canUndo;
			redoElementClone.disabled =  !event.detail.canRedo;
		});
		importContentElementClone.addEventListener('click', function () {
	      const value = importContentField.innerHTML;
	      editorElement.editor.import_(value, importContentField.dataset.type);
	    });
		DonebtnClone.addEventListener('click', function ()
		{
		 console.log('Converted output is ');
		 var content = editorElement.editor.exports["text/plain"];
		 var retVal = confirm("You are going to save this object as \" "+content+ "\". Do you want to continue?");
		 if( retVal == true )
		 {
			 if(fine_shapes[selected_index].shape_name.startsWith("recursive")){
				 content=content.replace(/\s/gm,'');
				 content=content.replace(/\.+/gm,'\.\.');
				 var regex=/^[_a-zA-Z][_a-zA-Z0-9]{0,30}[(][0|1][\.][\.][\*|1][)][_a-zA-Z][_a-zA-Z0-9]{0,30}[(][0|1][\.][\.][\*|1][)]$/gm;
				 if(content.match(regex)){
						fine_shapes[selected_index].shape_name="recursive\n"+content;
					}
					else{
						alert("You have to enter the content as Role1(cardinality)/\n Role2(cardinality), and the cardinality should be one of the following((0..1)(1..1)(1..*)(0..*)).");
					}
			 }
			 else{

				 content=content.replace(/\s/gm,'');
				 var regex=/^[_a-zA-Z][_a-zA-Z0-9]{0,30}$/gm;
				 if(content.match(regex)){
				 fine_shapes[selected_index].shape_name= content;
			 }
			 else{
				 var temp=content.replace(/\W/gm, '');
				var confirmation= confirm("All identifiers should have the same structure as [_a-zA-Z][_a-zA-Z0-9]{0,30}, Would you like to save it as "+temp);
				if(confirmation){
					fine_shapes[selected_index].shape_name= temp;

				}
			 }
		 }
	 }
		  else
  		{
  				 alert("Value has not been saved.");

  		}
			var editorDiv = document.getElementById('editor');
		 while(editorDiv)
		 {
			 editorDiv.parentNode.removeChild(editorDiv);
			 editorDiv = document.getElementById('editor');
		 }

		});
}
/**
 * [showeditor A function used to show the editor window for a specific object]
 * @param  {Int} selected_index [The index of the selected object]
 * @param  {String} text          [This parameter is used when the user wants to update a text in object so he could find the old text to import and then update this text ]
 */
function showeditor(selected_index,text){
	var modal = document.getElementById('modalBody');
	var editorElement = document.createElement('div');

	editorElement.id = 'editor';
	editorElement.setAttribute("style","width:100%; height:100%");
	modal.appendChild(editorElement);
	if(fine_shapes[selected_index].shape_name.startsWith(RECURSIVE_RELATION))
	{
		text_temp=fine_shapes[selected_index].shape_name.substr(9);
		text=text_temp.trimStart();
	}
	initEditorButtons(editorElement,text);

	//to show and prevent my modal from disappearing when clik outside it
	$("#myModal").modal({
    backdrop: 'static',
    keyboard: false
  });
  var editor = MyScript.register(editorElement, {
    recognitionParams: {
      server: {
        applicationKey: '7095474d-9e86-4aea-b7e9-a676f3e261ca',
         hmacKey: '64926759-da70-4f2a-a706-9f90386293bb'
      }
    }
  });
}
