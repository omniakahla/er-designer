var SQLConstraints=["PRIMARY KEY","NOT NULL","UNIQUE","IS BINARY COLUMN","UNSIGNED DATA TYPE",/*"ZERO FILL",*/"AUTO INCREMENTAL"/*,"FOREIGN KEY"*/,"CHECK", "DEFAULT"];

var MySQLTypesWithDefaultValues=[{DataType:"CHAR(size)",ParameterDefaultValue:"50"}];

var MySQLDataTypes=["CHAR(size)","VARCHAR(size)","TINYTEXT","TEXT","BLOB","MEDIUMTEXT","MEDIUMBLOB","LONGTEXT","LONGBLOB","SET",/*"ENUM(x,y,z,etc.)",*/
"TINYINT(length)","SMALLINT(length)","MEDIUMINT(length)","INT(length)","BIGINT(length)","FLOAT(m,d)","DOUBLE(m,d)","DECIMAL(m,d)",
"DATE","DATETIME(0-6)","TIMESTAMP(0-6)","TIME(0-6)","YEAR(2 or 4)"];
var MySQLDataTypes_Parameters=["size","size","","","","","","","","","x,y,z,...",
"size","size","size","size","size","size,d","size,d","size,d",
"",0,0,0,4];

var SQLServerDataTypes=["char(n)","varchar(n)", "text","nchar","nvarchar","nvarchar(max)","ntext","binary(n)","varbinary","varbinary(max)","image",
"bit","tinyint","smallint","int","bigint","decimal(p,s)","numeric(p,s)","smallmoney","money","float(n)","real",
"datetime","datetime2","smalldatetime","date","time","datetimeoffset","timestamp","sql_variant","uniqueidentifier","xml","cursor","table"];
var SQLServerDataTypes_Parameters=["n","n", "","","","max","","n","","max","",
"","","","","","p,s","p,s","","","n","",
"","","","","","","","","","","",""];

var OracleDataTypes=["char(size)","nchar(size)","nvarchar2(size)","varchar2(size)","long","raw","long raw",
"number(p,s)","numeric(p,s)","float","dec(p,s)","decimal(p,s)","int","smallint","real","double precision",
"date","timestamp (fractional seconds precision)","interval year (year precision)","interval day(day precision)",
"bfile","blob","clob","nclob","rowid","urowid(size)"];
var OracleDataTypes_Parameters=["size","size","size","size","","","",
"p,s","p,s","","p,s","p,s","","","","",
"","fractional seconds precision","year precision","day precision",
"bfile","blob","clob","nclob","rowid","size"];

var MongodbTypes=["Array", "Binary","Boolean","Code","Date","Decimal128", "Double",
                  "Int32","Int64", "MaxKey","MinKey","Null","Object","ObjectId",
                  "BSONRegexp","String", "Symbol","Timestamp","Undefined"];
var Mongodb_defaultValues=["[]", "Binary('')","true","Code('true', {})",getCurrentDateForMongodb(),"0", "0",
                  "-999999999","--1000000000000000128", "MaxKey()","MinKey()","null","Object","ObjectId",
                  "/[Object Object]/","\"\"", "","0","undefined"];

function getCurrentDateForMongodb(){
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
today = yyyy+'/'+ ((mm<10) ? '0'+mm:mm) + '/' + ((dd<10) ? '0'+dd:dd) +' 00:00:00.000';
return today;
}
