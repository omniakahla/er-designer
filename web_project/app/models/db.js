var configDB = require('../../config/database');
var mongoose = require('mongoose');
mongoose.connect(configDB.url); // connect to our database
var db= mongoose.connection;
module.exports = db;
