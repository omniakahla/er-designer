var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var attributeType=new Schema({
  entity_name:String,//used in the UI
  attribute_name: String,//used in the UI
  attribute_type:String,//returned from UI , for SQL databases
  attribute_type_parameters:String, // for SQL databases
  attribute_constraints:[String],//returned from UI, for SQL databases
  attribute_constraints_parameters:String, // for SQL databases
  attribute_Initialvalue:String,//returned from UI,  used for NoSQL databases
  entityName_attributeName:String//used for searching
});

var shape = new Schema({
    shape_x1  : Number,// the starting point of the shape, or the center for the circles.
    shape_y1  : Number,// the starting point of the shape, or the center for the circles.
    shape_type  : String, //rectangle, diamond, circle, ...
    shape_name  :String, // for example Employees.
    shape_line:Number, //1 for solid line, 0 for dotted line, 2 for double line
    shape_width:Number, //used for rectangles and diamonds.
    shape_height:Number,//used for rectangles and diamonds.
    shape_x2:Number, //used for lines
    shape_y2:Number,//used for lines
    shape_direction:Number, //used for the arrows
    shape_diameter:Number,//used for circles.
		id_shape_1:Number,
		id_shape_2:Number,
    id:Number,
    key:Number//1 for priamry key, 0 for partial key, -1 otherwise
});


var relation_extradetails=new Schema({ //used for the graph databases, to know the direction of the relation
  relationId:Number,
  relation:shape,
  connectedEntitiesToParentEntity:[shape],
  connectedEntitiesToRelation:[shape],
  mainRole:String,
  directionFrom:String
/*  relation_name:String,
  Entity1_name:String,
  Entity2_name:String,
  direction:Number //1 means the direction from Entity_1 to Entity_2, or 2 means the direction form Entity_2 to Entity_1
*/});
var SchemaConfigurationsSchema = mongoose.Schema({
  schemaName : String,
  dataTypes: [attributeType],
  relations:[relation_extradetails]
});

module.exports = mongoose.model('schemaConfiguration', SchemaConfigurationsSchema);
