var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var shape = new Schema({
    shape_x1  : Number,// the starting point of the shape, or the center for the circles.
    shape_y1  : Number,// the starting point of the shape, or the center for the circles.
    shape_type  : String, //rectangle, diamond, circle, ...
    shape_name  :String, // for example Employees.
    shape_line:Number, //1 for solid line, 0 for dotted line, 2 for double line
    shape_width:Number, //used for rectangles and diamonds.
    shape_height:Number,//used for rectangles and diamonds.
    shape_x2:Number, //used for lines
    shape_y2:Number,//used for lines
    shape_direction:Number, //used for the arrows
    shape_diameter:Number,//used for circles.
		id_shape_1:Number,
		id_shape_2:Number,
    id:Number,
    key:Number//1 for priamry key, 0 for partial key, -1 otherwise
});

var projectSchema = mongoose.Schema({
  name : String,
  model: String,
  createDate:  {type: Date, default: Date.now},
  modificationDate: {type: Date, default: Date.now},
  diagram: [shape],
  connections: [String],
  schemaIds: [mongoose.Schema.Types.ObjectId]
});

module.exports = mongoose.model('projects', projectSchema);
