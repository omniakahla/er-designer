var User = require('../app/models/user');
var projectSchema = require('../app/models/project');
var  SchemaConfigurationsSchema= require('../app/models/schemaConfigurations');
var db=require('../app/models/db');
var mysql = require('mysql');
var oracledb = require('oracledb');
var neo4j = require('neo4j-driver').v1;
oracledb.autoCommit = true;
var sys = require('util');
var exec = require('child_process').exec;

module.exports = function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        //res.render('index.ejs'); // load the index.ejs file
       res.render('front2.ejs', {
           user : req.user // get the user out of session and pass to template
       });
    });
    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage'), user : null } );
    });

    app.post('/login', passport.authenticate('local-login', {
       successRedirect : '/projects', // redirect to the secure profile section
       failureRedirect : '/login', // redirect back to the signup page if there is an error
       failureFlash : true // allow flash messages
    }));

    app.get('/front', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('front2.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    // process the login form
    // app.post('/login', do all our passport stuff here);

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { message: req.flash('signupMessage'), user : null });
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
       successRedirect : '/projects', // redirect to the secure profile section
       failureRedirect : '/signup', // redirect back to the signup page if there is an error
       failureFlash : true // allow flash messages
   }));
    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/profile', isLoggedIn, function(req, res) {
        res.render('profile.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout',function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/draw', isLoggedIn,function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('draw.ejs', {
            user : req.user // get the user out of session and pass to template
        });

    });

    app.get('/about', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('about.ejs', { message: req.flash('signupMessage') ,
            user : req.user // get the user out of session and pass to template
        });
    });

    app.get('/projects', isLoggedIn,function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('projects.ejs', {
            user : req.user // get the user out of session and pass to template
        });
      });

    app.use(function(req, res, next) {
      res.locals.user = req.session.user;
      next();
    });

    app.get( '/getProjectIDs', function( req, res)
    {
      console.log("Start get project ids");
      var query = {
          '_id': {$in : req.user.projects_id}
      };

      projectSchema.find(query, function (err, docs) {
        if (err) {
          console.log(req.user.projects_id[0]);
           console.log('Error > some err');
         }
        else {
          console.log("First project is : "+docs[0]);
          console.log(req.user.projects_id[0]);
          console.log(req.user._id);
          var user_id= req.user._id;
          res.send({docs,user_id});
        }
      });
    });

    app.get( '/GetProjectDiagrambyID', function( req, res)
    {
      var diagram = new Array();
      var query = {
          '_id': req.query.ID
      };
      console.log(" request => "+req.query.ID);
      var projection = {"diagram": 1, "_id": 0};
      projectSchema.find(query, function (err, docs)
      {
        if (err)
         {
           console.log('Error > some err');
         }
        else
        {
          console.log("Diagram returned successfully");
          console.log(docs.length);
        res.send(docs);
      }});
    });

    app.post('/deleteProjectbyID', function(req,res)
    {
      console.log("delete started");
      //  console.log("app session user "+app.session.user);
      var project_id=req.body.ID;
      var user_id = req.body.User;
      console.log(project_id);
      var query = {'_id': project_id};
      var query2 = {'_id': user_id};
      var updateOpn = {$pull: {projects_id : project_id}};
      console.log("delete project by ID for user_id: "+user_id);
      projectSchema.deleteOne(query, function (err, deleteditems)
       {
        if(err)
        {
          console.log("delete error");
        }
        else
        {
          console.log("project deleted still user_projectsID");
            User.update(query2, updateOpn, function(err,updatedfield)
            {
              if(err)
              {
                console.log("update error");
              }
              else
              {
                console.log(updatedfield);
                console.log("update user finished.")
                  res.send("deleted");
              }
            }
          );
        }
      });
    });

    app.get( '/getSchemaDatatypes', function( req, res)
    {
      var query1 = {
          '_id': req.query.ID//returns the schemas that correspond to a project
      };
      //var projection1={"_id":0,"schemaIds":1};
      projectSchema.find(query1/*, projection1*/,function (err, project) {
        if (err) {
          console.log("No Schemas yet");
          res.send("Project not saved yet");
        }
        else {
          console.log("Project Id = "+ req.query.ID);
          console.log("schema name = "+req.query.schemaName );
          console.log("Project returned from 1st query = "+project[0].schemaIds);
          var query2={'_id': {$in :project[0].schemaIds} ,'schemaName':req.query.schemaName};
          var projection2={"_id":0,"dataTypes":1};

          SchemaConfigurationsSchema.find(query2/*,projection2*/,function(err,docs){
            if (err) {
              console.log(err);
              console.log("No Schema with this Name");
              res.send([]);
            }
            else {
              console.log("returned from 2nd query = "+docs[0]);
              res.send(docs);
            }
          });
        }
      });
    });

    app.post( '/saveSchemaConfiguration',function (req,res) {
      var query = {'_id': req.body.ID};
      projectSchema.find(query, function(err,project){
        if (err) {
          console.log("Project is not saved yet!");
          res.send("Project not saved yet");
          //throw err;
	       }
         else{
           console.log("Project is already saved and we start to save the schema");
           console.log("Project schemas are "+project[0].schemaIds);
            //query={'_id': {$in : project[0].schemaIds},'schemaName':req.body.schemaName};
            query={$and:[{'_id': {$in : project[0].schemaIds}},{'schemaName':req.body.schemaName}]};
            //db.schemaconfigurations.find({$and:[{_id:{ $in:[1,ObjectId("5bb1eac884b8ed26b0479acd")]}},{schemaName:"oracle"}]},{schemaName:1,_id:0});
           SchemaConfigurationsSchema.find(query, function(err,docs){
             if(err||project[0].schemaIds.length==0||docs.length==0){//we didn't find it and we need to save a new one otherwise we need to update the existing one.
             console.log("we didn't find the schema so we will save it now");
               var NewSchema = new SchemaConfigurationsSchema();
               NewSchema.schemaName = req.body.schemaName;
               //NewSchema.keys = req.body.keys;
               NewSchema.relations=req.body.relation_extradetails;
                 console.log("look here");
                console.log(req.body.relation_extradetails);
               NewSchema.dataTypes= req.body.dataTypes;
               console.log(req.body.schemaName);
               NewSchema.save(function(err) {
                   if (err){
                     console.log("There was an error in saving the schema ---1");
                     res.send("Project not saved yet");
                     //throw err;
                   }

                     else {
                       console.log("schema saved successfully");
                       var updateOpn = {$push: {schemaIds : NewSchema._id}};
                       console.log("inserteditem._id "+NewSchema._id );
                       query= {'_id': req.body.ID};
                       projectSchema.update(query, updateOpn, function(err,updatedfield)
                         {
                           if(err)
                           {
                             console.log("update error during saving in the project");
                           }
                           else
                           {
                             console.log(updatedfield);
                             console.log("update project finished.")
                               res.send("updated");
                           }
                     });
               }});
             }
             else{
               console.log("We have found the schema and we will update it only");
               console.log("look here");
              console.log(req.body.relation_extradetails);
                 var updateOpn = {$set: {relations: req.body.relation_extradetails}};//,$set: {dataTypes : req.body.dataTypes}};
                 var query = {'_id': docs[0]._id};
                 console.log(docs[0]._id);
                 SchemaConfigurationsSchema.update(query,updateOpn,function(err,updatedfield){
                   if(err)
                   {
                     //throw err;
                     console.log("update error in updating the schema");
                   }
                   else
                   {
                     var updateOpn = {$set: {dataTypes : req.body.dataTypes}};
                     var query = {'_id': docs[0]._id};
                     console.log(docs[0]._id);
                     SchemaConfigurationsSchema.update(query,updateOpn,function(err,updatedfield){
                       if(err)
                       {
                         //throw err;
                         console.log("update error in updating the schema");
                       }
                       else
                       {
                         console.log(updatedfield);
                         console.log("update schema finished.")
                           res.send("updated");
                       }
                     });
                   }
                 });
               }
           });
         }
      });

    });

    app.post( '/saveDiagram',function (req,res) {
      var query = {'_id': req.user._id};
      var projectsCount;

      var Newproject = new projectSchema();
      Newproject.name = req.body.ProjectName;
      Newproject.model = req.body.model;
      Newproject.diagram= req.body.Diagram;
      Newproject.save(function(err) {
          if (err)
              throw err;
            else {
              var updateOpn = {$push: {projects_id : Newproject._id}};
              console.log("inserteditem._id "+Newproject._id );
              console.log("project inserted still user_projectsID");
              User.update(query, updateOpn, function(err,updatedfield)
                {
                  if(err)
                  {
                    throw err;
                    console.log("update error");
                  }
                  else
                  {
                    console.log(updatedfield);
                    console.log("update user finished.")
                      res.send(Newproject._id);
                  }
            });
      }});
    });

    app.post( '/updateProject',function (req,res) {
      var query = {'_id': req.body.ID};
      var updateOpn={ $set:{diagram:req.body.Diagram}};
      projectSchema.update(query, updateOpn, function(err,updatedfield)
      {
        if(err)
        {
          throw err;
          console.log("update error");
        }
        else
        {
          console.log(updatedfield);
          console.log("update project finished.")
            res.send("updated");
        }
      }
    );
    });

    app.post( '/connectToMySQL',function (req,res) {
      var MySQLhost=req.body.host;
      var MySQLuser=req.body.user;
      var MySQLpassword=req.body.password;
      var MySQLSchema=req.body.schema;
      var MySQLDBName=req.body.DBName;
      var CreateDB=req.body.CreateDB;
      var con = mysql.createConnection({
        host: MySQLhost,
        user: MySQLuser,
        password: MySQLpassword,
        //database:MySQLDBName,
        multipleStatements:true
      });
      con.connect(function(err)
      {
        if (err)
        {
          res.send(err.message);
        }
        else
        {
          console.log("Connected!");
          con.query(MySQLSchema, function (err, result) {
            if (err)
            {
              console.log(err);
              res.send("Error in creating schema: "+err.message);
            }
            else {
              res.send("Created");
            }
          });
          con.end();
        }
      });
});

app.post( '/connectToNeo4j',function (req,res) {

  var driver = neo4j.driver("bolt://localhost", neo4j.auth.basic("neo4j", "neo4j"));
  var session = driver.session();
  //var splittedSchema=req.body.script.split(';');
  /*session
    .run(req.body.script)
    .subscribe({
      onNext: function () {
        console.log('done');
        res.send("Created");
      },
      onCompleted: function () {
        session.close();
      },
      onError: function (error) {
        console.log(error);
      }
    });*/

 session
    .run(req.body.script.split('\n')[0])
    .then(function (result) {
      session.close();
      result.records.forEach(function (record) {
        console.log('done');
        res.send("Created");
      });
      driver.close();
    })
    .catch(function (error) {
      console.log(error);
      res.send("Error in creating schema");
      driver.close();
    });
});



    app.post( '/connectToOracle',function (req,res) {
      oracledb.getConnection(
      {
        user          : req.body.user,
        password      : req.body.password,
        connectString : req.body.connectString
      },
      function(err, connection)
      {
        if (err)
        {
          console.error(err.message);
          res.send(err.message);
        }
        else {
          var  sql_statements =req.body.schema.split(";");
          result=executeNextOracleStatement(connection,0,sql_statements,res);
        //  res.send(result);
        }
      });

});

function executeNextOracleStatement(connection,i, sql_statements,res)
{
  if(i>=sql_statements.length-1)
  {
    connection.close();
    res.send("Created");
    return;
  }
  console.log(sql_statements[i]);
  connection.execute(sql_statements[i],
  function(err, result)
  {
    if (err)
    {
      console.log(err.message);
      connection.close();
      res.send(err.message);
      //return err.message;
    }
    else
    {
      i++;
      executeNextOracleStatement(connection,i,sql_statements,res);

    }
  });
}
app.post( '/executeBashCMD',function (req,res) {

  var fs = require('fs');
  var filename=//"../../temp/"+Date.now()+".js";
  "C:/Users/kahlao/Desktop/tempfiles/"+Date.now()+".js";
  console.log("hello from here");
  fs.writeFile(filename, req.body.script, function(err) {
      if(err) {
           console.log(err);
           res.send("something went wrong!");
      }
      else
      {
        dir = exec("mongo "+filename, function(err, stdout, stderr) {
        if (err)
        {
            console.log(err);
            res.send("Command failed, check the initial values");
        }
        else
        {
          console.log(stdout);
          res.send("Created");
        }
        });
      }
  });
});
}
// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();
    // if they aren't redirect them to the home page
    res.redirect('/');
}
