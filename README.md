# ER-Designer

Before you start, you will need to install:
-----------------------------------------------
1. node.js -->port 8080
2. MongoDB-->port 27017
3. MySQL --> port 3306
4. OracleDB 11g -->  Port for 'Oracle HTTP Listener': 8888
5. Also, make "mongorestore  --db web_project" to the database of our project.
*****************************************************
To run the project:
----------------------
#### On Windows ####
1. open cmd and run "mongod"
2. open another cmd to run the project by "node server.js"
------------------------------------------------------------
#### On Ubuntu ####
1. sudo systemctl start mongod.service
2. open another cmd to run the project by "node server.js"

Finally, The magic happens on port 8080 :)
----------------------
*****************************************************

Some helpful commands for MongoDB on ubuntu:
---------------------------------------------
* sudo systemctl status mongod
* sudo systemctl stop mongod.service
* sudo systemctl start mongod.service
* sudo systemctl enable mongod.service